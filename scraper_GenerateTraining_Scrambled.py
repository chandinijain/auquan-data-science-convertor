import pandas as pd
import numpy as np
import os
import datetime as dt
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, TH

def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

def Price_Momentum(data, period):
        """
        1-Month Price Momentum:
        1-month closing price rate of change.
        https://www.pnc.com/content/dam/pnc-com/pdf/personal/wealth-investments/WhitePapers/FactorAnalysisFeb2014.pdf # NOQA
        Notes:
        High value suggests momentum (shorter term)
        Equivalent to analysis of returns (1-month window)
        """
        return data - data.shift(period)


def Price_Oscillator(data, p1, p2):
        """
        4/52-Week Price Oscillator:
        Average close prices over 4-weeks divided by average close
        prices over 52-weeks all less 1.
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests momentum
        """
        out = data.rolling(p1).mean() / data.rolling(p2).mean() - 1.
        return out

def Trendline(df,p):
		"""
		52-Week Trendline:
		Slope of the linear regression across a 1 year lookback window.
		https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
		Notes:
		High value suggests momentum
		Calculated using the MLE of the slope of the regression
		"""

		# using MLE for speed

		# prepare X matrix (x_is - x_bar)
		import pdb;pdb.set_trace()
		data=df.copy()
		X = np.array(range(p))/75.0
		X_bar = np.nanmean(X)
		X_vector = X - X_bar
		X_matrix = np.tile(X_vector, (len(data.T), 1)).T

		# prepare Y matrix (y_is - y_bar)
		Y_bar = np.nanmean(data, axis=0)
		Y_bars = np.tile(Y_bar, (p, 1))
		Y_matrix = data.values.reshape(-1) - Y_bars

		# prepare variance of X
		X_var = np.nanvar(X)

		# multiply X matrix an Y matrix and sum (dot product)
		# then divide by variance of X
		# this gives the MLE of Beta
		out = (np.sum((X_matrix * Y_matrix), axis=0) / X_var)/ (p)
		window = p
		a = np.array([np.nan] * len(df))
		b = [np.nan] * len(df)  # If betas required.
		y_ = df.values
		x_ = X_matrix
		for n in range(window, len(df)):
		    y = y_[(n - window):n]
		    X = x_[(n - window):n]
		    # betas = Inverse(X'.X).X'.y
		    betas = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(y)
		    y_hat = betas.dot(x_[n, :])
		    a[n] = y_hat
		    b[n] = betas.tolist()  # If betas required.

		return out

def Mean_Reversion(data,p1,p2):
        """
        1-Month Mean Reversion:
        1-month return less 12-month average of monthly return, all over
        standard deviation of 12-month average of monthly returns.
        https://www.pnc.com/content/dam/pnc-com/pdf/personal/wealth-investments/WhitePapers/FactorAnalysisFeb2014.pdf # NOQA
        Notes:
        High value suggests momentum (short term)
        Equivalent to analysis of returns (12-month window)

        """

        rets = data/data.shift(p1) - 1

        out = (rets - rets.rolling(p2).mean()) /rets.rolling(p2).std()

        return out

def Mean_Reversion_Rolling(data,p1,p2):
        """
        1-Month Mean Reversion:
        1-month return less 12-month average of monthly return, all over
        standard deviation of 12-month average of monthly returns.
        https://www.pnc.com/content/dam/pnc-com/pdf/personal/wealth-investments/WhitePapers/FactorAnalysisFeb2014.pdf # NOQA
        Notes:
        High value suggests momentum (short term)
        Equivalent to analysis of returns (12-month window)

        """
        rets = data/data.shift(p1) - 1

        mean = rets.groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).mean().reset_index(0,drop=True).sort_index()
        sdev = rets.groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).std().reset_index(0,drop=True).sort_index()
        out = (rets - mean) /sdev
        # import pdb;pdb.set_trace()
        out.fillna(method='pad', inplace=True)

        return out

    # 10 Day MACD signal line
def MACD_Signal(data,p1,p2,p3):
        """
        10 Day Moving Average Convergence/Divergence (MACD) signal line:
        10 day Exponential Moving Average (EMA) of the difference between
        12-day and 26-day EMA of close price
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests turning point in positive momentum
        Low value suggests turning point in negative momentum
        """
        window_length = 60

        sig_lines = []

        fastperiod=p1
        slowperiod=p2
        signalperiod=p3
        out = (data.ewm(halflife=fastperiod).mean()-data.ewm(halflife=slowperiod).mean()).ewm(halflife=signalperiod).mean()
        return out

def Vol(data,p1, p2):
        """
        3-month Volatility:
        Standard deviation of returns over 3 months
        http://www.morningstar.com/invglossary/historical_volatility.aspx
        Notes:
        High Value suggests that equity price fluctuates wildly
        """

        rets = data/data.shift(p1) - 1

        out = rets.rolling(p2).std()
        return out

def Vol_Rolling(data,p1, p2):
        """
        3-month Volatility:
        Standard deviation of returns over 3 months
        http://www.morningstar.com/invglossary/historical_volatility.aspx
        Notes:
        High Value suggests that equity price fluctuates wildly
        """

        rets = data/data.shift(p1) - 1
        sdev = rets.groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).std().reset_index(0,drop=True).sort_index()
        return sdev

    # 20-day Stochastic Oscillator
def Stochastic_Oscillator(data,p,p2):
        """
        20-day Stochastic Oscillator:
        K = (close price - 5-day low) / (5-day high - 5-day low)
        D = 100 * (average of past 3 K's)
        We use the slow-D period here (the D above)
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests turning point in positive momentum (expected decrease)
        Low value suggests turning point in negative momentum (expected increase)
        """
        high = data.rolling(p).max()
        low = data.rolling(p).min()
        k = (data-low)/(high-low)
        d = 100*k.rolling(p2).mean()
        return d

    # 5-day Money Flow / Volume

def Moneyflow_Volume(data,volume,p1, p2):
        """
        5-day Money Flow / Volume:
        Numerator: if price today greater than price yesterday, add price * volume, otherwise subtract
        Denominator: prices and volumes multiplied and summed
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests turning point in positive momentum (expected decrease)
        Low value suggests turning point in negative momentum (expected increase)
        """
        diff = data - data.shift(p1)

        n = (np.sign(diff)*data*volume).rolling(p2).sum()
        d = (data*volume).rolling(p2).sum()

        return n/d.astype(float)



def Moneyflow_Volume_Rolling(data,volume,p1, p2):
        """
        5-day Money Flow / Volume:
        Numerator: if price today greater than price yesterday, add price * volume, otherwise subtract
        Denominator: prices and volumes multiplied and summed
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests turning point in positive momentum (expected decrease)
        Low value suggests turning point in negative momentum (expected increase)
        """
        diff = data - data.shift(p1)

        volDiff = volume-volume.shift(1)
        volDiff.loc[volDiff<0] = 0

        resampledVol = volDiff.rolling(p1).sum()

        n = (np.sign(diff)*data*resampledVol).groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).sum().reset_index(0,drop=True).sort_index()
        d = (data*resampledVol).groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).sum().reset_index(0,drop=True).sort_index()

        return n/d.astype(float)

def ATR(data,p1):

	p = 75
	high = data.rolling(p).max()
	low = data.rolling(p).min()
	TR=(high-low).combine((high-data.shift(p)).abs(), max, 0).combine((low-data.shift(p)).abs(), max, 0)
	ATR = TR.ewm(span = p1).mean()
	return ATR

def RSI(data,p):
	p=75
	delta = data - data.shift(p)

	up, down = delta.copy(), delta.copy()
	up[up < 0] = 0
	down[down > 0] = 0

	# Calculate the EWMA
	roll_up1 = pd.stats.moments.ewma(up, p)
	roll_down1 = pd.stats.moments.ewma(down.abs(), p)

	# Calculate the RSI based on EWMA
	RS1 = roll_up1 / roll_down1
	RSI1 = 100.0 - (100.0 / (1.0 + RS1))
	return RSI1


def normalize_minmax(s):
    result = s.copy()
    max_value = s.max()
    min_value = s.min()
    result = (s - min_value) / (max_value - min_value)
    return result

def normalize_mean(s):
    result = s.copy()
    mean = s.mean()
    std = s.std()
    result = (s - mean) / std
    return result

def normalize_by_return(s,k):
    result = s.copy()
    rets = s/s.iloc[0]
    result = k*rets

    return result

def percentile(data):
	return data.rank(pct = True)

def transform(data):
	data['CLOSE'] = normalize_by_return(data['CLOSE'],100)
	data['RETURNS'] = data['RETURNS']*100
	data['Volume'] = (data['Volume']*data['CLOSE'].iloc[0]/100)/10000
	data['ADJCLOSE'] = normalize_by_return(data['ADJCLOSE'],100)
	data['ADJVOLUME'] = (data['ADJVOLUME']*data['ADJCLOSE'].iloc[0]/100)/10000


for root, dirs, files in os.walk('/Users/chandinijain/Auquan/DataQQ3'):
	print(dirs)

root = '/Users/chandinijain/Auquan/DataQQ3'
dirs = 'QQDataTraining'
files = next(os.walk('%s/%s'%(root,dirs)))[2]
useful = []
acc = None
scrambled = pd.read_csv('%s' % 'scramblerQQ3.csv', index_col=0)  # 
# pd.DataFrame(index=files, columns=['AddVal', 'Name'])
print(scrambled)
# ### Training Params
# dateshift = -1309
# start='2014-1-1'
# end = '2016-09-08'

# ### Test Params
dateshift = 0
start='2015-03-01'
end = '2017-02-28'

scrambleddirs = 'QQDataTest'
scrambledfiles = next(os.walk('%s/%s'%(root,scrambleddirs)))[2]
for name in files[1:]:
		# dateparse = lambda dates: [pd.datetime.strptime(d, '%d/%m/%y %H:%M:%S') for d in dates]

		'''
		returns*100
		scalings: Momentum: None + percentile
		Osc = *100
		MR : None + percentile
		SO: None
		Vol *100
		DR*100
		Vol*100
		'''
		try:
			fakenames = scrambled.loc[name, 'Name']
		except:
			print('%s likely corrupt, assigning fakename'%name)
			fakenames = 'sofake'
		if '%s.csv'%fakenames in scrambledfiles:
			print('skipping %s %s'%(name, fakenames))
			continue
		print(name, fakenames)
		data = pd.read_csv(os.path.join(root, dirs, name), index_col=0, parse_dates=True)#, parse_dates=[['DATE', 'TIME']], date_parser=dateparse)	

		data['1dM'] = Price_Momentum(data['ADJCLOSE'], period=75)
		data['5dM'] = Price_Momentum(data['ADJCLOSE'], period=5*75)
		data['2wM'] = Price_Momentum(data['ADJCLOSE'], period=10*75)
		data['1mM'] = Price_Momentum(data['ADJCLOSE'], period=21*75)
		data['3mM'] = Price_Momentum(data['ADJCLOSE'], period=63*75)

		data['PO1'] = Price_Oscillator(data['ADJCLOSE'], 375,4500)*100
		data['PO2'] = Price_Oscillator(data['ADJCLOSE'], 75,750)*100
		data['PO3'] = Price_Oscillator(data['ADJCLOSE'], 12,75)*100
		data['PO4'] = Price_Oscillator(data['ADJCLOSE'], 36,75)*100

		data['MR1'] = Mean_Reversion(data['ADJCLOSE'], 150,750)
		data['MR2'] = Mean_Reversion(data['ADJCLOSE'], 12,75)
		data['MR3'] = Mean_Reversion(data['ADJCLOSE'], 375,1500)
		data['MR4'] = Mean_Reversion(data['ADJCLOSE'], 750,4500)

		data['MACD1'] = MACD_Signal(data['ADJCLOSE'], 375, 750,150)
		data['MACD2'] = MACD_Signal(data['ADJCLOSE'], 750,1500,375)

		data['Vol1'] = Vol(data['ADJCLOSE'], 5, 75)*100
		data['Vol2'] = Vol(data['ADJCLOSE'], 75, 375)*100
		data['Vol3'] = Vol(data['ADJCLOSE'], 75, 750)*100
		data['Vol4'] = Vol(data['ADJCLOSE'], 75, 1500)*100

		data['SO1'] = Stochastic_Oscillator(data['ADJCLOSE'], 750, 3)
		data['SO2'] = Stochastic_Oscillator(data['ADJCLOSE'], 375, 3)
		data['SO3'] = Stochastic_Oscillator(data['ADJCLOSE'], 75, 3)

		data['MFV1'] = Moneyflow_Volume(data['ADJCLOSE'], data['ADJVOLUME'], 75,375)
		data['MFV2'] = Moneyflow_Volume(data['ADJCLOSE'], data['ADJVOLUME'], 75,750)
		data['MFV3'] = Moneyflow_Volume(data['ADJCLOSE'], data['ADJVOLUME'], 75,1500)

		data['ATR1'] = ATR(data['ADJCLOSE'], 10*75)
		data['ATR2'] = ATR(data['ADJCLOSE'], 20*75)
		data['RS1'] = RSI(data['ADJCLOSE'], 10*75)
		data['RS2'] = RSI(data['ADJCLOSE'], 20*75)

		data['DR'] = data['ADJCLOSE'] / data['ADJCLOSE'].shift(75) -1


		data['MAR0'] = data['ADJCLOSE'].rolling(36).mean()/data['ADJCLOSE'].rolling(75).mean()
		data['MAR1'] = data['ADJCLOSE'].rolling(75).mean()/data['ADJCLOSE'].rolling(5*75).mean()
		data['MAR2'] = data['ADJCLOSE'].rolling(5*75).mean()/data['ADJCLOSE'].rolling(21*75).mean()
		data['MAR3'] = data['ADJCLOSE'].rolling(10*75).mean()/data['ADJCLOSE'].rolling(63*75).mean()
		data['MAR4'] = data['ADJCLOSE'].rolling(21*75).mean()/data['ADJCLOSE'].rolling(125*75).mean()

		data['MAr1'] = (data['ADJCLOSE'] / data['ADJCLOSE'].shift(12) -1).rolling(75).mean()*100
		data['MAr2'] = (data['ADJCLOSE'] / data['ADJCLOSE'].shift(75) -1).rolling(5*75).mean()*100
		data['MAr3'] = (data['ADJCLOSE'] / data['ADJCLOSE'].shift(75) -1).rolling(10*75).mean()*100
		data['MAr4'] = (data['ADJCLOSE'] / data['ADJCLOSE'].shift(75) -1).rolling(21*75).mean()*100
		data['MAr5'] = (data['ADJCLOSE'] / data['ADJCLOSE'].shift(75) -1).rolling(63*75).mean()*100
		data['VolDiff'] = data['ADJVOLUME']-data['ADJVOLUME'].shift(1)
		data['VolDiff'].loc[data['VolDiff']<0] = 0
		data['1hVol'] = data['VolDiff'].rolling(12).sum()/data['VolDiff'].rolling(75).sum()
		data['3hVol'] = data['VolDiff'].rolling(36).sum()/data['VolDiff'].rolling(75).sum()
		data['1dVol'] = data['VolDiff'].rolling(75).sum()/(data['VolDiff'].rolling(5*75).sum()/5)
		data['5dAvgVol'] = (data['VolDiff'].rolling(5*75).sum()/5)/(data['VolDiff'].rolling(21*75).sum()/21)
		data['2wAvgVol'] = (data['VolDiff'].rolling(10*75).sum()/10)/(data['VolDiff'].rolling(63*75).sum()/63)
		data['1MAvgVol'] = (data['VolDiff'].rolling(21*75).sum()/21)/(data['VolDiff'].rolling(125*75).sum()/125)

		data['5DHigh'] = data['ADJCLOSE'].rolling(5*75).max()
		data['1DHigh'] = data['ADJCLOSE'].rolling(75).max()
		data['3HHigh'] = data['ADJCLOSE'].rolling(36).max()
		data['1HHigh'] = data['ADJCLOSE'].rolling(12).max()

		data['5DLow'] = data['ADJCLOSE'].rolling(5*75).min()
		data['1DLow'] = data['ADJCLOSE'].rolling(75).min()
		data['3HLow'] = data['ADJCLOSE'].rolling(36).min()
		data['1HLow'] = data['ADJCLOSE'].rolling(12).min()

		data['MRr1'] = Mean_Reversion_Rolling(data['ADJCLOSE'], 150,750)
		data['MRr2'] = Mean_Reversion_Rolling(data['ADJCLOSE'], 12,75)
		data['MRr3'] = Mean_Reversion_Rolling(data['ADJCLOSE'], 375,1500)
		data['MRr4'] = Mean_Reversion_Rolling(data['ADJCLOSE'], 750,4500)

		data['Volr1'] = Vol_Rolling(data['ADJCLOSE'], 5,75)*100
		data['Volr2'] = Vol_Rolling(data['ADJCLOSE'], 75,375)*100
		data['Volr3'] = Vol_Rolling(data['ADJCLOSE'], 75,750)*100
		data['Volr4'] = Vol_Rolling(data['ADJCLOSE'], 75,1500)*100

		data['MFVr1'] = Moneyflow_Volume_Rolling(data['ADJCLOSE'], data['ADJVOLUME'], 75,375)
		data['MFVr2'] = Moneyflow_Volume_Rolling(data['ADJCLOSE'], data['ADJVOLUME'], 75,750)
		data['MFVr3'] = Moneyflow_Volume_Rolling(data['ADJCLOSE'], data['ADJVOLUME'], 75,1500)

		del data['VolDiff']
		p = data['ADJCLOSE']
		transform(data)
		newdata = pd.DataFrame(index=data.index)
		
		#scramble columns
		for i in range(len(data.columns)):
			# print(i,data.columns[i])
			if data.columns[i]!='Y':
				newdata['F%i'%i] = data[data.columns[i]]
		# import pdb;pdb.set_trace()
		delta =  (np.sign(-data['ADJCLOSE']+data['ADJCLOSE'].shift(-75))+np.sign(data['ADJCLOSE']-data['ADJCLOSE'].shift(75)))
		delta.loc[delta.abs()==1] =  (np.sign(-data['ADJCLOSE']+data['ADJCLOSE'].shift(-150))+np.sign(data['ADJCLOSE']-data['ADJCLOSE'].shift(75)))
		delta.loc[delta.abs()==1] =  (np.sign(-data['ADJCLOSE']+data['ADJCLOSE'].shift(-150))+np.sign(data['ADJCLOSE']-data['ADJCLOSE'].shift(150)))
		delta = delta.replace(1, np.nan)
		delta = delta.replace(-1, np.nan)
		delta = delta.fillna(method='bfill')
		newdata['TrueY'] = delta.abs()/2
		newdata['ADJCLOSE'] = p
		# newdata['Y'].loc[newdata['Y']==0.5] =(1+delta)/2
		# newdata['Y'] = newdata['Y'].shift(-75)

		#scramble time
		finalTrainingData = newdata.loc[start:end]
		idx = finalTrainingData.index + pd.Timedelta('%i days'%dateshift)
		finalTrainingData.set_index(idx, inplace=True)
		if finalTrainingData.isna().any().any():
			print(finalTrainingData.isna().any())

			print(finalTrainingData.index)
			print('skipping %s %s'%(name, fakenames))
			import pdb;pdb.set_trace()
		# finalTrainingData = normalize(finalTrainingData)
		# print(finalTrainingData)
		# print(finalTrainingData.isna().any())
		# print(finalTrainingData.isna().any().any())
		else:
			finalTrainingData=finalTrainingData.resample('30T',label='right',closed='right').last().dropna()
	
		# if finalTrainingData.isna().any().any():
		# 	print(finalTrainingData.isna().any())

		# 	print(finalTrainingData.index)
		# 	print('skipping %s %s'%(name, fakenames))
		# else:
			finalTrainingData['TrueY'] = finalTrainingData['TrueY'].astype(int)
			
			# # finalTrainingData = data.loc['2014-1-1':'2017-6-10'] 
			# finalTrainingData.to_csv('/Users/chandinijain/Auquan/DataQQ3/QQDataTest/%s.csv'% scrambled.loc[name, 'Name'], index=True, float_format='%.3f')
			finalTrainingData.to_csv('/Users/chandinijain/Auquan/DataQQ3/QQDataTest/%s.csv'% name, index=True, float_format='%.3f')
			## writecsv('C:/Users/Chandini/Auquan_Beta/%s' % finalloc, data, '%s.csv' )
                   
            