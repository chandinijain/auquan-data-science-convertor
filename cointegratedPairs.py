# -*- coding: utf-8 -*-
"""
Created on Tue May 25 20:29:59 2018

@author: Kaustubh Prakash
"""

import os  
import sys

import pandas as pd
import numpy as np

import statsmodels.formula.api as smf
import statsmodels.tsa.api as smt
import statsmodels.api as sm
import scipy.stats as scs
import statsmodels.stats as sms

import matplotlib.pyplot as plt
import matplotlib as mpl

from statsmodels.tsa.stattools import coint

"""
Code takes in input as the pairs we got from the baccktest with the format: A B
in every line where A and B are stocks
"""

with open("pairslist.txt") as f:    # Pairslist is the file which contains the names of all profitable pairs
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
pairnames = [x.strip().split() for x in content]

with open("stockslist.txt") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
stocknames = [x.strip()[:-4] for x in content]

stocks = {}
trade = {}
for stock in stocknames:
    stocks[stock]=pd.read_csv("parsedData/"+stock+".csv", error_bad_lines=False)
    trade[stock]=0
    
for stock in stocknames:
    stocks[stock]["Unnamed: 0"] = stocks[stock]["Unnamed: 0"].str[1:-2]

for stock in stocknames:
    stocks[stock] = stocks[stock].set_index("Unnamed: 0")
    
for stock in stocknames:
    stocks[stock] = stocks[stock]["stockVWAP"]
    
pos = {}
for pair in pairnames:
    pairhash=pair[0]+pair[1]
    pos[pairhash]=0
    
def check_cointegrated_pairs(S1,S2,pairs,time):
    pairs = []   
    result = coint(S1, S2)
    score = result[0]
    pvalue = result[1]
    if pvalue < 0.1:
        return True
    return False

def arima(ratiovwap):
    best_aic = np.inf 
    best_order = None
    best_mdl = None
    pq_rng = range(5) # [0,1,2,3]
    d_rng = range(2) # [0,1]
    for i in pq_rng:
        for d in d_rng:
            for j in pq_rng:
                try:
                    tmp_mdl = smt.ARIMA(ratiovwap, order=(i,d,j)).fit(method='mle', trend='nc')
                    tmp_aic = tmp_mdl.aic
                    if tmp_aic < best_aic:
                        best_aic = tmp_aic
                        best_order = (i, d, j)
                        best_mdl = tmp_mdl
                except: continue
    
    return best_mdl
                
"""
This code takes each pair, checks if the pair is cointegrated for the time interval
'time'. Then it uses an arima model on the ratios of the pair (since ratios are more stationary)
it then forecast for n_steps and then gives us predictions when to buy or not
"""

""" We found the optiml timestep to be t=1650 in the Pairs_trading.ipynb file"""
""" pos array is used to denote the current position of the pair. pos[] = 1 denotes a long position, 
-1 denotes a short position and 0 denoted no position."""
delta = 1650
time = delta
money = 0
while (time<(90000-delta)):
    for pairs in pairnames:
        S1 = stocks[pairs[0]].iloc[time-delta:time]
        S2 = stocks[pairs[1]].iloc[time-delta:time]
        rat = S1/S2;
        pairshash=pairs[0]+pairs[1]
        if check_cointegrated_pairs(S1,S2,pairs,time):
            mdl = arima(rat)
            n_steps = delta
            f, err95, ci95 = mdl.forecast(steps=n_steps)
            for i in range(1650):
                if (rat[time+i] > ci95[i][1]) and (pos[pairshash] == 0):
                    trade[pairs[0]] = trade[pairs[0]] - 100
                    money += S1[time+i]*100
                    trade[pairs[1]] = trade[pairs[1]] + 100*rat[time+i]
                    money -= S2[time+i]*100*rat[time+i]
                    pos[pairshash]=1
                    print("Money After Trade",money)
                    print("Stock of S1 after trade",trade[pairs[0]])
                    print("Stock of S2 after trade",trade[pairs[1]])
                    print("\n")
                elif (rat[time+i] < ci95[i][0]) and (pos[pairshash]==0):
                    trade[pairs[0]] = trade[pairs[0]] + 100
                    money -= S1[time+i]*100
                    trade[pairs[1]] = trade[pairs[1]] - 100*rat[time+i]
                    money += S2[time+i]*100*rat[time+i]
                    pos[pairshash]=-1
                    print("Money After Trade",money)
                    print("Stock of S1 after trade",trade[pairs[0]])
                    print("Stock of S2 after trade",trade[pairs[1]])
                    print("\n")
                elif (pos[pairshash]==1) and (rat[time+i]-f[i] < err95[i]):
                    trade[pairs[0]] = trade[pairs[0]] + 100
                    money -= S1[time+i]*100
                    trade[pairs[1]] = trade[pairs[1]] - 100*rat[time+i]
                    money += S2[time+i]*100*rat[time+i]
                    pos[pairshash]=0
                    print("Money After Trade",money)
                    print("Stock of S1 after trade",trade[pairs[0]])
                    print("Stock of S2 after trade",trade[pairs[1]])
                    print("\n")
                elif (pos[pairshash]==-1) and (rat[time+i]-f[i] < err95[i]):
                    trade[pairs[0]] = trade[pairs[0]] - 100
                    money += S1[time+i]*100
                    trade[pairs[1]] = trade[pairs[1]] + 100*rat[time+i]
                    money -= S2[time+i]*100*rat[time+i]
                    pos[pairshash]=0
                    print("Money After Trade",money)
                    print("Stock of S1 after trade",trade[pairs[0]])
                    print("Stock of S2 after trade",trade[pairs[1]])
                    print("\n")
                    
    time = time + delta