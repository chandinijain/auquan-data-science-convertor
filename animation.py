import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import pandas as pd

import statsmodels
from statsmodels.tsa.stattools import coint
# just set the seed for the random number generator
np.random.seed(107)
# Generate the daily returns
Xreturns = np.random.normal(0, 1, 1000) 
# sum them and shift all the prices up
# X = pd.Series(np.cumsum(Xreturns), name='X') + 50
noise = np.random.normal(0, .1, 1000)
# Y = X + 5 + noise
# Y.name = 'Y'
# Z = pd.concat([X, Y], axis=1)

def data_gen(t=0):
    cnt = 0
    X = 10
    Y = 20
    while cnt < 999:
        cnt += 1
        t += 1
        X = X + .1 + Xreturns[cnt]
        Y = (2+ cnt*0.0005+ noise[cnt])*X 
        yield t, X, Y


def init():
    ax[0].set_ylim(30, 70)
    ax[0].set_xlim(0, 1000)
    ax[1].set_ylim(1.9, 2.1)
    del xdata[:]
    del y1data[:]
    del y2data[:]
    del y3data[:]
    del y4data[:]
    line1.set_data(xdata, y1data)
    line2.set_data(xdata, y2data)
    line3.set_data(xdata, y3data)
    line4.set_data(xdata, y4data)
    return line1, line2, line3, line4, 

fig, ax = plt.subplots(2, sharex=True, figsize=(15,7))
line1, = ax[0].plot([], [], 'g', lw=1, label='Stock1')
line2, = ax[0].plot([], [], 'c', lw=1, label='Stock2')
line3, = ax[1].plot([], [], 'b', lw=1, label='Price Ratio')
line4, = ax[1].plot([], [], 'r', lw=1.5, label='Mean')
ax[0].grid()
ax[0].set_title('Stock Price')
ax[0].legend(loc="upper right")
ax[1].grid()
ax[1].set_title('Stock Price Ratio')
ax[1].legend(loc="upper right")
xdata, y1data, y2data, y3data, y4data = [], [], [], [], []


def run(data):
    # update the data
    t, y1, y2 = data
    xdata.append(t)
    y1data.append(y1)
    y2data.append(y2)
    y3data.append(y2/y1)
    y4data.append(np.mean(y3data))
    ymin, ymax = ax[0].get_ylim()

    if y2 >= ymax:
        ax[0].set_ylim(ymin, y2+5)
        ax[0].figure.canvas.draw()
    if y1 <= ymin:
        ax[0].set_ylim(y1-5, ymax)
        ax[0].figure.canvas.draw()

    y2min, y2max = ax[1].get_ylim()

    if y2/y1 >= y2max:
        ax[1].set_ylim(y2min, y2/y1+.05)
        ax[1].figure.canvas.draw()
    if y2/y1 <= y2min:
        ax[1].set_ylim(y2/y1-.05, y2max)
        ax[1].figure.canvas.draw()
    line1.set_data(xdata, y1data)
    line2.set_data(xdata, y2data)
    line3.set_data(xdata, y3data)
    line4.set_data(xdata, y4data)
    return line1, line2, line3, line4,

ani = FuncAnimation(fig, run, data_gen, blit=False, interval=2,
                              repeat=False, init_func=init, save_count=999)
plt.show()