from retrain_models import *
from trading_models import *
import pickle
import requests
import keras
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import StandardScaler
from keras.models import load_model


def loadModel(filename=""):
        '''
        Load model paramters for each stock using pickle file
        '''
        make_keras_picklable()
        with open(filename, "rb") as f:
            trading_model = pickle.load(f)
        print(trading_model)
        print(trading_model.stocks)
        print(trading_model.stock_clusters)
        return trading_model
dates = ['20170831', '20170902', '20170904', '20170905', '20170906', '20170907', '20170908',
			'20170911', '20170912', '20170913', '20170914', '20170915']
unscrambler = pd.read_csv('unscrambler.csv',index_col=0)
for i in range(0,len(dates)-1):
	model_path = 'saved_models/clustered_minmax_lstm_model_1_10_clusters'+dates[i]+'.pkl'

	#model_path ='clustered_minmax_lstm_model_1_10_clusters.pkl'

	trading_model = loadModel(filename=model_path)

	stocks = trading_model.stocks
	# ['AGW','AIO','BSN','CHV','CUN','DCO',
	#          'FFA','FFS','IES','ILW','IMC','IUQ',
	#          'IYU','JYW','KAA','KKG','KRZ','LPQ',
	#          'MUF','NYO','OMP','PFK','PQS','PUO',
	#          'QRK','TWK','WAG','XPV','XYR','YHW','ZLX']

	daterange = ['20170801','20170831']

	dset = '/Users/chandinijain/Auquan/qq2solver-data/historicalData/unscrambledData/testData3'


	volumepath = '/Users/chandinijain/Auquan/qq2solver-data/historicalData/cleanDataVolume'

	problem1 = TrainModel(stocks, daterange, dset, '', trading_model.stock_clusters, unscrambler, volumepath, filterData=True)
	    # problem1.createClusters(num_clusters=10)
	    # problem1.createFairClusters(num_clusters=6)
	path = '/Users/chandinijain/Auquan/qq2solver-data/historicalData/unscrambledData/OSDataP1'
	updaterange = ['20170901',dates[i+1]]
	initial_epoch = 5*(i+1)
	problem1.updateData(path, updaterange, unscrambler, volumepath, filterData=True)
	problem1.computeModelParams(trading_model, epochs=initial_epoch+5, batch_size=64, validation_split=0.05, initial_epoch=initial_epoch, update=True, 
				name='clustered_minmax_lstm_model_1_10_clusters'+updaterange[1]+'.pkl')