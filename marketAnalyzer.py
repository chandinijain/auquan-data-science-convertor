import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta as td
import os
import numpy as np

def frange(start, stop, step):
	i = start
	r = []
	while i < stop:
		r += [i]
		i += step

	return r
  

runLogFolder = 'runLog_20180227_172025'

# scrambler = pd.read_csv('scrambler.csv',index_col=0) 

# data = pd.read_csv('runLogs/'+runLogFolder+'/'+'marketFeatures.csv',index_col=0, parse_dates=True)
# predictions = data['predictionString'].apply(lambda x: x.split(', '))
# df = pd.DataFrame(predictions.values.tolist(), index=data.index).astype(float)
# pnl = data['pnlString'].apply(lambda x: x.split(', '))
# pnldf = pd.DataFrame(pnl.values.tolist(), index=data.index).astype(float)
# position = data['positionString'].apply(lambda x: x.split(', '))
# positiondf = pd.DataFrame(position.values.tolist(), index=data.index).astype(float)
dataset = 'OSData2P1'
unscrambler = pd.read_csv('unscrambler.csv',index_col=0) 
stocks = ['AGW','AIO','AUZ','BSN','BWU','CBT','CHV','CSR','CUN','CYD','DCO','DFZ',
        'DVV','DYE','EGV','FCY','FFA','FFS','FKI','FRE','FUR','GFQ','GGK','GYJ',
        'GYV','HHK','IES','ILW','IMC','IUE','IUQ','IYU','JSG','JYW','KAA','KKG',
        'KMV','KRZ','LDU','LKB','LPQ','MAS','MQK','MUF','NDG','NSL','NYO','OED',
        'OGU','OMP','PFK','PLX','PMS','PQS','PUO','QRK','SCN','SVG','TGI','TWK',
        'UBF','UWC','UWD','VML','VND','VSL','VTN','WAG','WTJ','XAD','XCS','XFD',
        'XIT','XPV','XYR','XZR','YGC','YHW','YUZ','ZEW','ZLX']

print(unscrambler.loc[stocks,'Name'])
analysis = pd.DataFrame(index=unscrambler.loc[stocks,'Name'], columns=['Opp Avg', 'Opp 0.5', 'Opp 0.7', 'Opp 0.9', 'Stk Spd', 'Fut Spd', 'Cnt P', '% P'])
period = 10
print(analysis)
for i in range(len(stocks)):
	data = pd.read_csv('historicalData/'+dataset+'/'+unscrambler.loc[stocks[i],'Name']+'.csv',index_col=0, parse_dates=True)
	# df.set_index(data.index, inplace=True)
	# pnldf.set_index(data.index, inplace=True)
	# positiondf.set_index(data.index, inplace=True)
	print('Plotting %s'%unscrambler.loc[stocks[i],'Name'])
	data['FV'] = data.basis.rolling(period).mean().shift(-period)
	diff = data['FV']-data.basis
	z = (diff/(data.basis.rolling(60).std()))[500:]
	z = z.replace([np.inf, -np.inf], np.nan).dropna()
	qnt = diff.abs().quantile([0.5, 0.7, 0.9])
	stk = (data['stockTopAskPrice'] - data['stockTopBidPrice']).mean()/2.0
	fut = (data['futureTopAskPrice'] - data['futureTopBidPrice']).mean()/2.0
	profits = diff.abs() - 1.5 *((data['stockTopAskPrice'] - data['stockTopBidPrice']).mean() + (data['futureTopAskPrice'] - data['futureTopBidPrice']).mean())/2.0
	analysis.loc[unscrambler.loc[stocks[i],'Name'],:] = [diff.abs().mean(), qnt.iloc[0], qnt.iloc[1], qnt.iloc[2], stk, fut, profits[profits>0].count(), profits[profits>0].count()/float(len(profits))]
	print('Avg Opportunity:%.3f, Avg Stock Spread:%.3f, Avg Future Spread:%.3f'%(diff.abs().mean(), stk, fut) 
											)
	print(diff.abs().quantile([0.3, 0.5, 0.7, 0.9]))
	print(profits[profits>0].count())
	print(profits[profits>0].count()/float(len(profits)))
	# print(z[z < - 1.5 ].count()/float(len(z)), z[z > 1.5 ].count()/float(len(z)))
	# print(z[z < - 1 ].count()/float(len(z)), z[z > 1 ].count()/float(len(z)))
	# print(z[z < - 0.5 ].count()/float(len(z)), z[z > 0.5 ].count()/float(len(z)))
	# print(np.mean(data.basis.rolling(60).std()))
	# plt.plot(data['FV'].iloc[:-5],'.')
	plt.plot(data.FairValue)
	plt.plot(data.basis.rolling(5).mean().shift(-5).iloc[500:-5])
	plt.plot(data.basis.iloc[:-5])
	plt.show()
	# plt.plot(diff[500:])
	# plt.plot(0.25 + diff.rolling(60).mean()[500:])#(100*data.basis.rolling(375).std()/375.0)[500:] 
	# plt.plot(-0.25 + (data['FV']-data.basis).rolling(60).mean()[500:])
	# plt.show()

	# fig, ax1 = plt.subplots()
	# ax1.plot(pnldf[i], 'b.')
	# ax2 = ax1.twinx()
	# ax2.plot(positiondf[i], 'r.')
	# plt.show()
	
	# print(z.hist(bins = frange(np.min(z), np.max(z), (np.max(z)-np.min(z))/20.0), align = 'mid', density=True, cumulative=True))
	# plt.hist(z, bins = frange(np.min(z), np.max(z), (np.max(z)-np.min(z))/20.0), align = 'mid', density=True, cumulative=False)
	# plt.show()

# names = next(os.walk('runLogs/'+runLogFolder))[2]
# print(names)

# for i in names:
# 	if i=='marketFeatures.csv':
# 		continue
# 	try:

# 		data = pd.read_csv('runLogs/'+runLogFolder+'/'+i,index_col=0)
# 		predictions = data['predictionString'].apply(lambda x: x.split(', ')).values.tolist().astype(float)
		

# 		# print('Plotting %s'%i)
# 		# plt.plot(data.prediction.rolling(50).mean()[1000:])
# 		# # plt.plot(data.FairValue)
# 		# plt.plot(data.basis.rolling(50).mean()[1000:])
# 		# plt.show()
# 		# plt.plot((data.prediction-data.basis)[1000:])
# 		# plt.show()
# 	except:

# 		continue
# 	# frames = [data1, data2, data3, data4]
# 	# result = pd.concat(frames)
# 	# result.sort_index(inplace=True)
# 	# # print(result[result.index.duplicated(keep=False)])
# 	# result.fillna(method='pad',inplace=True)
	
print(analysis)
analysis.to_csv('analysis%s'%period, float_format='%.3f')
