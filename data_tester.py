import pandas as pd
import numpy as np
import os
import datetime as dt
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, TH
import sklearn.metrics as sm

def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

for root, dirs, files in os.walk('/Users/chandinijain/Auquan/DataQQ3'):
	print(dirs)

root = '/Users/chandinijain/Auquan/DataQQ3'
dirs = 'QQData'
files = next(os.walk('%s/%s'%(root,dirs)))[2]
useful = []
acc = None
alldata = pd.DataFrame(columns=files[1:])
alldelta = pd.DataFrame(columns=files[1:])
allprices = pd.DataFrame(columns=files[1:])
for name in files[1:]:
		print(name)
		# dateparse = lambda dates: [pd.datetime.strptime(d, '%d/%m/%y %H:%M:%S') for d in dates]

		data = pd.read_csv(os.path.join(root, dirs, name), index_col=0, parse_dates=True)#, parse_dates=[['DATE', 'TIME']], date_parser=dateparse)	
		data = data[data['ADJCLOSE']!=0]
		returns = data['ADJCLOSE']/data['ADJCLOSE'].shift(freq='1D',periods=1) -1
		# import pdb;pdb.set_trace()
		delta = np.sign(returns) + np.sign(returns.shift(freq='1D',periods=1)) # 0 is mean reversion, -2 or 2 is momentum
		allprices[name] = data['ADJCLOSE']
		alldata[name] = returns
		alldelta[name] = delta.abs()/2
alldata.dropna(inplace=True)
alldelta.dropna(inplace=True)
# #ranking method 1
# ranks = alldata.rank(axis=1)
# alldata[(ranks>10) & (ranks<100)] = 0
# alldata[(ranks<10)] = -alldata
# dailyreturns = alldata.sum(axis=1).resample('1D').last().dropna()
# print(dailyreturns.mean()*250)
# # dailyreturns = (alldata*allprices.shift(freq='1D',periods=1)).sum(axis=1).resample('1D').last().dropna()
# # print(dailyreturns.mean()*250)
# plt.plot(dailyreturns)
# plt.show()

temp = alldelta.copy().astype(int)
n = 10
results = range(25)
#ranking method 2
for i in range(25):
	
	alldelta = (1-temp).shift(freq='1D',periods=-1) #pd.DataFrame(np.random.randint(0,2,size=(len(alldata.index), len(files)-1)), index = alldata.index, columns=files[1:])
	alldelta[alldelta.index.isin(temp.shift(freq='1D',periods=-1).sample(frac=0.65).index)] = temp.shift(freq='1D',periods=-1)
	## alldelta is a time x stocks array with prediction for momentum(1) or mean reversion(0) at every time t for time t+1D 
	allranks = alldata.rank(axis=1)
	palldata = alldata.copy()
	palldata[alldata>0] = 0

	momdata = (palldata*alldelta).dropna() #.shift(freq='1D',periods=-1)
	## momdata is a time x stocks array with returns for stocks at t whose prediction is momentum(1) for time t+1D 
	momranks = momdata.replace(0, np.nan).rank(axis=1,method='average', numeric_only=True, na_option='keep', ascending=True)
	## momranks is a time x stocks array with return ranking for stocks at t whose prediction is momentum(1) at time t+1 
	momranks = momranks.shift(freq='1D',periods=1)
	## we shift momranks by +1D to see profits at t+1 from selling stocks with lowest returns at time t whose prediction is momentum(1) for time t+1D 
	meandata = (palldata*(1-alldelta)).dropna()
	meanranks = meandata.replace(0, np.nan).rank(axis=1,method='average', numeric_only=True, na_option='keep', ascending=True).shift(freq='1D',periods=1)
	newdata = pd.DataFrame(index = alldata.index, columns=alldata.columns)
	newdata[(meanranks>n)] = 0
	newdata[(meanranks<n)] = alldata
	newdata[(meanranks.isna())] = 0
	dailyreturns1 = newdata.sum(axis=1).resample('1D').last().dropna()
	print('Top 10 mean reversion', dailyreturns1.mean()*250)
	# import pdb;pdb.set_trace()
	newdata[(momranks>n)] = 0
	newdata[(momranks<n)] = alldata
	newdata[(momranks.isna())] = 0
	dailyreturns2 = newdata.sum(axis=1).resample('1D').last().dropna()
	print('Top 10 momentum', dailyreturns2.mean()*250)

	palldata = alldata.copy()
	palldata[alldata<0] = 0
	momdata = (palldata*alldelta).dropna()
	momranks = momdata.replace(0, np.nan).rank(axis=1,method='average', numeric_only=True, na_option='keep', ascending=False).shift(freq='1D',periods=1)
	meandata = (palldata*(1-alldelta)).dropna()
	meanranks = meandata.replace(0, np.nan).rank(axis=1,method='average', numeric_only=True, na_option='keep', ascending=False).shift(freq='1D',periods=1)
	newdata = alldata.copy()
	newdata[(meanranks>n)] = 0
	newdata[(meanranks.isna())] = 0
	dailyreturns3 = newdata.sum(axis=1).resample('1D').last().dropna()
	print('Bot 10 mean reversion', dailyreturns3.mean()*250)
	newdata[(momranks>n)] = 0
	newdata[(momranks<n)] = alldata
	newdata[(momranks.isna())] = 0
	dailyreturns4 = newdata.sum(axis=1).resample('1D').last().dropna()
	print('Bot 10 momentum', dailyreturns4.mean()*250)
	dailyreturns = dailyreturns1-dailyreturns2-dailyreturns3+dailyreturns4
	print(i, ' Net', dailyreturns.mean()*250/(4*n))
	results[i] = dailyreturns.mean()*250/(4*n)
	# print(sm.classification.type_of_target(temp.values))
	# print(sm.classification.type_of_target(alldelta.values))
	print(temp.iloc[-70:], alldelta.iloc[-140:-70])
	print('matthews_corrcoef',sm.matthews_corrcoef(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('confusion_matrix',sm.confusion_matrix(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('accuracy_score',sm.accuracy_score(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('classification_report',sm.classification_report(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('f1_score',sm.f1_score(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	# print(sm.fbeta_score(temp.values.ravel(), alldelta.values.ravel()))
	print('hamming_loss',sm.hamming_loss(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('jaccard_similarity_score',sm.jaccard_similarity_score(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('log_loss',sm.log_loss(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('precision_recall_fscore_support',sm.precision_recall_fscore_support(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('precision_score',sm.precision_score(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('recall_score',sm.recall_score(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))
	print('zero_one_loss',sm.zero_one_loss(temp.shift(freq='1D',periods=-1).values.ravel(), alldelta.values.ravel()))

print(results)
print(sum(results) / float(len(results)))

plt.plot(results)
plt.show()


#ranking method 3
# momranks = (alldata*alldelta).rank(axis=1).shift(freq='1D',periods=1)
# meanranks = 
# alldata[(ranks>10) & (ranks<100)] = 0
# # import pdb;pdb.set_trace()
# alldata[(ranks<10)] = -alldata
# alldata[(alldelta==0)] = -alldata
# dailyreturns = alldata.sum(axis=1).resample('1D').last().dropna()
# print(dailyreturns.mean()*250)

# plt.plot(dailyreturns)
# plt.show()



