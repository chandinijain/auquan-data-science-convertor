from backtester.trading_system_parameters import TradingSystemParameters
from backtester.executionSystem.basis_execution_system import BasisExecutionSystem
from backtester.constants import *
from backtester.features.feature import Feature
from backtester.logger import *
from backtester.trading_system import TradingSystem
import pandas as pd
from trading_models import *
import numpy as np

import pickle
import requests
import keras
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
import types
import tempfile
import datetime


class LSTMTradingFunctions():

    def __init__(self, stocks, model=None,model_path='', startdate='20170901', update=True):
        self.stocks = stocks
        self.count = 0
        self.__featureKeys = []
                        #'KFW': 1, 'VKE': 1, 'IFL': 8
        self.X = []     # Container to store features for update
        self.Y = []     # Container to store labels for update
        self.period = 5*60/30
        self.halflife = 20*60/30
        self.update = update
        self.FLAG = True
        self.today = startdate
        if model is None:
          self.loadModel(filename=model_path)
        else:
          self.trading_model = model
        self.unscrambler = pd.read_csv('/home/cjain/live_order_placer/unscrambler.csv', index_col=0)

    def getSymbolsToTrade(self):
        print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]

    def loadModel(self, filename=""):
        '''
        Load model paramters for each stock using pickle file
        '''
        make_keras_picklable()
        with open(filename, "rb") as f:
            self.trading_model = pickle.load(f)
        print(self.trading_model)
        print(self.trading_model.stocks)
        print(self.trading_model.stock_clusters)

    def evaluateModel(self, outf=""):
        err_df = pd.DataFrame(index=list(np.arange(self.trading_model.num_clusters))+self.stocks, columns=["name", "mse", "rmse", "clusterID"])
        X = np.array(self.X)
        Y = np.array(self.Y)
        print(X.shape, Y.shape)
        for s, stock in enumerate(self.stocks):
            mse = self.trading_model.models[self.trading_model.stock_clusters[stock]].evaluate(
                        x=np.expand_dims(self.trading_model.normalizers[self.trading_model.stock_clusters[stock]].transform(X[:,:,s]), axis=1),
                        y=Y[:,s],
                        batch_size=1)
            rmse = np.sqrt(mse)
            err_df.loc[stock] = [self.unscrambler.loc[stock].values[0], mse, rmse, self.trading_model.stock_clusters[stock]]
        for cid in range(self.trading_model.num_clusters):
            x = [X[:,:,s] for s,stock in enumerate(self.stocks) if self.trading_model.stock_clusters[stock] == cid]
            if len(x) == 0:
                continue
            x = np.vstack(x)
            y = np.vstack([Y[:,s].reshape(-1,1) for s,stock in enumerate(self.stocks) if self.trading_model.stock_clusters[stock] == cid])
            cluster_mse = self.trading_model.models[cid].evaluate(
                        x=np.expand_dims(self.trading_model.normalizers[cid].transform(x), axis=1),
                        y=y,
                        batch_size=1)
            cluster_rmse = np.sqrt(cluster_mse)
            err_df.loc[cid] = ["cluster_%s" % cid, cluster_mse, cluster_rmse, cid]
        print(err_df)
        if outf != "":
            err_df.to_csv(outf)


    def getInstrumentFeatureConfigDicts(self):

        generate_features = []
        featureList = []
        real_features = ['stockVWAP', 'futureVWAP', 'basis', 'stockTopBidVol', 'stockTopAskVol', 'stockTopBidPrice',
                        'stockTopAskPrice', 'futureTopBidVol', 'futureTopAskVol', 'futureTopBidPrice', 'futureTopAskPrice',
                        'stockNextBidVol', 'stockNextAskVol', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidVol',
                        'futureNextAskVol', 'futureNextBidPrice', 'futureNextAskPrice', 'stockTotalBidVol',
                        'stockTotalAskVol', 'futureTotalBidVol', 'futureTotalAskVol', 'stockAverageBidPrice',
                        'stockAverageAskPrice', 'futureAverageBidPrice', 'futureAverageAskPrice',
                        'totalTradedStockVolume', 'totalTradedFutureVolume', 'totalTradedStockValue',
                        'totalTradedFutureValue']
        generate_features = []
        for rf in real_features:
            ma = {'featureKey': 'ma' + rf,
                     'featureId': 'moving_average',
                     'params': {'period': self.period,
                                'featureName': rf}}
            moving_min = {'featureKey': 'moving_min' +rf,
                        'featureId': 'moving_min',
                        'params': {'period': self.period,
                                   'featureName': rf}}
            moving_max = {'featureKey': 'moving_max' +rf,
                        'featureId': 'moving_max',
                        'params': {'period': self.period,
                                   'featureName': rf}}
            sdev = {'featureKey': 'sdev' + rf,
                      'featureId': 'moving_sdev',
                      'params': {'period': self.period,
                                 'featureName': rf}}
            expma = {'featureKey': 'expma' + rf,
                   'featureId': 'exponential_moving_average',
                   'params': {'period': self.halflife,
                                'featureName': rf}}
            # moving_sum = {'featureKey': 'moving_sum' + rf,
            #           'featureId': 'moving_sum',
            #           'params': {'period': self.period,
            #                      'featureName': rf}}

            generate_features += [ma, moving_min, moving_max, sdev, expma]
            featureList += [f_id + rf for f_id in ['ma', 'moving_min', 'moving_max', 'sdev', 'expma']]

        moving_sum = {'featureKey': 'moving_sum',
                  'featureId': 'moving_sum',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        delay = {'featureKey': 'delay',
                  'featureId': 'delay',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        diff = {'featureKey': 'diff',
                  'featureId': 'difference',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        rank = {'featureKey': 'rank',
                  'featureId': 'rank',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        scale = {'featureKey': 'scale',
                  'featureId': 'scale',
                  'params': {'period': self.period,
                             'featureName': 'basis', 'scale': 3}}

        generate_features += [moving_sum, delay, diff, rank, scale]
        featureList += ['moving_sum', 'delay', 'diff', 'rank', 'scale']

        fairValueDict = {'featureKey': 'FairValue',
                     'featureId': 'moving_average',
                     'params': {'period': self.period,
                                'featureName': 'basis'}}

        generate_features += [fairValueDict]
        self.setFeatureKeys(featureList)
        return generate_features

    def getPrediction(self, time, updateNum, instrumentManager):
        # holder for all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        # dataframe for a historical instrument feature (ma_5 in this case). The index is the timestamps
        # atmost upto lookback data points. The columns of this dataframe are the stock symbols/instrumentIds.
        print("Updatenum:", updateNum)
        predictedFairValue = []
        x = []
        training_x = []
        for f in self.__featureKeys:
            data = lookbackInstrumentFeatures.getFeatureDf(f).fillna(0)
            x.append(np.array(data.iloc[-1]))
            if len(data) > self.period:
                training_x.append(np.array(data.iloc[-self.period-1]))

        if self.update:# and len(training_x)>0:
            fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')
            # self.X.append(np.array(training_x))                        # shape = d x s
            self.X.append(np.array(x))                        # shape = d x s
            self.Y.append(np.array(fairValueData.iloc[-1]))   # shape = 1 x s
            if (self.today != datetime.strftime(time.date(),'YYYYMMDD')):
                self.today = datetime.strftime(time.date(),'YYYYMMDD')
                print(self.today, "Updating model...")
                self.trading_model.update(self.stocks, np.array(self.X), np.array(self.Y), epochs=5, save=True, model_name="test_model_1.pkl")

        x = np.array(x)     # shape = d x s
        # if self.FLAG:
            # self.FLAG = False
            # print("Filling lookback...")
            # self.trading_model.updateLookback(x, fill=True)
        predictedFairValue = self.trading_model.predict(x, self.stocks)
        #print(predictedFairValue)
        idx = [self.unscrambler.loc[i, 'Name'] for i in self.stocks]
        predictedFairValue.rename(lambda x: self.unscrambler.loc[x, 'Name'], inplace=True) #= pd.Series(predictedFairValue, index=idx)
        #print(predictedFairValue)
        # fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')
        # print(np.array(fairValueData.iloc[-1]))
        # self.trading_model.updateLookback(x)

        # Returns a series with index as all the instrumentIds. This returns the value of the feature at the last
        # time update.
        #print(np.transpose(x))

        return predictedFairValue

    def getFeatureKeys(self):
        return self.__featureKeys

    def setFeatureKeys(self, featureList):
        self.__featureKeys = featureList
