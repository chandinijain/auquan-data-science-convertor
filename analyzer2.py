import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta as td
import os
import numpy as np

def calc(str, a,b,threshold=0.2):
	c = a[abs(a)>threshold].corr(b[abs(a)>threshold])
	print(str + ' %.3f'%c)
	if abs(c)>0.1:
		plt.plot(b[abs(a)>threshold], a[abs(a)>threshold], '.')
		plt.ylim(-1, 1)
		plt.xlim(-2, 2)
		plt.show()

pd.options.mode.use_inf_as_na = True

dataset = 'OSData2P1'
unscrambler = pd.read_csv('unscrambler.csv',index_col=0) 
stocks = ['AGW','AIO','AUZ','BSN','BWU','CBT','CHV','CSR','CUN','CYD','DCO','DFZ',
        'DVV','DYE','EGV','FCY','FFA','FFS','FKI','FRE','FUR','GFQ','GGK','GYJ',
        'GYV','HHK','IES','ILW','IMC','IUE','IUQ','IYU','JSG','JYW','KAA','KKG',
        'KMV','KRZ','LDU','LKB','LPQ','MAS','MQK','MUF','NDG','NSL','NYO','OED',
        'OGU','OMP','PFK','PLX','PMS','PQS','PUO','QRK','SCN','SVG','TGI','TWK',
        'UBF','UWC','UWD','VML','VND','VSL','VTN','WAG','WTJ','XAD','XCS','XFD',
        'XIT','XPV','XYR','XZR','YGC','YHW','YUZ','ZEW','ZLX']

print(unscrambler.loc[stocks,'Name'])
analysis = pd.DataFrame(index=unscrambler.loc[stocks,'Name'], columns=['Opp Avg', 'Opp 0.5', 'Opp 0.7', 'Opp 0.9', 'Stk Spd', 'Fut Spd', 'Cnt P', '% P'])
period = 5
for i in range(len(stocks)):
	data = pd.read_csv('historicalData/'+dataset+'/'+unscrambler.loc[stocks[i],'Name']+'.csv',index_col=0, parse_dates=True)

	print('Plotting %s'%unscrambler.loc[stocks[i],'Name'])
	data['FV'] = data.basis.rolling(period).mean()
	diffF = data.shift(-period)-data
	diffB = data/data.shift(period) - 1
	param = 'basis'
	calc('Correl with SP ', diffF[param].loc[abs(diffF[param])>0.2], diffB['stockVWAP'].loc[abs(diffF[param])>0.2])
	calc('Correl with FP ', diffF[param].loc[abs(diffF[param])>0.2], diffB['futureVWAP'].loc[abs(diffF[param])>0.2])
	s = (diffB['stockVWAP']/diffB['futureVWAP']).fillna(0)
	calc('Correl with SP/FP ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])


	calc('Correl with SBidVol ', diffF[param].loc[abs(diffF[param])>0.2], diffB['stockTotalBidVol'].loc[abs(diffF[param])>0.2])
	calc('Correl with FBidVol ', diffF[param].loc[abs(diffF[param])>0.2], diffB['futureTotalBidVol'].loc[abs(diffF[param])>0.2])
	s = (diffB['stockTotalBidVol']/diffB['futureTotalBidVol']).fillna(0)
	calc('Correl with SBidVol/FBidVol ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])

	calc('Correl with SAskVol ', diffF[param].loc[abs(diffF[param])>0.2], diffB['stockTotalAskVol'].loc[abs(diffF[param])>0.2])
	calc('Correl with FAskVol ', diffF[param].loc[abs(diffF[param])>0.2], diffB['futureTotalAskVol'].loc[abs(diffF[param])>0.2])
	s = (diffB['stockTotalAskVol']/diffB['futureTotalAskVol']).fillna(0)
	calc('Correl with SAskVol/FAskVol ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])

	s = (diffB['stockTotalBidVol']/diffB['stockTotalAskVol']).fillna(0)
	calc('Correl with SBidVol/SAskVol ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])
	s = (diffB['futureTotalBidVol']/diffB['futureTotalAskVol']).fillna(0)
	calc('Correl with FBidVol/FAskVol ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])

	calc('Correl with SBidVol ', diffF[param].loc[abs(diffF[param])>0.2], diffB['stockTopBidVol'].loc[abs(diffF[param])>0.2])
	calc('Correl with FBidVol ', diffF[param].loc[abs(diffF[param])>0.2], diffB['futureTopBidVol'].loc[abs(diffF[param])>0.2])
	s = (diffB['stockTopBidVol']/diffB['futureTopBidVol']).fillna(0)
	calc('Correl with SBidVol/FBidVol ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])

	calc('Correl with SAskVol ', diffF[param].loc[abs(diffF[param])>0.2], diffB['stockTopAskVol'].loc[abs(diffF[param])>0.2])
	calc('Correl with FAskVol ', diffF[param].loc[abs(diffF[param])>0.2], diffB['futureTopAskVol'].loc[abs(diffF[param])>0.2])
	s = (diffB['stockTopAskVol']/diffB['futureTopAskVol']).fillna(0)
	calc('Correl with SAskVol/FAskVol ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])

	s = (diffB['stockTopBidVol']/diffB['stockTopAskVol']).fillna(0)
	calc('Correl with SBidVol/SAskVol ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])
	s = (diffB['futureTopBidVol']/diffB['futureTopAskVol']).fillna(0)
	calc('Correl with FBidVol/FAskVol ', diffF[param].loc[abs(diffF[param])>0.2], s.loc[abs(diffF[param])>0.2])




