import numpy as np
import scipy.stats


def findSecondMeasurement(firstMeasurement, standardDeviation):
	lisa = 110
	susan = 90

	##################################
	##          FILL ME IN          ##
	##################################
	mu1 = scipy.stats.norm(lisa, standardDeviation)
	mu2 = scipy.stats.norm(susan, standardDeviation)
	x = firstMeasurement
	z = mu1.pdf(x)/(mu1.pdf(x)+mu2.pdf(x)) 
	print(mu1.pdf(x), mu2.pdf(x), z)
	secondMeasurement = susan*(1-z)+lisa*(z)   # Replace me with your answer

	return round(secondMeasurement, 2)

if __name__ == "__main__":
	standardDeviation = 20
	firstMeasurement = 105
	print('For First Measurement %.2f, standard deviation %.2f, Expected second measurement: %.2f'%(firstMeasurement, standardDeviation\
		,findSecondMeasurement(firstMeasurement, standardDeviation)))
