### Here the data scientists define the features they want to use in their prediction models
### We've de-anonymized the feature names for code to be clear

def getInstrumentFeatureConfigDicts(self):

	real_features = ['basis', 'stockVWAP', 'futureVWAP', 'stockTopBidPrice', 'stockTopAskPrice', 'futureTopBidPrice',
						 'futureTopAskPrice', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidPrice',
						 'futureNextAskPrice', 'stockAverageBidPrice', 'stockAverageAskPrice', 'futureAverageBidPrice',
						 'futureAverageAskPrice']

	generate_features = []
	featureList = []
	for rf in real_features:
		ma_5 = {'featureKey': 'ma_5' + rf,
				'featureId': 'moving_average',
				'params': {'period': 5*60/10,
						   'featureName': rf}}
		expma = {'featureKey': 'expma' + rf,
				 'featureId': 'exponential_moving_average',
				 'params': {'period': 20*60/10,
							'featureName': rf}}
		sdev_5 = {'featureKey': 'sdev_5' + rf,
				  'featureId': 'moving_sdev',
				  'params': {'period': 5*60/10,
							 'featureName': rf}}

		generate_features += [ma_5, sdev_5, expma]
		featureList += [f_id + rf for f_id in ['ma_5', 'sdev_5', 'expma']]


	moving_min = {'featureKey': 'moving_min',
				  'featureId': 'moving_min',
				  'params': {'period': 5*60/10,
							 'featureName': 'basis'}}
	moving_sum = {'featureKey': 'moving_sum',
				  'featureId': 'moving_sum',
				  'params': {'period': 5*60/10,
							 'featureName': 'basis'}}
	delay = {'featureKey': 'delay',
			 'featureId': 'delay',
			 'params': {'period': 5*60/10,
						'featureName': 'basis'}}
	diff = {'featureKey': 'diff',
			'featureId': 'difference',
			'params': {'period': 5*60/10,
						 'featureName': 'basis'}}
	rank = {'featureKey': 'rank',
			'featureId': 'rank',
			'params': {'period': 5*60/10,
						 'featureName': 'basis'}}
	scale = {'featureKey': 'scale',
			 'featureId': 'scale',
			 'params': {'period': 5*60/10,
						'featureName': 'basis', 'scale': 3}}

	generate_features += [moving_min, moving_sum, delay, diff, rank, scale]
	featureList += ['moving_min', 'moving_sum', 'delay', 'diff', 'rank', 'scale']
	self.setFeatureKeys(featureList)

	ma1 = {'featureKey': 'ma_5',
						 'featureId': 'moving_average',
						 'params': {'period': 30*60/self.frequency,
												'featureName': 'basis'}}
	ma2 = {'featureKey': 'ma_10',
						 'featureId': 'moving_average',
						 'params': {'period': 60*60/self.frequency,
												'featureName': 'stockTopAskPrice'}}
	ma3 = {'featureKey': 'ma_90',
						 'featureId': 'moving_average',
						 'params': {'period': 10*60/self.frequency,
												'featureName': 'basis'}}
	expma = {'featureKey': 'expma20',
					 'featureId': 'exponential_moving_average',
					 'params': {'period': 90*60/self.frequency,
												'featureName': 'basis'}}
	sdevDict = {'featureKey': 'sdev_5',
							'featureId': 'moving_sdev',
							'params': {'period': 30*60/self.frequency,
												 'featureName': 'basis'}}
	diff = {'featureKey': 'price_change',
					'featureId': 'difference',
					'params': {'period': 30*60/self.frequency,
										 'featureName': 'basis'}}
	macd = {'featureKey': 'macd',
					'featureId': 'macd',
					'params': {'period1': 30*60/self.frequency,
										 'period2': 60*60/self.frequency,
										 'featureName': 'basis'}}
	bbandlower = {'featureKey': 'lowerBB',
										'featureId': 'bollinger_bands_lower',
										'params': {'period': 90*60/self.frequency,
															 'featureName': 'stockVWAP'}}
	bbandupper = {'featureKey': 'upperBB',
										'featureId': 'bollinger_bands_upper',
										'params': {'period': 90*60/self.frequency,
															 'featureName': 'stockVWAP'}}
	bbandlower1 = {'featureKey': 'lowerBBbasis',
										'featureId': 'bollinger_bands_lower',
										'params': {'period': 90*60/self.frequency,
															 'featureName': 'basis'}}
	bbandupper1 = {'featureKey': 'upperBBbasis',
										'featureId': 'bollinger_bands_upper',
										'params': {'period': 90*60/self.frequency,
															 'featureName': 'basis'}}
	rsi = {'featureKey': 'rsi',
				 'featureId': 'rsi',
				 'params': {'period': 30*60/self.frequency,
										'featureName': 'basis'}}

	return generate_features + [ma1, ma2, ma3, expma, sdevDict, diff, macd,
								bbandlower, bbandupper, rsi, bbandlower1, bbandupper1]

### Here the data scientists use the features they created and build a prediction model
### We've de-anonymized the feature names for code to be clear

### This model used a combination of three models, the models are displayed below

#### 1. ExtraTrees Regressor:
def getPrediction(self, time, updateNum, instrumentManager):
		# holder for all the instrument features
		lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
		features = ['basis', 'stockVWAP', 'futureVWAP', #'benchmark_score',
					'ma_1','ma_2','ma_3','exponential_moving_average','sdev_5',
					'price_change', 'macd', 'lowerBB', 'upperBB', 'rsi', 'pctB' ]

		basis = lookbackInstrumentFeatures.getFeatureDf('basis')
		stockVWAP = lookbackInstrumentFeatures.getFeatureDf('stockVWAP')
		futureVWAP = lookbackInstrumentFeatures.getFeatureDf('futureVWAP')
		ma_5 = lookbackInstrumentFeatures.getFeatureDf('ma_5')
		ma_10 = lookbackInstrumentFeatures.getFeatureDf('ma_10')
		ma_90 = lookbackInstrumentFeatures.getFeatureDf('ma_90')
		expma20 = lookbackInstrumentFeatures.getFeatureDf('expma20')
		sdev_5 =lookbackInstrumentFeatures.getFeatureDf('sdev_5')
		price_change =lookbackInstrumentFeatures.getFeatureDf('price_change')
		macd =lookbackInstrumentFeatures.getFeatureDf('macd')
		lowerBB =lookbackInstrumentFeatures.getFeatureDf('lowerBB')
		upperBB =lookbackInstrumentFeatures.getFeatureDf('upperBB')
		rsi =lookbackInstrumentFeatures.getFeatureDf('rsi')
		lowerBB1 =lookbackInstrumentFeatures.getFeatureDf('lowerBBbasis')
		upperBB1 =lookbackInstrumentFeatures.getFeatureDf('upperBBbasis')

		stocks = list(ma_5.columns)

		ma5 = ma_5.iloc[-1]
		percentB = (basis - lowerBB1)/(lowerBB1 - upperBB1)

		if len(ma_5.index) > 90*60/self.frequency:
			out = pd.Series()
			for i in stocks:
				X=pd.concat([basis[i],stockVWAP[i], futureVWAP[i], #benchmark_score[i],
							 ma_5[i], ma_10[i], ma_90[i], expma20[i], sdev_5[i], price_change[i],
							 macd[i], lowerBB[i], upperBB[i], rsi[i], percentB[i]],
							axis=1, keys=features)
				X.fillna(method='ffill', inplace=True)
				#print(X.iloc[-1])
				#print(basis)
				from sklearn.preprocessing import Imputer
				X = Imputer().fit_transform(X)

				y = ma_5[i]

				#making testing training
				X_test = X[-1:]
				if self.count%(5*60/self.frequency) == 0:
					X_train = X[:-(90*60/self.frequency)]
					y_train = y[(90*60/self.frequency):]
					self.model[i].fit(X_train,y_train.values.ravel())

				p = self.model[i].predict(X_test)

				out = out.append(pd.Series([float(p)], index=[i]))
		else:
				out = expma20.iloc[-1]

		self.count +=1

		return out

#### 2. Predictions using a pre-trained LSTM Model. Functions to build and train the LSTM Model follow
def loadModelParams(self, filename=""):
		"""
		Load model paramters for each stock using pickle file
		"""
		with open(filename, "rb") as f:
			self.stock_cluster, self.normalizer = pickle.load(f)

		self.model = dict()
		for i in range(6):
			self.model[i] = load_model("problem1_lstm_%d.h5" % i)


def getPrediction(self, time, updateNum, instrumentManager):

	lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

	predictedFairValue = []
	x_star = []             # Data point at time t (whose FairValue will be predicted)
	for f in self.__featureKeys:
		data = lookbackInstrumentFeatures.getFeatureDf(f).fillna(0)
		x_star.append(np.array(data.iloc[-1]))
	x_star = np.array(x_star)                                      # shape = d x s
	x_star = np.vstack((x_star, np.power(x_star[0, :], 2)))          # shape = (d+1) x s

	predictedFairValue = [self.model[self.stock_cluster[stock]].predict(np.expand_dims(self.normalizer[self.stock_cluster[stock]].transform(np.array([x_star[:, i]])), axis=1), batch_size=1)[
		0][0] for i, stock in enumerate(self.stocks)]
	print(predictedFairValue)

	return predictedFairValue


#### Helper Functions #####

#### 1. Building an LSTM Model


class TradingModel(object):
	"""Base class for trading models"""
	def __init__(self, stocks, stock_clusters, num_features, scaler="MinMaxScaler", feature_range=None):
		"""
		Args:
				stocks : List of stock names
				stock_clusters : Dict with keys as stock name and value is the cluster number of that stock
				scaler : Feature scalers- "MinMaxScaler", "StandardScaler"
				feature_range : range of features after scaling (optional parameter in MinMaxScaler)
		"""
		self.stocks = stocks
		self.stock_clusters = stock_clusters
		self.num_features = num_features
		self.num_clusters = len(set(self.stock_clusters.values()))
		scaler_method = getattr(scalers, scaler)
		self.normalizers = {cid : scaler_method(feature_range=feature_range) if feature_range else scaler_method() for cid in range(self.num_clusters)}
		self.models = {cid : None for cid in range(self.num_clusters)}
		try:
				with open('history.pkl', 'rb') as fp:
						self.history = pickle.load(fp)
		except:
				self.history = {cid : History() for cid in range(self.num_clusters)}
		self.stock_indexes = dict()
		for cid in range(self.num_clusters):
				self.stock_indexes[cid] = [idx for idx, stock in enumerate(stocks) if self.stock_clusters[stock] == cid]

	def save_model(self, model_name):
		make_keras_picklable()
		model_name = "%s.pkl" % (model_name) if model_name != "" else "checkpoint.pkl"
		with open(model_name, 'wb') as f:
				pickle.dump(self, f)
		with open('%s_history.pkl' % model_name, 'wb') as fp:
				pickle.dump(self.history, fp, protocol=pickle.HIGHEST_PROTOCOL)

	def update(self, stocks, features, labels, epochs=5, batch_size=64, initial_epoch=0, save=True, model_name="", validation_split=0.05):
		"""
		Args:
				features : Numpy array of shape (N x d x s)
									 where d = dimension of a data point, N = Number of data points and s = Number of stocks
				labels : Numpy array of shape (N x s)
								 where N = Number of data points and s = Number of stocks
		"""
		# assert features.shape[2] == len(self.stocks)
		feature_dict = {stock : features[:, :, s] for s, stock in enumerate(stocks)}
		label_dict = {stock : labels[:, s] for s, stock in enumerate(stocks)}
		self.fit(stocks, feature_dict, label_dict, epochs, batch_size, validation_split, initial_epoch=initial_epoch, update=True)
		if save:
				self.save_model(model_name)



class LSTM_model_1(TradingModel):
	"""Stateless LSTM with no look-back"""
	def __init__(self, stocks, stock_clusters, num_features, scaler="MinMaxScaler", feature_range=None):
		super(LSTM_model_1, self).__init__(stocks, stock_clusters, num_features, scaler, feature_range)
		layers = [self.num_features, 50, 100, 1]
		for cid in range(self.num_clusters):
				self.models[cid] = build_model(layers, batch_size=None, timesteps=1, stateful=False)

	def fit(self, stocks, features, labels, epochs=30, batch_size=64, validation_split=0.05, initial_epoch=0, update=False, name=""):
		"""
		Args:
				features : Dict with keys as stock name and value as Numpy array of shape (N x d)
									 where d = dimension of a data point and N = Number of data points
				labels : Dict with keys as stock name and value as Numpy array of shape (N x 1)
								 where N = Number of data points
		"""
		try:
				with open('%s_history.pkl' % name, 'rb') as fp:
						self.history = pickle.load(fp)
		except:
				self.history = {cid : History() for cid in range(self.num_clusters)}

		for cid in range(self.num_clusters):
			X = [features[stock] for stock in stocks if self.stock_clusters[stock] == cid]
			if len(X) == 0:
					continue
			if not update:
					self.normalizers[cid].fit(np.vstack(X))
			X = [self.normalizers[cid].transform(x) for x in X]
			X = np.vstack([np.expand_dims(x, axis=1) for x in X])
			Y = [labels[stock] for stock in stocks if self.stock_clusters[stock] == cid]
			Y = np.vstack([y.reshape(-1, 1) for y in Y])
			print("CID:", cid, "X shape:", X.shape, "Y shape:", Y.shape)

			history = self.history[cid]

			checkpointer = ModelCheckpoint(filepath='checkpoints/%s_weights.h5' % name , verbose=1, save_best_only=True, period=1)
			self.models[cid].fit(X, Y,
								 batch_size=batch_size,
								 epochs=epochs,
								 shuffle=True,
								 validation_split=validation_split,
								 initial_epoch=initial_epoch,
								 callbacks=[checkpointer, history])
			print(history.epoch, history.history)

			del self.models[cid]
			self.models[cid]= load_model('checkpoints/%s_weights.h5' % name)
			self.history[cid] = history

	def evaluate(self, stocks, features, labels, batch_size=1):

		for cid in range(self.num_clusters):
				X = [features[stock] for stock in stocks if self.stock_clusters[stock] == cid]
				if len(X) == 0:
						continue
				X = [self.normalizers[cid].transform(x) for x in X]
				X = np.vstack([np.expand_dims(x, axis=1) for x in X])
				Y = [labels[stock] for stock in stocks if self.stock_clusters[stock] == cid]
				Y = np.vstack([y.reshape(-1, 1) for y in Y])
				print("CID:", cid, "X shape:", X.shape, "Y shape:", Y.shape)

				loss = self.models[cid].evaluate(X, Y,
												 batch_size=batch_size,
												 verbose=1)
				print(loss)

	def predict(self, X, stocks):
		"""
		Args:
				X : Numpy array of shape (d x s)
						where d = Number of features and s = Number of stocks

		"""
		# print(X.shape[1] , len(self.stocks))
		# assert X.shape[1] == len(self.stocks)
		predictions = pd.Series(index=stocks)
		for i in range(len(stocks)):
			stock =stocks[i]
			predictions[stock] = self.models[self.stock_clusters[stock]].predict(np.expand_dims(self.normalizers[self.stock_clusters[stock]].transform(np.expand_dims(X[:,i], axis=0)), axis=1))[0][0]


		return predictions


def make_keras_picklable():
		"""http://zachmoshe.com/2017/04/03/pickling-keras-models.html
		"""
		def __getstate__(self):
				model_str = ""
				with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
						keras.models.save_model(self, fd.name, overwrite=True)
						model_str = fd.read()
				d = { 'model_str': model_str }
				return d

		def __setstate__(self, state):
				with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
						fd.write(state['model_str'])
						fd.flush()
						model = keras.models.load_model(fd.name)
				self.__dict__ = model.__dict__

		cls = keras.models.Model
		cls.__getstate__ = __getstate__
		cls.__setstate__ = __setstate__

def build_model(layers, batch_size=None, timesteps=1, stateful=False):
		model = Sequential()

		model.add(LSTM(
				units=layers[1],
				batch_input_shape=(batch_size, timesteps, layers[0]),
				kernel_regularizer=keras.regularizers.l1_l2(l1=0.0, l2=0.001),
				return_sequences=True
				# bias_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001),
				# recurrent_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001)
				))
		model.add(Dropout(0.2))

		model.add(LSTM(
				units=layers[2],
				kernel_regularizer=keras.regularizers.l1_l2(l1=0.0, l2=0.001),
				return_sequences=False
				# bias_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001),
				# recurrent_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001)
				))
		model.add(Dropout(0.2))

		model.add(Dense(
				units=layers[3],
				kernel_regularizer=keras.regularizers.l1_l2(l1=0.0, l2=0.001)
				# bias_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001)
				))
		model.add(Activation("linear"))

		# start = time.time()
		model.compile(loss="mean_squared_error", optimizer="adam")
		# print("> Compilation Time : ", time.time() - start)
		return model

class History(Callback):
		"""
		Callback that records events into a `History` object.

		This callback is automatically applied to
		every Keras model. The `History` object
		gets returned by the `fit` method of models.
		"""

		def on_train_begin(self, logs=None):
				if not hasattr(self, 'epoch'):
						self.epoch = []
						self.history = {}

		def on_epoch_end(self, epoch, logs=None):

				logs = logs or {}
				self.epoch.append(epoch)
				for k, v in logs.items():
						self.history.setdefault(k, []).append(v)

#### 2. Training LSTM Model: This is used to re-train the LSTM model periodically
class TrainModel(object):
	"""docstring for TrainModel."""
	def __init__(self, stocks, daterange, dset, cluster_path='', clusters=None, periods=None, factor=1, unscrambler=None, volumepath='',filterData=True):
			self.dset_path = dset
			self.cluster_path = cluster_path
			self.stocks = stocks
			if clusters is None:
				self.loadClusters()
			else:
				self.stock_clusters = clusters

			self.loadData(daterange, unscrambler, volumepath)
			if filterData:
				self.filterData()
			self.training_features = {s : None for s in self.stocks}
			self.training_labels = {s : None for s in self.stocks}
			if periods:
				feature_periods =  {k : int(factor * periods[k]) for k in periods.keys()}
			self.computeLabels(periods)
			self.computeFeatures(feature_periods)
			self.num_features = self.training_features[self.stocks[0]].shape[1]
			print("Num features:", self.num_features)
			del self.stock_df
			gc.collect()

	### Load Data to be used for re-training
	def loadData(self, daterange, unscrambler, volumepath):
		self.stock_df = dict()
		for stock in self.stocks:
			df = pd.read_csv(os.path.join(self.dset_path, stock + ".csv"), index_col=0, parse_dates=True)
			self.stock_df[stock] = pd.DataFrame()
			if daterange:
				if type(daterange[0]) is tuple:
					for d in daterange:
						self.stock_df[stock] = pd.concat([self.stock_df[stock], df[d[0]:d[1]]])
				else:
					self.stock_df[stock] = df[daterange[0]:daterange[1]].copy()
			else:
				self.stock_df[stock] = df.copy()

			if volumepath is not '' :
					volumedata = pd.read_csv(os.path.join(volumepath, unscrambler.loc[stock, 'Name'] + ".csv"), index_col=0, parse_dates=True)
					self.stock_df[stock] = self.stock_df[stock].join(volumedata[daterange[0]:daterange[1]], how='inner')

		self.all_features = self.stock_df[stock].columns

		self.features = ['stockVWAP', 'futureVWAP', 'basis', 'stockTopBidPrice','stockTopAskPrice', 'futureTopBidPrice', 
						'futureTopAskPrice', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidPrice', 'futureNextAskPrice', 
						'stockAverageBidPrice', 'stockAverageAskPrice', 'futureAverageBidPrice', 'futureAverageAskPrice']
		self.label = 'FairValue'

	### Update data
	def updateData(self, path, daterange, unscrambler, volumepath, filterData=True):
		self.stock_df = dict()
		for stock in self.stocks:
			df = pd.read_csv(os.path.join(path, stock + ".csv"), index_col=0, parse_dates=True)
			self.stock_df[stock] = df[daterange[0]:daterange[1]]
			if volumepath is not '' :
					volumedata = pd.read_csv(os.path.join(volumepath, unscrambler.loc[stock, 'Name'] + ".csv"), index_col=0, parse_dates=True)
					self.stock_df[stock] = self.stock_df[stock].join(volumedata[daterange[0]:daterange[1]], how='inner')
		if filterData:
			self.filterData()
		self.computeFeatures(period=5, halflife=20)
		del self.stock_df
		gc.collect()


	### Fix for missing time stamps
	def filterData(self):
		timestamps = self.stock_df[self.stocks[0]].index
		for stock in self.stocks[1:]:
				timestamps = timestamps.intersection(self.stock_df[stock].index)
		for stock in self.stocks:
				self.stock_df[stock] = self.stock_df[stock].loc[timestamps.values]

	
	### Load clusters of similar stocks
	def loadClusters(self):
		with open(self.cluster_path) as f:
				self.stock_clusters = pickle.load(f)
		print(self.stock_clusters)


	### Calculate the Target Variable
	def computeLabels(self, minutes):
			for stock in self.stocks:
				fv = self.stock_df[stock]['basis'].rolling('%ds' % (minutes[stock]*60)).mean()
				fv = fv.shift(-minutes[stock], freq='Min')
				labels = pd.Series(index=self.stock_df[stock]['basis'].index)
				labels.update(fv)
				labels.fillna(method='ffill', inplace=True)
				self.training_labels[stock] = labels.as_matrix()

	### Retrain the Model
	def computeModelParams(self, model, epochs=30, batch_size=64, validation_split=0.05, initial_epoch=0, update=False, name=""):
			model.fit(self.stocks, self.training_features, self.training_labels, epochs, batch_size, validation_split, initial_epoch, update, name)
			model.save_model("saved_models/"+name)

			gc.collect()

	### Evaluate the Model
	def evaluateModelParams(self, model, batch_size=1):
			model.evaluate(self.stocks, self.training_features, self.training_labels, batch_size)

			gc.collect()

	### Other helpers
	def computeFeatures(self, period, halflife=None):
		for stock in self.stocks:
			num_rows = len(self.stock_df[stock])
			stock_features = np.empty((num_rows, 0))
			for feature in self.features:
				### Code to call corresponding feature calcs

			stock_features = np.nan_to_num(stock_features)
			print(stock, stock_features.shape)
			if self.training_features[stock] is None:
				self.training_features[stock] = stock_features.copy()
			else:
				print("CONCATENATE")
				self.training_features[stock] = np.concatenate((self.training_features[stock], stock_features), axis=0)

		del stock_features
		gc.collect()

#### Other Helpers to account for different models around future expiry

def calc_last_thursday(date):
		"""Assuming the "date" is not equal to "last_thursday" """
		end_of_month = date + relativedelta(day=31)
		last_thursday = end_of_month + relativedelta(weekday=TH(-1))
		if last_thursday < date:
				end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
				last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
		return last_thursday

def is_in_expiry_week(date):
		last_thursday = calc_last_thursday(date)
		return (last_thursday - date).days < 7 
