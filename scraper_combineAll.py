import pandas as pd
import numpy as np
import os
import datetime as dt
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, TH

def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

# for root, dirs, files in os.walk('/Users/chandinijain/Auquan/DataQQ3'):

useful = []
acc = None
root = '/Users/chandinijain/Auquan/DataQQ3'
dirs = 'VolumeByStock'
files = next(os.walk('%s/%s'%(root,dirs)))[2]
print(files)

stocks = ['ACC','ADANIENT','ALBK','AMBUJACEM','ANDHRABANK','APOLLOTYRE','ASHOKLEY','AUROPHARMA','AXISBANK','BAJAJ-AUTO','BANKBARODA','BANKINDIA',
		'BHARATFORG','BHARTIARTL','BHEL','BIOCON','BPCL','CANBK','CENTURYTEX','CESC','CIPLA','COLPAL','DABUR','DISHTV','DLF','DRREDDY','FEDERALBNK',
		'GAIL','GMRINFRA','TVSMOTOR','GODREJIND','GRASIM','HCLTECH','HDFC','HDFCBANK','HDIL','HINDALCO','HINDPETRO','HINDUNILVR','ICICIBANK','IDBI',
		'IDEA','IDFC','IFCI','INDIACEM','IOC','ITC','MRF','JINDALSTEL','JISLJALEQS','JPASSOCIAT','JSWSTEEL','KOTAKBANK','LICHSGFIN','LT','LUPIN',
		'MARUTI','NTPC','ONGC','ORIENTBANK','PETRONET','PFC','PNB','POWERGRID','PTC','RCOM','RECLTD','RELCAPITAL','RELIANCE','RELINFRA','RPOWER','SAIL',
		'SBIN','SIEMENS','SUNPHARMA','SUNTV','TATACHEM','TATACOMM','TATAMOTORS','TATAPOWER','TATASTEEL','TCS','TECHM','TITAN','UNIONBANK','VOLTAS',
		'YESBANK','ZEEL','OFSS','ASIANPAINT','DIVISLAB','HINDZINC','SYNDIBANK','ULTRACEMCO','WIPRO','MCDOWELL-N','BEL','BEML','NMDC','EXIDEIND',
		'HEXAWARE','TATAGLOBAL','ADANIPOWER','NHPC','SRTRANSFIN','HAVELLS','INDUSINDBK','IRB','KTKBANK','TATAMTRDVR','BATAINDIA','IGL','COALINDIA','INFY',
		'ARVIND','JUBLFOOD','HEROMOTOCO','ADANIPORTS','GLENMARK','STAR','UBL','UPL','APOLLOHOSP','ENGINERSIN','IBULHSGFIN','WOCKPHARMA','JUSTDIAL','L&TFH',]
for s in stocks:
	VolData = pd.Series(index=[])
	listfiles = [x for x in files if x[:-11]==s]

	for name in listfiles:
		print(name)
		month = name[-7:-4]

		data = pd.read_csv(os.path.join(root, dirs, name), index_col=0, header=None, parse_dates=True, names=['Volume'])#, parse_dates=[['DATE', 'TIME']], date_parser=dateparse)		
		VolData = VolData.append(data)
	
	VolData.sort_index(inplace=True)
	data2 = pd.read_csv(os.path.join(root, 'StockData', '%s.csv'%s))
	
	data2['datetime'] = pd.to_datetime(data2['DATE'] + ' ' + data2['TIME'], format='%d/%m/%y %H:%M:%S' )
	data2.set_index(data2['datetime'], drop=True, inplace=True)
	data2 = data2[data2['CLOSE']!=0]
	
	data2['Volume'] = VolData['Volume']
	del data2['DATE']
	del data2['TIME']
	del data2['datetime']
	data2.dropna(inplace=True)
	print(data2[data2.isnull().any(axis=1)])
	
	data2.to_csv('/Users/chandinijain/Auquan/DataQQ3/QQDataAllDates/%s.csv'%s, index=True)
				

	# frames=[VolData['Volume'], data2]
	# final = pd.concat(frames)
	# print(final)
		# data = data[data['CLOSE']!=0]
		# print(name, ' Start: ', dt.datetime.strftime(data.index[0], '%Y-%m-%d'), \
		# 	' Close: ', dt.datetime.strftime(data.index[-1], '%Y-%m-%d'), \
		# 	' Length: ', len(data))
	## check if data exists from 2014-2018 
	# 	if data.index[-1] > dt.datetime.strptime('20180420', '%Y%m%d'):
	# 		if data.index[0] < dt.datetime.strptime('20140601', '%Y%m%d'):
	# 			useful = useful + [name]
	# print(useful) 
		# print(data[-1000:])
		# data.sort_index(inplace=True)
	## compare data with ACC to see missing points	
		# if acc is None:
		# 	acc = data['CLOSE']

		# fig, ax1 = plt.subplots()
		# ax1.plot(data['CLOSE'])
		# ax1.set_xlabel('time (s)')
		# # Make the y-axis label, ticks and tick labels match the line color.
		# ax1.set_ylabel('%s'%name, color='b')
		# ax1.tick_params('y', colors='b')

		# ax2 = ax1.twinx()

		# ax2.plot(acc)
		# ax2.set_ylabel('ACC', color='r')
		# ax2.tick_params('y', colors='r')

		# fig.tight_layout()
		# plt.title('%s'%name)
		# plt.show() 