import os
import os.path
import traceback
import imp
import requests
import json
import time
import math
from json_utils import getFinalJSON
from slack_utils import sendSlackMessage


SECRET_KEY = 'BALLE BALLE'

#API_BASE_URL = 'http://localhost:3002'
API_BASE_URL = 'https://auquan-backend.herokuapp.com'

# api for getSubmission
API_GET_SUBMISSIONS = '/api/getUserInfoForProblem'

BASE_DATA_SET_P1 = 'testData'
#BASE_DATA_SET_P1 = 'tData'
BASE_DATA_SET_P2 = 'testDataP2_'


def triggerContinous(simulateTradingSystem, probId):
    while True:
        try:
            simulateTradingSystem(probID=probId, url=API_GET_SUBMISSIONS)
        except Exception as e:
            print("Random error")
            print(str(e))
            print(traceback.format_exc())
        time.sleep(60)  # Delay for 1 minute (60 seconds)

def getProblemToProcess(subId, probId, url):
    if not subId or not probId:
        data = {'secretKey': SECRET_KEY,
                'problemId': probId}
        url = API_BASE_URL + url
        criteria = [{'problemId': 'qq5p3'}, {'status': 'computed'}]
        r = requests.post(url=url, json=data)
        responseBody = json.loads(r.text)
        if not responseBody.get('submission') or not responseBody.get('submission') != 'None':
            print('no submission to be processed')
            return
        submission = responseBody.get('submission')
        submissionId = submission.get('_id')
        username = submission.get('username')
        problemId = submission.get('problemId')
        solution = submission.get('solution')
        filename = submissionId + '.py'
        codeFile = open(filename, "wb")
        codeFile.write(solution.encode('utf8'))
        codeFile.close()
    else:
        submissionId = subId
        problemId = probId
        username = 'Unknown'
        filename = username + '_' + problemId + '_' +  '.py'
    print('Submission processing for username: ' + username + ' problemId: ' + problemId + ' submissionId: ' + submissionId)
    sendSlackMessage('Submission processing for username: ' + username + ' problemId: ' + problemId + ' submissionId: ' + submissionId)
    print('simulating trading system for submission: ' + submissionId)
    return filename, username, problemId, submissionId