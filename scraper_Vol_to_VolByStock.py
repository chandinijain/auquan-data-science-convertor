import pandas as pd
import numpy as np
import os
import datetime as dt
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, TH

def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

# for root, dirs, files in os.walk('/Users/chandinijain/Auquan/DataQQ3'):
files = next(os.walk('/Users/chandinijain/Auquan/DataQQ3/Volume'))[2]
useful = []
acc = None
root = '/Users/chandinijain/Auquan/DataQQ3'
dirs = 'Volume'
VolData = {}
stocks = ['ACC','ADANIENT','ALBK','AMBUJACEM','ANDHRABANK','APOLLOTYRE','ASHOKLEY','AUROPHARMA','AXISBANK','BAJAJ-AUTO','BANKBARODA','BANKINDIA',
		'BHARATFORG','BHARTIARTL','BHEL','BIOCON','BPCL','CANBK','CENTURYTEX','CESC','CIPLA','COLPAL','DABUR','DISHTV','DLF','DRREDDY','FEDERALBNK',
		'GAIL','GMRINFRA','TVSMOTOR','GODREJIND','GRASIM','HCLTECH','HDFC','HDFCBANK','HDIL','HINDALCO','HINDPETRO','HINDUNILVR','ICICIBANK','IDBI',
		'IDEA','IDFC','IFCI','INDIACEM','IOC','ITC','MRF','JINDALSTEL','JISLJALEQS','JPASSOCIAT','JSWSTEEL','KOTAKBANK','LICHSGFIN','LT','LUPIN',
		'MARUTI','NTPC','ONGC','ORIENTBANK','PETRONET','PFC','PNB','POWERGRID','PTC','RCOM','RECLTD','RELCAPITAL','RELIANCE','RELINFRA','RPOWER','SAIL',
		'SBIN','SIEMENS','SUNPHARMA','SUNTV','TATACHEM','TATACOMM','TATAMOTORS','TATAPOWER','TATASTEEL','TCS','TECHM','TITAN','UNIONBANK','VOLTAS',
		'YESBANK','ZEEL','OFSS','ASIANPAINT','DIVISLAB','HINDZINC','SYNDIBANK','ULTRACEMCO','WIPRO','MCDOWELL-N','BEL','BEML','NMDC','EXIDEIND',
		'HEXAWARE','TATAGLOBAL','ADANIPOWER','NHPC','SRTRANSFIN','HAVELLS','INDUSINDBK','IRB','KTKBANK','TATAMTRDVR','BATAINDIA','IGL','COALINDIA','INFY',
		'ARVIND','JUBLFOOD','HEROMOTOCO','ADANIPORTS','GLENMARK','STAR','UBL','UPL','APOLLOHOSP','ENGINERSIN','IBULHSGFIN','WOCKPHARMA','JUSTDIAL','L&TFH',]
for s in stocks:
	VolData[s] = pd.Series(index=[])
old_month = 'Apr'
for name in files:
	#files[2:]:
		print(name)
		date = name[:8]
		th = calc_last_thursday(dt.datetime.strptime(date, '%Y%m%d' ))
		# next_month = th + relativedelta(months=1)
		month = dt.datetime.strftime(th, '%b' )
		# dateparse = lambda dates: [pd.datetime.strptime(d, '%d/%m/%y %H:%M:%S') for d in dates]

		data = pd.read_csv(os.path.join(root, dirs, name), index_col=0)#, parse_dates=[['DATE', 'TIME']], date_parser=dateparse)		
		data = data.T
		a, data['Time'] = data.index.str.split('v', 1).str
		data['datetime'] = pd.to_datetime(date + ' ' + data['Time'], format='%Y%m%d %H%M' )
		data.set_index(data['datetime'], drop=True, inplace=True)
		del[data['Time']]
		del[data['datetime']]
		listcols = list(data.filter(regex = date[2:4]+month.upper()))
		data = data[listcols]
		# data.drop(data.columns not in listc, axis = 1, inplace = True)
		data.rename(columns=lambda x: x[:-8], inplace=True)
		for s in stocks:
			if s in data.columns:
				# print(data[s])
				if old_month!=month:
					# print(data[s], VolData[s])
					VolData[s].to_csv('/Users/chandinijain/Auquan/DataQQ3/VolumeByStock/%s%s%s.csv'%(s,old_month,date[:4]), float_format='%.0f', index=True)
					VolData[s] = pd.Series(index=[])
				try:
					VolData[s] = VolData[s].append(data[s])
				except:
					print(s)
					print(data[s], VolData[s])
		old_month = month

		# data = data[data['CLOSE']!=0]
		# print(name, ' Start: ', dt.datetime.strftime(data.index[0], '%Y-%m-%d'), \
		# 	' Close: ', dt.datetime.strftime(data.index[-1], '%Y-%m-%d'), \
		# 	' Length: ', len(data))
	## check if data exists from 2014-2018 
	# 	if data.index[-1] > dt.datetime.strptime('20180420', '%Y%m%d'):
	# 		if data.index[0] < dt.datetime.strptime('20140601', '%Y%m%d'):
	# 			useful = useful + [name]
	# print(useful) 
		# print(data[-1000:])
		# data.sort_index(inplace=True)
	## compare data with ACC to see missing points	
		# if acc is None:
		# 	acc = data['CLOSE']

		# fig, ax1 = plt.subplots()
		# ax1.plot(data['CLOSE'])
		# ax1.set_xlabel('time (s)')
		# # Make the y-axis label, ticks and tick labels match the line color.
		# ax1.set_ylabel('%s'%name, color='b')
		# ax1.tick_params('y', colors='b')

		# ax2 = ax1.twinx()

		# ax2.plot(acc)
		# ax2.set_ylabel('ACC', color='r')
		# ax2.tick_params('y', colors='r')

		# fig.tight_layout()
		# plt.title('%s'%name)
		# plt.show() 