import pandas as pd
import numpy as np
import csv
import os 
import matplotlib.pyplot as plt
from sklearn import *
def hurst(input_ts, lags_to_test=20):
    # interpretation of return value
    # hurst < 0.5 - input_ts is mean reverting
    # hurst = 0.5 - input_ts is effectively random/geometric brownian motion
    # hurst > 0.5 - input_ts is trending
    tau = []
    lagvec = []   
    #  Step through the different lags
    for lag in range(2, lags_to_test):  
        #  produce price difference with lag  
        pp = np.subtract(input_ts[lag:], input_ts[:-lag])  
        #  Write the different lags into a vector  
        lagvec.append(lag)  
        #  Calculate the variance of the differnce vector  
        tau.append(np.sqrt(np.std(pp)))  
    #  linear fit to double-log graph (gives power)  
    m = np.polyfit(np.log10(lagvec), np.log10(tau), 1)  
    # calculate hurst  
    hurst = m[0]*2   
    return hurst

if __name__ == "__main__":
	path = "/Users/chandinijain/Auquan/qq3data/QQ3DataDownSampled/"
	mean_reverting = []
	momentum = []
	req_table = pd.DataFrame(index=sorted(os.listdir(path)), columns=['1D'])
	freq_table = pd.DataFrame(index=sorted(os.listdir(path)), columns=['1D'])
	hurst_value_table = pd.DataFrame(index=sorted(os.listdir(path)), columns=['1D'])
	hurst_indicator_table = pd.DataFrame(index=sorted(os.listdir(path)), columns=['1D'])
	intersection_array_mom = []
	intersection_array_mean = []
	gap_array = [14]
	column_array = req_table.columns.values
	# print column_array
	for j in range(len(gap_array)):
		for file in sorted(os.listdir(path))[:-1]:
			# print("StockName: %s"%file)
			df = pd.read_csv(path+file, index_col=0)
			df['stockVWAP'] = df['F5']
			same_direction = 0
			opp_direction = 0
			iterating_array = [gap_array[j]*a for a in range(df.shape[0]/gap_array[j])]
			for i in iterating_array:
				if (i+2*gap_array[j])<(df.shape[0]-2):
					if (df['stockVWAP'][i]>df['stockVWAP'][i+gap_array[j]] and df['stockVWAP'][i+gap_array[j]]>df['stockVWAP'][i+2*gap_array[j]]) or (df['stockVWAP'][i]<df['stockVWAP'][i+gap_array[j]] and df['stockVWAP'][i+gap_array[j]]<df['stockVWAP'][i+2*gap_array[j]]):
						same_direction = same_direction+1
					else:
						opp_direction = opp_direction +1
			# print('IDK',req_table.at[file,column_array[j]], file, column_array[j])
			# if j<=1:
			# 	print('StockData:', df['stockVWAP'][::gap_array[j]])
			hurst_value = hurst(df['stockVWAP'][::gap_array[j]])
			print('For stock %s, Same Direction: %i, Opp Direction: %i, Hurst Value: %.3f'%(file, same_direction, opp_direction, hurst_value))
			hurst_value_table.at[file,column_array[j]] = hurst_value

			if hurst_value>0.5:
				hurst_indicator_table.at[file,column_array[j]] = 1 #momentum
			else:
				hurst_indicator_table.at[file,column_array[j]] = 0 #mean_reverting

			if same_direction>1.1*opp_direction:
				req_table.at[file,column_array[j]] = 1 #momentum
			elif opp_direction>1.1*same_direction:
				req_table.at[file,column_array[j]] = 0 #mean_reverting
				
			freq_table.at[file,column_array[j]] = float(same_direction)/(same_direction+opp_direction)
			# print req_table.loc[file], "success"
			if hurst_value>0.5 and req_table.at[file,column_array[j]] == 1:
				intersection_array_mom.append([file, column_array[j]])
			if hurst_value<0.5 and req_table.at[file,column_array[j]] == 0:
				intersection_array_mean.append([file, column_array[j]])

	req_table.to_csv("Classification_Price_Difference.csv", sep='\t')
	freq_table.to_csv("Frequency_Price_Difference.csv", sep='\t')
	hurst_indicator_table.to_csv("Hurst_indicator_signs.csv", sep='\t')
	hurst_value_table.to_csv("Hurst_values.csv", sep='\t')
	print('Momentum:')
	print intersection_array_mom
	print('Mean R:')
	print intersection_array_mean
	

	#2 hour mometum stocks
	# for file in list(['ASHOKLEY.csv', 'IDBI.csv', 'IDEA.csv', 'INDIACEM.csv', 'JUSTDIAL.csv', 'MANAPPURAM.csv', 'PNB.csv', 'SREINFRA.csv', 'SUNPHARMA.csv', 'UNIONBANK.csv', 'TATAMTRDVR.csv']):
	# 	gap = 2*60
	# 	df = pd.read_csv(path+file)
	# print req_table