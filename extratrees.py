from backtester.features.feature import Feature
from backtester.trading_system import TradingSystem
from backtester.sample_scripts.fair_value_params import FairValueTradingParams
from backtester.version import updateCheck

#Machine Learning
import numpy as np
import pandas as pd
from sklearn.ensemble import ExtraTreesRegressor


from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
##other
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)


class GBTradingFunctions():


    def __init__(self, stocks, frequency, startdate):
        self.stocks = stocks
        self.count = 0
        self.today = startdate
        self.frequency = frequency
        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)
        self.model={}
        for i in self.stocks:
          self.model[self.unscrambler.loc[i, 'Name']] = ExtraTreesRegressor()

    def getSymbolsToTrade(self):
        print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]


    def getCustomFeatures(self):
        return {'percentB': MyCustomFeature}

    def getInstrumentFeatureConfigDicts(self):

        ma1 = {'featureKey': 'ma_5',
                   'featureId': 'moving_average',
                   'params': {'period': 30*60/self.frequency,
                              'featureName': 'basis'}}
        ma2 = {'featureKey': 'ma_10',
                   'featureId': 'moving_average',
                   'params': {'period': 60*60/self.frequency,
                              'featureName': 'stockTopAskPrice'}}
        ma3 = {'featureKey': 'ma_90',
                   'featureId': 'moving_average',
                   'params': {'period': 10*60/self.frequency,
                              'featureName': 'basis'}}
        expma = {'featureKey': 'expma20',
                 'featureId': 'exponential_moving_average',
                 'params': {'period': 90*60/self.frequency,
                              'featureName': 'basis'}}
        sdevDict = {'featureKey': 'sdev_5',
                    'featureId': 'moving_sdev',
                    'params': {'period': 30*60/self.frequency,
                               'featureName': 'basis'}}
        diff = {'featureKey': 'price_change',
                'featureId': 'difference',
                'params': {'period': 30*60/self.frequency,
                           'featureName': 'basis'}}
        macd = {'featureKey': 'macd',
                'featureId': 'macd',
                'params': {'period1': 30*60/self.frequency,
                           'period2': 60*60/self.frequency,
                           'featureName': 'basis'}}
        bbandlower = {'featureKey': 'lowerBB',
                          'featureId': 'bollinger_bands_lower',
                          'params': {'period': 90*60/self.frequency,
                                     'featureName': 'stockVWAP'}}
        bbandupper = {'featureKey': 'upperBB',
                          'featureId': 'bollinger_bands_upper',
                          'params': {'period': 90*60/self.frequency,
                                     'featureName': 'stockVWAP'}}
        bbandlower1 = {'featureKey': 'lowerBBbasis',
                          'featureId': 'bollinger_bands_lower',
                          'params': {'period': 90*60/self.frequency,
                                     'featureName': 'basis'}}
        bbandupper1 = {'featureKey': 'upperBBbasis',
                          'featureId': 'bollinger_bands_upper',
                          'params': {'period': 90*60/self.frequency,
                                     'featureName': 'basis'}}
        rsi = {'featureKey': 'rsi',
               'featureId': 'rsi',
               'params': {'period': 30*60/self.frequency,
                          'featureName': 'basis'}}

        return [ma1, ma2, ma3, expma, sdevDict, diff, macd,
                bbandlower, bbandupper, rsi, bbandlower1, bbandupper1]


    def getPrediction(self, time, updateNum, instrumentManager):
        # holder for all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
        features = ['basis', 'stockVWAP', 'futureVWAP', #'benchmark_score',
        			'ma_1','ma_2','ma_3','exponential_moving_average','sdev_5',
        			'price_change', 'macd', 'lowerBB', 'upperBB', 'rsi', 'pctB' ]

        basis = lookbackInstrumentFeatures.getFeatureDf('basis')
        stockVWAP = lookbackInstrumentFeatures.getFeatureDf('stockVWAP')
        futureVWAP = lookbackInstrumentFeatures.getFeatureDf('futureVWAP')
        ma_5 = lookbackInstrumentFeatures.getFeatureDf('ma_5')
        ma_10 = lookbackInstrumentFeatures.getFeatureDf('ma_10')
        ma_90 = lookbackInstrumentFeatures.getFeatureDf('ma_90')
        expma20 = lookbackInstrumentFeatures.getFeatureDf('expma20')
        sdev_5 =lookbackInstrumentFeatures.getFeatureDf('sdev_5')
        price_change =lookbackInstrumentFeatures.getFeatureDf('price_change')
        macd =lookbackInstrumentFeatures.getFeatureDf('macd')
        lowerBB =lookbackInstrumentFeatures.getFeatureDf('lowerBB')
        upperBB =lookbackInstrumentFeatures.getFeatureDf('upperBB')
        rsi =lookbackInstrumentFeatures.getFeatureDf('rsi')
        lowerBB1 =lookbackInstrumentFeatures.getFeatureDf('lowerBBbasis')
        upperBB1 =lookbackInstrumentFeatures.getFeatureDf('upperBBbasis')
        #percentB =lookbackInstrumentFeatures.getFeatureDf('percentB')

        stocks = list(ma_5.columns)

        ma5 = ma_5.iloc[-1]
        percentB = (basis - lowerBB1)/(lowerBB1 - upperBB1)

        if len(ma_5.index) > 90*60/self.frequency:
            out = pd.Series()
            for i in stocks:
                X=pd.concat([basis[i],stockVWAP[i], futureVWAP[i], #benchmark_score[i],
                             ma_5[i], ma_10[i], ma_90[i], expma20[i], sdev_5[i], price_change[i],
                             macd[i], lowerBB[i], upperBB[i], rsi[i], percentB[i]],
                            axis=1, keys=features)
                X.fillna(method='ffill', inplace=True)
                #print(X.iloc[-1])
                #print(basis)
                from sklearn.preprocessing import Imputer
                X = Imputer().fit_transform(X)

                y = ma_5[i]

                #making testing training
                X_test = X[-1:]
                if self.count%(5*60/self.frequency) == 0:
                  X_train = X[:-(90*60/self.frequency)]
                  y_train = y[(90*60/self.frequency):]
                  self.model[i].fit(X_train,y_train.values.ravel())

                p = self.model[i].predict(X_test)

                out = out.append(pd.Series([float(p)], index=[i]))
        else:
            out = expma20.iloc[-1]

        self.count +=1

        return out


class MyCustomFeature(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):

        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        lookbackInstrumentBasis = lookbackInstrumentFeatures.getFeatureDf('basis')
        last = lookbackInstrumentBasis.iloc[-1]
        avg = lookbackInstrumentBasis[-20*60/self.frequency:].mean()
        sdev = lookbackInstrumentBasis[-20*60/self.frequency:].std()

        lbb = avg - sdev
        ubb = avg + sdev

        return (last - lbb) / (ubb - lbb)

