import sys
sys.path.insert(0, "../auquantoolbox")

import backtester
import cPickle
import pandas as pd

initializerFile = 'savedData20180605'
with open(initializerFile, 'rb') as oldFile:
	initializer = cPickle.load(oldFile)

b = initializer['instrument']['basis']
p = initializer['instrument']['prediction']

p.getData().to_csv('pred_%s'%sys.argv[1], float_format='%.3f')
b.getData().to_csv('basis_%s'%sys.argv[1], float_format='%.3f')