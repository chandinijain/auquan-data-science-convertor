import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta as td
import os
 
def writecsv(csv_dir, results, m):
     # results = results.sort_index(axis=0, ascending=False)
      print('writing %s%s.csv' % (csv_dir, m))
      fileName = '%s%s.csv' % (csv_dir, m)
      csv_file = open('%s%s.csv' % (csv_dir, m), 'wb')
      results.to_csv(csv_file, header=True)
      csv_file.close()

datasetId1 = 'testData1'
datasetId2 = 'testData2'
datasetId3 = 'testData3'
datasetId4 = 'OSDataP1'
volfile = 'cleanDataVolume'
scrambler = pd.read_csv('scrambler.csv',index_col=0)
#scrambler.set_index('Unnamed: 0', inplace=True)
print(scrambler)
period = '30T'
output = pd.DataFrame(index = scrambler.index, columns = ['Name', 'Volatility','Average Trading Price','Average Trading Volume'])
output['Name'] = scrambler['Name']

for i in ['RELIANCE.csv']: # scrambler.index:
	try:
		data1 = pd.read_csv('historicalData/unscrambledData/'+datasetId1+'/'+scrambler.loc[i,'Name']+'.csv',index_col=0, parse_dates=True)
		data2 = pd.read_csv('historicalData/unscrambledData/'+datasetId2+'/'+scrambler.loc[i,'Name']+'.csv',index_col=0, parse_dates=True)
		data3 = pd.read_csv('historicalData/unscrambledData/'+datasetId3+'/'+scrambler.loc[i,'Name']+'.csv',index_col=0, parse_dates=True)
		data4 = pd.read_csv('historicalData/unscrambledData/'+datasetId4+'/'+scrambler.loc[i,'Name']+'.csv',index_col=0, parse_dates=True)
	except IOError:
		continue

	frames = [data3, data4]
	result = pd.concat(frames)
	result.sort_index(inplace=True)
	# print(result[result.index.duplicated(keep=False)])
	result.fillna(method='pad',inplace=True)
	result_sampled = result.resample(period, label='right').mean()
	del result_sampled['Dividends']
	del result_sampled['0']

	result_sampled.dropna(inplace=True)
	volumedata = pd.read_csv('historicalData/'+volfile+'/'+i,index_col=0, parse_dates=True)
	final = pd.DataFrame(0,index=result_sampled.index, columns=[])
	final['Bid Volume'] = result_sampled['stockTopBidVol']
	final['Bid Price'] = result_sampled['stockTopBidPrice']
	final['Ask Price'] = result_sampled['stockTopAskPrice']
	final['Ask Volume'] = result_sampled['stockTopAskVol']
	final['stockVWAP Last'] = result['stockVWAP'].resample('30T', label='right').last().dropna()
	final['stockVWAP High'] = result['stockVWAP'].resample('30T', label='right').max().dropna()
	final['stockVWAP Low'] = result['stockVWAP'].resample('30T', label='right').min().dropna()
	final['stockVWAP'] = result_sampled['stockVWAP']

	final['totalTradedStockVolume']	= volumedata['totalTradedStockVolume'].resample(period, label='right').last().dropna()
	final['totalTradedFutureVolume'] = volumedata['totalTradedFutureVolume'].resample(period, label='right').last().dropna()
	final['totalTradedStockValue'] = volumedata['totalTradedStockValue'].resample(period, label='right').last().dropna()
	final['totalTradedFutureValue'] = volumedata['totalTradedFutureValue'].resample(period, label='right').last().dropna()
	# output.loc[i, 'Volatility'] = 16*(final['stockVWAP']/final['stockVWAP'].shift(1)).dropna().std()
	# output.loc[i, 'Average Trading Price'] = (final['totalTradedStockValue']/final['totalTradedStockVolume']).mean()/100.0
	# output.loc[i, 'Average Trading Volume'] = final['totalTradedStockVolume'].mean()
	print(final)
	print(scrambler.loc[i,'Name'])



# print(output)
	writecsv('historicalData/tw9Data/', final, scrambler.loc[i,'Name'])

