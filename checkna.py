import pandas as pd
import os

folderName = '/Users/chandinijain/Auquan/auquantoolbox/historicalData/OSDataP1'

names = next(os.walk(folderName))[2]
print(names)
for name in names:

	df = pd.read_csv(folderName+'/'+name, index_col=0)
	if df.isnull().values.any():
		print(name)
		nan_rows = df[df.isnull().T.any().T]
		print(nan_rows)