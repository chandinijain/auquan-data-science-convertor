# TODO: This is a temporay way so that you can run this anywhere and backtester from auquantoolbox
# can be anywhere.
import sys
sys.path.insert(0, "../auquantoolbox")

from backtester.orderPlacer.base_order_placer import BaseOrderPlacer, PlacedOrder
from backtester.constants import *
from backtester.executionSystem.base_execution_system import InstrumentExection
import datetime
import threading
import time as t


def asyncTrackOrderConfirmer(orderPlacer):
    orderPlacer.startTrackOrderConfirmerFile()


def getUnixExpFromFutureSymbol(futureSymbolId):
    return int(futureSymbolId[-13:-3])


class LiveAlphaOrderPlacer(BaseOrderPlacer):

    def __init__(self, orderPlacerFilename, orderConfirmerFilename):
        self.__orderPlacerFilename = orderPlacerFilename
        self.__orderConfirmerFilename = orderConfirmerFilename
        self.__orderConfirmerFile = open(orderConfirmerFilename, "r")
        self.__orderConfirmerFile.seek(0, 2)
        self.__unconfirmedOrders = {}  # Orders which we have sent for placing, but haven't got confirmation yet.
        self.__unprocessedPlacedOrders = []  # Orders which are confirmed placed, but we havent emitted yet
        self.__unfinishedOrderConfirmerLine = ''
        self.__shouldTrack = True
        self.__thread = threading.Thread(target=asyncTrackOrderConfirmer, args=(self,))
        self.__thread.start()

    def writeToOrderFile(self, orderConfig):
        strComponents = []
        for key in orderConfig:
            val = orderConfig[key]
            strComponents.append('%s|%s' % (key, val))
        strToWrite = ','.join(strComponents)
        strToWrite = strToWrite + '\n'
        with open(self.__orderPlacerFilename, "a") as myfile:
            myfile.write(strToWrite)

    '''
    instrumentExecutions: Array of InstrumentExecution
    '''

    def placeOrders(self, time, instrumentExecutions, instrumentsManager):

        instrumentLookbackData = instrumentsManager.getLookbackInstrumentFeatures()
        predictions = instrumentLookbackData.getFeatureDf('prediction').iloc[-1]
        threshold_params = instrumentLookbackData.getFeatureDf('execution_threshold').iloc[-1]
        basis = instrumentLookbackData.getFeatureDf('basis').iloc[-1]
        sdev = instrumentLookbackData.getFeatureDf('basis_sdev').iloc[-1]
        for instrumentExecution in instrumentExecutions:
            instrumentId = instrumentExecution.getInstrumentId()
            timeOfExecution = instrumentExecution.getTimeOfExecution()
            volume = instrumentExecution.getVolume()

            if instrumentId in self.__unconfirmedOrders:
                print('Skipping order for %s since not confirmed on a previous order' % (instrumentId))
                continue
            instrument = instrumentsManager.getInstrument(instrumentId)
            bookData = instrument.getCurrentBookData()
            # write stock purchase order
            stockOrderConfig = {}
            stockOrderConfig['time'] = int(t.mktime(timeOfExecution.timetuple()))
            stockOrderConfig['instrumentId'] = instrumentId
            stockOrderConfig['stockId'] = instrumentId
            stockOrderConfig['futureId'] = bookData['future_symbolid']
            stockOrderConfig['stockExp'] = 0
            stockOrderConfig['futureExp'] = getUnixExpFromFutureSymbol(bookData['future_symbolid'])
            stockOrderConfig['quantity'] = volume
            stockOrderConfig['type'] = 'buy' if (instrumentExecution.getExecutionType() == INSTRUMENT_EXECUTION_BUY) else 'sell'
            stockOrderConfig['strike'] = 0  # TODO: How to fill this? not needed?
            stockOrderConfig['basis'] = 100*basis[instrumentId]
            stockOrderConfig['predicted_value'] = 100 * predictions[instrumentId]
            stockOrderConfig['threshold_param'] = 100 * threshold_params[instrumentId]
            stockOrderConfig['sdev'] = 100*sdev[instrumentId]
            self.writeToOrderFile(stockOrderConfig)
            self.__unconfirmedOrders[instrumentId] = stockOrderConfig

    def processLine(self, orderConfirmerLine):
        orderConfirmComponents = orderConfirmerLine.strip().split(',')
        orderConfirmConfig = {}
        for orderConfirmComponent in orderConfirmComponents:
            x = orderConfirmComponent.split('|')
            orderConfirmConfig[x[0]] = x[1]

        instrumentId = orderConfirmConfig['instrumentId'].strip()
        changeInPosition = int(orderConfirmConfig['changeInPosition'])
        tradeLoss = float(orderConfirmConfig['tradeLoss'])/100.0
        #if instrumentId in self.__unconfirmedOrders:
        #    self.__unconfirmedOrders.pop(instrumentId, None)
        if changeInPosition == 0:
            if instrumentId in self.__unconfirmedOrders:
                self.__unconfirmedOrders.pop(instrumentId, None)
            return  # This is to just remove the unconfirmed order above, for the case an order failed. No need to make a placedorder
        placedOrder = PlacedOrder(instrumentId=instrumentId,
                                  changeInPosition=changeInPosition,
                                  timeOfExecution = datetime.datetime.now(),
                                  tradeLoss = -tradeLoss)
        placedOrder.setTradePrice(float(orderConfirmConfig['price'])/100.0)  # TODO: price here should be basis? stock - future. How to calculate that
        print(orderConfirmConfig)
        # TODO: Lock this line below to prevent race condition
        self.__unprocessedPlacedOrders.append(placedOrder)

    def startTrackOrderConfirmerFile(self):
        while True:
            readLine=self.__orderConfirmerFile.readline()
            if readLine:
                self.__unfinishedOrderConfirmerLine=self.__unfinishedOrderConfirmerLine + readLine
                if self.__unfinishedOrderConfirmerLine.endswith('\n'):
                    self.processLine(self.__unfinishedOrderConfirmerLine)
                    self.__unfinishedOrderConfirmerLine=''
            else:
                t.sleep(0.1)
            if not self.__shouldTrack:  # TODO: Lock this to prevent race condition
                break

    def emitPlacedOrders(self, time, instrumentsManager):
        # TODO: Lock this line below to prevent race condition
        for placedOrder in self.__unprocessedPlacedOrders:
            instrumentId = placedOrder.getInstrumentId()
            if instrumentId in self.__unconfirmedOrders:
                self.__unconfirmedOrders.pop(instrumentId, None)
            yield(placedOrder)
        self.__unprocessedPlacedOrders=[]

    def cleanup(self):
        self.__shouldTrack=False  # TODO: Lock this to prevent race condition
        self.__thread.join()


if __name__ == "__main__":
    placer=LiveAlphaOrderPlacer('orderPlacer.txt', 'orderConfirmer.txt')
    time=datetime.datetime.now()
    ie=InstrumentExection(time = time,
                            instrumentId='testInstrumentId',
                            volume=10,
                            executionType=1)
    placer.placeOrders(time, [ie, ie], None)
    x = False
    while True:
        for placedOrder in placer.emitPlacedOrders(None, None):
            print(" got order")
            x = True
            placer.cleanup()
        if x:
            break
        t.sleep(0.1)
    print (" done")
