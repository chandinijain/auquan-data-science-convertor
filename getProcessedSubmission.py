import os
import os.path
import traceback
import imp
import requests
import json
import time
import math
import numpy as np
from fpdf import FPDF
import pandas as pd
# from Code2pdf.code2pdf import Code2pdf


SECRET_KEY = 'BALLE BALLE'

#API_BASE_URL = 'http://localhost:3002'
API_BASE_URL = 'https://auquan-backend.herokuapp.com'

# api for getSubmission
API_GET_SUBMISSIONS = '/api/getUserInfoForProblem'



def triggerContinous(simulateTradingSystem, probId):
    while True:
        try:
            simulateTradingSystem(probID=probId, url=API_GET_SUBMISSIONS)
        except Exception as e:
            print("Random error")
            print(str(e))
            print(traceback.format_exc())
        time.sleep(60)  # Delay for 1 minute (60 seconds)

def getAllSubmissions(probId, url, alldata):
    # import pdb;pdb.set_trace()
    data = {'secretKey': SECRET_KEY,
            'criteria':[{'problemId': probId}, {'username':'bhavishyagopesh@gmail.com'},{'status': 'computed'}]}
    url = API_BASE_URL + url
    r = requests.post(url=url, json=data)
    responseBody = json.loads(r.text)
    # print(responseBody)
    for i in range(len(responseBody)):
        submission = responseBody[i]
        username = submission.get('username')
        score = submission.get('score')
        solution = submission.get('solution')
        createdAt = submission.get('createdAt')
        entries = submission.get('entries')
        filename = username + probId + '.py'
        if not(os.path.exists(filename)):
            print('wiriting new file')
            codeFile = open(filename, "wb")
            codeFile.write(solution.encode('utf8'))
            codeFile.close()

        if probId=='qq5p1' and score!='Error':
            score = np.max(20*score - 2*(entries-1), 0)
        elif probId=='qq5p2'and score!='Error':
            score = np.max(20*score - (entries-1), 0)

        # ifile,ofile,size = filename+ '.py', filename+ '.pdf', 'A4'
        # pdf = Code2pdf(ifile, ofile, size)  # create the Code2pdf object
        # pdf.init_print()    # call print method to print pdf
        try:
            name = str(alldata.loc[username, 'First Name']) + str(alldata.loc[username, 'Last Name'])
        except KeyError:
            continue

        print('Submission writing for username: ' + username + ' problemId: ' + probId )
        college = alldata.loc[username, 'College']
        title = name +'_' + college + '_' + probId[-2:]
        pdf = PDF()
        pdf.set_title(title)
        pdf.set_author(username)
        pdf.print_chapter(name, college, probId, score, entries, filename, title)
        pdf.output(title + '.pdf', 'F')

    return True


def jsonify(data):
    # TODO matrix
    json_data = dict(data)
    for key, value in data.items():
        if isinstance(value, list):  # for lists
            value = [jsonify(item) if isinstance(item, dict) else convertNanAndInf(item) for item in value]
        if isinstance(value, dict):  # for nested lists
            value = jsonify(value)
        if isinstance(key, int):  # if key is integer: > to string
            key = str(key)
        if type(value).__module__ == 'numpy':  # if value is numpy.*: > to python list
            if(key == 'dates'):
                dates = []
                for date in value:
                    dates.append(str(date))
                value = dates
            else:
                value = value.tolist()
        json_data[key] = convertNanAndInf(value)
    return json_data

def convertNanAndInf(value):
    if isinstance(value, float) and (math.isnan(value) or math.isinf(value)):
        return float(0)
    return value



class PDF(FPDF):
    def header(self):
        # Arial bold 15
        self.set_font('Arial', 'B', 15)
        # Calculate width of title and position
        w = self.get_string_width(self.title) + 6
        self.set_x((210 - w) / 2)
        # Colors of frame, background and text
        self.set_draw_color(0, 80, 180)
        # self.set_fill_color(230, 230, 0)
        self.set_text_color(220, 50, 50)
        # Thickness of frame (1 mm)
        self.set_line_width(1)
        # Title
        self.cell(w, 9, self.title, 1, 1, 'C', 1)
        # Line break
        self.ln(10)
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Text color in gray
        self.set_text_color(128)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()), 0, 0, 'C')
    def chapter_title(self, name, college, problemId, score, entries):
        # Arial 12
        self.set_font('Arial', '', 10)
        # Background color
        self.set_fill_color(200, 220, 255)
        # Title
        self.cell(0, 6, 'User: %s College: %s Problem: %s Score: %s Submissions: %i' % (name, college, problemId[-2:].upper(), score, entries), 0, 1, 'L', 1)
        # Line break
        self.ln(4)
    def chapter_body(self, name):
        # Read text file
        with open(name, 'rb') as fh:
            txt = fh.read().decode('utf8')
        # Times 12
        self.set_font('Courier', '', 9)
        # Output justified text
        self.multi_cell(0, 5, txt)
        # Line break
        self.ln()
        # Mention in italics
        self.set_font('', 'I')
        self.cell(0, 5, '(end of excerpt)')
    def print_chapter(self, name, college, problemId, score, entries, filename, title):
        self.add_page()
        self.chapter_title(name, college, problemId, score, entries)
        self.chapter_body(filename)



if __name__ == "__main__":
    url=API_GET_SUBMISSIONS
    alldata = pd.read_csv('usernames.csv', index_col=0)
    probId = [ 'qq4p1'] #
    for p in probId:
        print(p)
        getAllSubmissions(p, url, alldata)

