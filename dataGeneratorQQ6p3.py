import pandas as pd
import numpy as np

path = '/Users/chandinijain/Auquan/qq3data/QQ3DataDownSampled/'
with open(path + "/stock_list.txt") as f:
	content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
stocks_name = [x.strip() for x in content]
stocksDict = {}
i=0

final = [('INS', 'BPM'), ('EYC', 'RCL'), ('CSI', 'JBP'), ('OLY', 'NTG'), ('MPC', 'VUV'), ('VIC', 'NTG'), ('GQX', 'QZO'), ('HNM', 'DOE'), ('EYC', 'HIS'), ('GQX', 'FXQ'), ('GQX', 'AQM'), ('GQX', 'NSN'), ('SSD', 'JHB'), ('OLY', 'SJL'), ('GQX', 'HRY')]

for pair in final:
	for j in pair:
		try:
			stock = j
			print(stock)
			stocksDict[stock] = pd.read_csv(path+stock+".csv")
		except:
			print('skipping: ',stock)
			continue
	i+=1

final = [('INS', 'BPM'), ('EYC', 'RCL'), ('CSI', 'JBP'), ('OLY', 'NTG'), ('MPC', 'VUV'), ('VIC', 'NTG'), ('GQX', 'QZO'), ('HNM', 'DOE'),\
		 ('EYC', 'HIS'), ('GQX', 'FXQ'), ('GQX', 'AQM'), ('GQX', 'NSN'), ('SSD', 'JHB'), ('OLY', 'SJL'), ('GQX', 'HRY')]

c= ['F41', 'F40', 'F43', 'F42', 'F45', 'F44', 'F47', 'F46', 'F49', 'F48', 'S1', 'F68', 'F23', 'F22', 'F21', 'F20', 'F25', 'F24', 'F67', \
	'F66', 'F29', 'F64', 'F63', 'F62', 'F61', 'F60', 'F16', 'F12', 'F17', 'S2', 'Y', 'F56', 'F57', 'F54', 'F55', 'F52', 'F53', 'F50',\
	'F51', 'F13', 'F58', 'F59', 'F30', 'F31', 'F18', 'F19', 'F34', 'F35', 'F37', 'F38', 'F39', 'F10', 'F11', 'F70', 'F71', 'F14', 'F15',\
	'F0', 'F1', 'F3', 'F5', 'F6', 'F8', 'F9', 'F32', 'F33', 'F69', 'F65']

for pair in final:
	S1 = stocksDict[pair[0]]
	S2 = stocksDict[pair[1]]
	S1 = S1.set_index('datetime')
	S2 = S2.set_index('datetime')
	ixs = S1.index.intersection(S2.index)
	result = S1.loc[ixs,:]/S2.loc[ixs,:]
	ratio = S1['F5']/S2['F5']
	ratio_small = ratio.rolling(window=3).mean()
	ratio_big = ratio.rolling(window=250).mean().shift(-250)
	target = ratio_small < ratio_big
	target = target.replace(False,0)
	target = target.replace(True,1)
	result['Y'] = target
	result['S1'] = S1['F5']
	result['S2'] = S2['F5']
	result.dropna(axis=1, how='any', thresh=None, subset=None, inplace=True)
	#     print(result.columns[result.isnull().any()].tolist())
	#     print(result)

	if len(c) > 0:
		c = list(set(c) & set(result.columns))
	else:
		c = result.columns
	result = result[c]
	print(len(result.columns))
	result.dropna(axis=0, how='any', thresh=None, subset=None, inplace=True)
	result.to_csv(pair[0]+pair[1]+".csv", float_format='%.3f', header=True, index=True, index_label='datetime')
	print(pair[0]+pair[1])
	print(len(c)) 

