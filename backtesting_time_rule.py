# TODO: This is a temporay way so that you can run this anywhere and backtester from auquantoolbox
# can be anywhere.
import sys
sys.path.insert(0, "../auquantoolbox")

from backtester.timeRule.time_rule import TimeRule
from datetime import datetime, timedelta


class BacktestingTimeRule(TimeRule):
    def __init__(self, date, frequencySecs):
        self.__startTime = datetime.strptime(date, '%Y/%m/%d') + timedelta(hours=9, minutes=15)
        self.__frequencySecs = frequencySecs
        self.__endTime = datetime.strptime(date, '%Y/%m/%d') + timedelta(hours=15, minutes=30)

    def emitTimeToTrade(self):
        nextTime = self.__startTime + timedelta(seconds=self.__frequencySecs)
        while (nextTime < self.__endTime):
            yield(nextTime)
            nextTime = nextTime + timedelta(seconds=self.__frequencySecs)
