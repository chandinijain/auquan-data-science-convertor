from backtester.features.feature import Feature
from backtester.trading_system import TradingSystem
from backtester.sample_scripts.fair_value_params import FairValueTradingParams
from backtester.version import updateCheck
import pandas as pd
from refined_trading_models import *
import numpy as np
import sys
import datetime
from dateutil.relativedelta import relativedelta, TH

def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

def is_in_expiry_week(date):
    last_thursday = calc_last_thursday(date)
    return (last_thursday - date).days < 7

class Problem1Solver():

    '''
    Specifies which training data set to use. Right now support
    sampleData, trainingData1, trainingData2, trainingData3.
    '''

    def __init__(self, dataset, model_path, startdate, prediction_date, update=True):
        self.dataset = dataset
        self.stocks = ['AIO', 'AUZ', 'BWU', 'CBT', 'CHV', 'CUN', 'DVV', 'EGV', 'IES', 'IFL']
        # self.stocks = ['AGW','AIO','AUZ','BSN','BWU','CBT','CHV','CSR','CUN','CYD','DCO','DFZ','DVV','DYE','EGV','FCY','FFA','FFS','FKI','FRE','FUR','GFQ','GGK','GYJ','GYV','HHK','IES','IFL','ILW','IMC','IUE','IUQ','IYU','JSG','JYW','KAA','KFW','KKG','KMV','KRZ','LDU','LKB','LPQ','MAS','MQK','MUF','NDG','NSL','NYO','OED','OGU','OMP','PFK','PLX','PMS','PQS','PUO','QRK','SCN','SVG','TGI','TWK','UBF','UWC','UWD','VKE','VML','VND','VSL','VTN','WAG','WTJ','XAD','XCS','XFD','XIT','XPV','XYR','XZR','YGC','YHW','YUZ','ZEW','ZLX']
        # self.stocks = ['AGW','AIO','BSN','CHV','CUN','DCO',
          # 'FFA','FFS','IES','ILW','IMC','IUQ',
          # 'IYU','JYW','KAA','KKG','KRZ','LPQ',
          # 'MUF','NYO','OMP','PFK','PQS','PUO',
          # 'QRK','TWK','WAG','XPV','XYR','YHW','ZLX']
        self.X = []     # Container to store features for update
        self.Y = []     # Container to store labels for update
        self.period = 60
        self.halflife = 60*4
        # self.update = update
        # self.FLAG = True
        # self.today = startdate
        # self.prediction_date = prediction_date
        # self.loadModel(filename=model_path)
        self.loadModel(filename=['../models/new/normal_week_model_1.pkl', '../models/new/expiry_week_model_1.pkl'])
        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)
        # self.predictedFairValue = [0.7]*len(self.stocks)

    def getTrainingDataSet(self):
        return self.dataset

    '''
    Returns the stocks to trade.
    If empty, uses all the stocks.
    '''

    def getSymbolsToTrade(self):
        # print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        # return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]
        return self.stocks

    def loadModel(self, filename=""):
        '''
        Load model paramters for each stock using pickle file
        '''
        make_keras_picklable()
        # with open(filename, "rb") as f:
        #     self.trading_model = pickle.load(f)
        # print(self.trading_model)
        # print(self.trading_model.stocks)
        # print(self.trading_model.stock_clusters)
        with open(filename[0], "rb") as f:
            self.normal_trading_model = pickle.load(f)
        with open(filename[1], "rb") as f:
            self.expiry_trading_model = pickle.load(f)

    def evaluateModel(self, outf="", evalCluster=False):
        if evalCluster:
            err_df = pd.DataFrame(index=list(np.arange(self.trading_model.num_clusters))+self.stocks, columns=["name", "mse", "rmse", "clusterID"])
        else:
            err_df = pd.DataFrame(index=self.stocks, columns=["name", "mse", "rmse", "clusterID"])
        X = np.array(self.X)
        Y = np.array(self.Y)
        for s, stock in enumerate(self.stocks):
            mse = self.trading_model.models[self.trading_model.stock_clusters[stock]].evaluate(
                        x=np.expand_dims(self.trading_model.normalizers[self.trading_model.stock_clusters[stock]].transform(X[:,:,s]), axis=1),
                        y=Y[:,s],
                        batch_size=1)
            rmse = np.sqrt(mse)
            err_df.loc[stock] = [self.unscrambler.loc[stock].values[0], mse, rmse, self.trading_model.stock_clusters[stock]]
        if evalCluster:
            for cid in range(self.trading_model.num_clusters):
                x = np.vstack([X[:,:,s] for s,stock in enumerate(self.stocks) if self.trading_model.stock_clusters[stock] == cid])
                y = np.vstack([Y[:,s].reshape(-1,1) for s,stock in enumerate(self.stocks) if self.trading_model.stock_clusters[stock] == cid])
                cluster_mse = self.trading_model.models[cid].evaluate(
                            x=np.expand_dims(self.trading_model.normalizers[cid].transform(x), axis=1),
                            y=y,
                            batch_size=1)
                cluster_rmse = np.sqrt(cluster_mse)
                err_df.loc[cid] = ["cluster_%s" % cid, cluster_mse, cluster_rmse, cid]
        print(err_df)
        err_df.to_csv(outf)


    '''
    [Optional] This is a way to use any custom features you might have made.
    Returns a dictionary where
    key: featureId to access this feature (Make sure this doesnt conflict with any of the pre defined feature Ids)
    value: Your custom Class which computes this feature. The class should be an instance of Feature
    Eg. if your custom class is MyCustomFeature, and you want to access this via featureId='my_custom_feature',
    you will import that class, and return this function as {'my_custom_feature': MyCustomFeature}
    '''

    def getCustomFeatures(self):
        return {'diff_ma': DiffInMovingAverage, 'time_to_expiry' : TimeToExpiry, 'ratio_ma' : RatioFutureStock}

    '''
    Returns a dictionary with:
    value: Array of instrument feature config dictionaries
        feature config Dictionary has the following keys:
        featureId: a string representing the type of feature you want to use
        featureKey: {optional} a string representing the key you will use to access the value of this feature.
                    If not present, will just use featureId
        params: {optional} A dictionary with which contains other optional params if needed by the feature
    Example:
    ma1Dict = {'featureKey': 'ma_5',
               'featureId': 'moving_average',
               'params': {'period': 5,
                          'featureName': 'stockVWAP'}}
    sdevDict = {'featureKey': 'sdev_5',
                'featureId': 'moving_sdev',
                'params': {'period': 5,
                           'featureName': 'stockVWAP'}}
    customFeatureDict = {'featureKey': 'custom_inst_feature',
                         'featureId': 'my_custom_feature',
                          'params': {'param1': 'value1'}}
    return [ma1Dict, sdevDict, customFeatureDict]
    For  instrument, you will have features keyed by ma_5, sdev_5, custom_inst_feature
    '''

    def getFeatureConfigDicts(self):
        # real_features = ['stockVWAP', 'futureVWAP', 'basis', 'stockTopBidVol', 'stockTopAskVol', 'stockTopBidPrice',
        #                 'stockTopAskPrice', 'futureTopBidVol', 'futureTopAskVol', 'futureTopBidPrice', 'futureTopAskPrice',
        #                 'stockNextBidVol', 'stockNextAskVol', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidVol',
        #                 'futureNextAskVol', 'futureNextBidPrice', 'futureNextAskPrice', 'stockTotalBidVol',
        #                 'stockTotalAskVol', 'futureTotalBidVol', 'futureTotalAskVol', 'stockAverageBidPrice',
        #                 'stockAverageAskPrice', 'futureAverageBidPrice', 'futureAverageAskPrice',
        #                 'totalTradedStockVolume', 'totalTradedFutureVolume', 'totalTradedStockValue',
        #                 'totalTradedFutureValue']
        real_features = ['stockVWAP', 'futureVWAP', 'basis', 'stockTopBidPrice',
                        'stockTopAskPrice', 'futureTopBidPrice', 'futureTopAskPrice',
                        'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidPrice', 'futureNextAskPrice', 
                        'stockAverageBidPrice', 'stockAverageAskPrice', 'futureAverageBidPrice', 'futureAverageAskPrice']
        generate_features = []
        self.feature_keys = []
        for rf in real_features:
            ma = {'featureKey': 'ma' + rf,
                     'featureId': 'moving_average',
                     'params': {'period': self.period,
                                'featureName': rf}}
            moving_min = {'featureKey': 'moving_min' +rf,
                        'featureId': 'moving_min',
                        'params': {'period': self.period,
                                   'featureName': rf}}
            moving_max = {'featureKey': 'moving_max' +rf,
                        'featureId': 'moving_max',
                        'params': {'period': self.period,
                                   'featureName': rf}}
            sdev = {'featureKey': 'sdev' + rf,
                      'featureId': 'moving_sdev',
                      'params': {'period': self.period,
                                 'featureName': rf}}
            expma = {'featureKey': 'expma' + rf,
                   'featureId': 'exponential_moving_average',
                   'params': {'period': self.halflife,
                                'featureName': rf}}
            diff_ma = {'featureKey': 'diff_ma' + rf,
                     'featureId': 'diff_ma',
                     'params': {'period': self.period,
                                'featureName': rf}}

            # moving_sum = {'featureKey': 'moving_sum' + rf,
            #           'featureId': 'moving_sum',
            #           'params': {'period': self.period,
            #                      'featureName': rf}}

            generate_features += [ma, moving_min, moving_max, sdev, expma, diff_ma]
            self.feature_keys += [f_id + rf for f_id in ['ma', 'moving_min', 'moving_max', 'sdev', 'expma', 'diff_ma']]

        moving_sum = {'featureKey': 'moving_sum',
                  'featureId': 'moving_sum',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        delay = {'featureKey': 'delay',
                  'featureId': 'delay',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        diff = {'featureKey': 'diff',
                  'featureId': 'difference',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        rank = {'featureKey': 'rank',
                  'featureId': 'rank',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        scale = {'featureKey': 'scale',
                  'featureId': 'scale',
                  'params': {'period': self.period,
                             'featureName': 'basis', 'scale': 3}}
        tte = {'featureKey': 'tte',
                 'featureId': 'time_to_expiry',
                 'params': {}}

        generate_features += [moving_sum, delay, diff, rank, scale, tte]
        self.feature_keys += ['moving_sum', 'delay', 'diff', 'rank', 'scale', 'tte']

        # ratio of moving average
        sf_pairs = [('stockTopBidVol', 'futureTopBidVol'), ('stockTopAskVol', 'futureTopAskVol'),
        ('stockNextBidVol', 'futureNextBidVol'), ('stockNextAskVol', 'futureNextAskVol'),
        ('stockTotalBidVol', 'futureTotalBidVol'), ('stockTotalAskVol', 'futureTotalAskVol'),
        ('totalTradedStockVolume', 'totalTradedFutureVolume'), ('totalTradedStockValue', 'totalTradedFutureValue')]

        for S, F in sf_pairs:
            ratio_ma = {'featureKey': 'ratio_ma' + S + F,
                      'featureId': 'ratio_ma',
                      'params': {'period': self.period,
                                 'featureName1': F, 'featureName2': S}}
            generate_features += [ratio_ma]
            self.feature_keys += ['ratio_ma' + S + F]

        # fairValueDict = {'featureKey': 'FairValue',
        #              'featureId': 'moving_average',
        #              'params': {'period': self.period,
        #                         'featureName': 'basis'}}

        # generate_features += [fairValueDict]
        return generate_features


    '''
    Using all the features you have calculated in getFeatureConfigDicts, combine them in a meaningful way
    to compute the fair value as specified in the question
    Params:
    time: time at which this is being calculated
    instrumentManager: Holder for all the instruments
    Returns:
    A Pandas DataSeries with instrumentIds as the index, and the corresponding data your estimation of the fair value
    for that stock/instrumentId
    '''

    def getFairValue(self, updateNum, time, instrumentManager):
        # holder for all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        # dataframe for a historical instrument feature (ma_5 in this case). The index is the timestamps
        # atmost upto lookback data points. The columns of this dataframe are the stock symbols/instrumentIds.
        print("Updatenum:", updateNum)
        # predictedFairValue = []
        x = []
        # training_x = []
        for f in self.feature_keys:
            data = lookbackInstrumentFeatures.getFeatureDf(f).fillna(0)
            x.append(np.array(data.iloc[-1]))
            # if len(data) > self.period:
                # training_x.append(np.array(data.iloc[-self.period-1]))

        fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')
        # self.X.append(np.array(x))                        # shape = d x s
        # self.Y.append(np.array(fairValueData.iloc[-1]))   # shape = 1 x s
        # print("X:", np.array(x)[:,0])
        print("Y:", np.array(fairValueData.iloc[-1]))
        x = np.array(x)     # shape = d x s
        if is_in_expiry_week(time):
            print("EXPIRY WEEK")
            predictedFairValue = self.expiry_trading_model.predict(x, self.stocks)
        else:
            predictedFairValue = self.normal_trading_model.predict(x, self.stocks)
        print("Predict:", predictedFairValue)

        # if self.update:# and len(training_x)>0:
        #     if self.prediction_date == str(time.date() + datetime.timedelta(days=-1)):
        #         self.today = str(time.date())
        #         print(self.today, "Updating model...")
        #         self.trading_model.update(np.array(self.X), np.array(self.Y), epochs=5, save=True, model_name="test_model_1.pkl")
        #         sys.exit()
        #     fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')
        #     self.X.append(np.array(x))                        # shape = d x s
        #     self.Y.append(np.array(fairValueData.iloc[-1]))   # shape = 1 x s
        #
        # x = np.array(x)     # shape = d x s
        # # if self.FLAG:
        #     # self.FLAG = False
        #     # print("Filling lookback...")
        #     # self.trading_model.updateLookback(x, fill=True)
        # if self.prediction_date == str(time.date()):
        #     predictedFairValue = self.trading_model.predict(x, self.stocks)
        #     fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')
        #     for i,s in enumerate(self.stocks):
        #         print("")
        #         print("%-15s %f %f" % (self.unscrambler.loc[s, 'Name'], np.array(fairValueData.iloc[-1])[i], predictedFairValue[i]))
        #     # print(predictedFairValue[:12])
        #     # print(np.array(fairValueData.iloc[-1])[:12])
        # else:
        #     predictedFairValue = [0.7]*len(self.stocks)
        # self.trading_model.updateLookback(x)

        # Returns a series with index as all the instrumentIds. This returns the value of the feature at the last
        # time update.
        return predictedFairValue

'''
We have already provided a bunch of commonly used features. But if you wish to make your own, define your own class like this.
Write a class that inherits from Feature and implement the one method provided.
'''


class MyCustomFeature(Feature):
    ''''
    Custom Feature to implement for instrument. This function would return the value of the feature you want to implement.
    This function would be called at every update cycle for every instrument. To use this feature you MUST do the following things:
    1. Define it in getCustomFeatures, where you specify the identifier with which you want to access this feature.
    2. To finally use it in a meaningful way, specify this feature in getFeatureConfigDicts with appropirate feature params.
    Example for this is provided below.
    Params:
    updateNum: current iteration of update. For first iteration, it will be 1.
    time: time in datetime format when this update for feature will be run
    featureParams: A dictionary of parameter and parameter values your features computation might depend on.
                   You define the structure for this. just have to make sure these parameters are provided when
                   you wanted to actually use this feature in getFeatureConfigDicts
    featureKey: Name of the key this will feature will be mapped against.
    instrumentManager: A holder for all the instruments
    Returns:
    A Pandas series with stocks/instrumentIds as the index and the corresponding data the value of your custom feature
    for that stock/instrumentId
    '''
    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        # Custom parameter which can be used as input to computation of this feature
        param1Value = featureParams['param1']

        # A holder for the all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        # dataframe for a historical instrument feature (basis in this case). The index is the timestamps
        # atmost upto lookback data points. The columns of this dataframe are the stocks/instrumentIds.
        lookbackInstrumentBasis = lookbackInstrumentFeatures.getFeatureDf('basis')

        # The last row of the previous dataframe gives the last calculated value for that feature (basis in this case)
        # This returns a series with stocks/instrumentIds as the index.
        currentBasisValue = lookbackInstrumentBasis.iloc[-1]

        if param1Value == 'value1':
            return currentBasisValue * 0.1
        else:
            return currentBasisValue * 0.5

class DiffInMovingAverage(Feature):
    """docstring for DiffInMovingAverage."""
    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        # A holder for the all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
        featureDf = lookbackInstrumentFeatures.getFeatureDf(featureParams['featureName'])
        moving_avgs = featureDf.rolling(window=featureParams['period'], min_periods=1).mean()
        # moving_avg = featureDf[-featureParams['period']:].mean()

        if len(featureDf.index) < featureParams['period']:
            instrumentDict = instrumentManager.getAllInstrumentsByInstrumentId()
            zeroSeries = pd.Series([0] * len(instrumentDict), index=instrumentDict.keys())
            return zeroSeries
        return moving_avgs.iloc[-1] - moving_avgs.iloc[-featureParams['period']]


class RatioFutureStock(Feature):
    """docstring for DiffInMovingAverage."""
    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        # A holder for the all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
        featureDf1 = lookbackInstrumentFeatures.getFeatureDf(featureParams['featureName1'])
        featureDf2 = lookbackInstrumentFeatures.getFeatureDf(featureParams['featureName2'])
        moving_avg1 = featureDf1[-featureParams['period']:].mean()
        moving_avg2 = featureDf2[-featureParams['period']:].mean()

        ratio =  moving_avg1 / moving_avg2
        ratio[ratio == np.Inf] = 0
        return ratio


class TimeToExpiry(Feature):
    """docstring for TimeToExpiry."""
    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        timediff = calc_last_thursday(time).replace(hour=15, minute=30) - time
        days, seconds = timediff.days, timediff.seconds
        return days*24 + seconds // 3600



if __name__ == "__main__":
    if False:#updateCheck():
        print('Your version of the auquan toolbox package is old. Please update by running the following command:')
        print('pip install -U auquan_toolbox')
    else:
        problem1Solver = Problem1Solver(dataset="../testData/",
                                        model_path="saved_models/models_with_checkpoint/clustered_minmax_lstm_model_1_10_clusters.pkl",
                                        startdate='2017-08-31', update=True, prediction_date='2017-08-31')
        # problem1Solver = Problem1Solver(dataset="OSData2P1",
                                        # model_path="test_model_1.pkl",
                                        # startdate='2017-10-03', update=True, prediction_date='2017-10-06')
        tsParams = FairValueTradingParams(problem1Solver)
        tradingSystem = TradingSystem(tsParams)
        # Set onlyAnalyze to True to quickly generate csv files with all the features
        # Set onlyAnalyze to False to run a full backtest
        # Set makeInstrumentCsvs to False to not make instrument specific csvs in runLogs. This improves the performance BY A LOT
        tradingSystem.startTrading(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)

        # model_file = "clustered_minmax_lstm_model_1_10_clusters.pkl"
        # problem1Solver.loadModel(filename=model_file)
        # problem1Solver.evaluateModel(outf="error/stock_model_train_error_%s.csv" % "clustered_minmax_lstm_model_1_10_clusters_cp")
        # problem1Solver.evaluateModel(outf="test_error_%s.csv" % "clustered_minmax_lstm_model_1_10_clusters")

        # model_file = "saved_models/models_with_checkpoint/clustered_minmax_lstm_model_1_20_clusters.pkl"
        # problem1Solver.loadModel(filename=model_file)
        # problem1Solver.evaluateModel(outf="error/stock_model_train_error_%s.csv" % "clustered_minmax_lstm_model_1_20_clusters_cp")
        # # problem1Solver.evaluateModel(outf="error/stock_model_test_error_%s.csv" % "clustered_minmax_lstm_model_1_20_clusters_cp")
        #
        # model_file = "saved_models/models_with_checkpoint/clustered_minmax_lstm_model_1_fair_10.pkl"
        # problem1Solver.loadModel(filename=model_file)
        # problem1Solver.evaluateModel(outf="error/stock_model_train_error_%s.csv" % "clustered_minmax_lstm_model_1_fair_10_cp")
        # # problem1Solver.evaluateModel(outf="error/stock_model_test_error_%s.csv" % "clustered_minmax_lstm_model_1_fair_10_cp")
        #
        # model_file = "saved_models/models_with_checkpoint/clustered_minmax_lstm_model_1_fair_20.pkl"
        # problem1Solver.loadModel(filename=model_file)
        # problem1Solver.evaluateModel(outf="error/stock_model_train_error_%s.csv" % "clustered_minmax_lstm_model_1_fair_20_cp")
        # problem1Solver.evaluateModel(outf="error/stock_model_test_error_%s.csv" % "clustered_minmax_lstm_model_1_fair_20_cp")
