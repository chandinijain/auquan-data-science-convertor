import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta as td
import os
  

runLogFolder = 'runLog_20180226_112836'

# scrambler = pd.read_csv('scrambler.csv',index_col=0) 

data = pd.read_csv('runLogs/'+runLogFolder+'/'+'marketFeatures.csv',index_col=0)
predictions = data['predictionString'].apply(lambda x: x.split(', ')).values.tolist().astype(float)

names = next(os.walk('runLogs/'+runLogFolder))[2]
print(names)

for i in names:
	if i=='marketFeatures.csv':
		continue
	try:

		data = pd.read_csv('runLogs/'+runLogFolder+'/'+i,index_col=0)
		predictions = data['predictionString'].apply(lambda x: x.split(', ')).values.tolist().astype(float)
		

		print('Plotting %s'%i)
		plt.plot(data.prediction.rolling(50).mean()[1000:])
		# plt.plot(data.FairValue)
		plt.plot(data.basis.rolling(50).mean()[1000:])
		plt.show()
		plt.plot((data.prediction-data.basis)[1000:])
		plt.show()
	except:

		continue
	# frames = [data1, data2, data3, data4]
	# result = pd.concat(frames)
	# result.sort_index(inplace=True)
	# # print(result[result.index.duplicated(keep=False)])
	# result.fillna(method='pad',inplace=True)
	

