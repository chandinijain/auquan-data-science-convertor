import numpy as np
import pickle, tempfile
import keras
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.callbacks import ModelCheckpoint, TensorBoard, Callback
import sklearn.preprocessing as scalers
from keras.models import load_model
import gc
import pandas as pd
def make_keras_picklable():
    """http://zachmoshe.com/2017/04/03/pickling-keras-models.html
    """
    def __getstate__(self):
        model_str = ""
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            keras.models.save_model(self, fd.name, overwrite=True)
            model_str = fd.read()
        d = { 'model_str': model_str }
        return d

    def __setstate__(self, state):
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            fd.write(state['model_str'])
            fd.flush()
            model = keras.models.load_model(fd.name)
        self.__dict__ = model.__dict__

    cls = keras.models.Model
    cls.__getstate__ = __getstate__
    cls.__setstate__ = __setstate__

def build_model(layers, batch_size=None, timesteps=1, stateful=False):
    model = Sequential()

    model.add(LSTM(
        units=layers[1],
        batch_input_shape=(batch_size, timesteps, layers[0]),
        kernel_regularizer=keras.regularizers.l1_l2(l1=0.0, l2=0.001),
        return_sequences=True
        # bias_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001),
        # recurrent_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001)
        ))
    model.add(Dropout(0.2))

    model.add(LSTM(
        units=layers[2],
        kernel_regularizer=keras.regularizers.l1_l2(l1=0.0, l2=0.001),
        return_sequences=False
        # bias_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001),
        # recurrent_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001)
        ))
    model.add(Dropout(0.2))

    model.add(Dense(
        units=layers[3],
        kernel_regularizer=keras.regularizers.l1_l2(l1=0.0, l2=0.001)
        # bias_regularizer= keras.regularizers.l1_l2(l1=0.0, l2=0.001)
        ))
    model.add(Activation("linear"))

    # start = time.time()
    model.compile(loss="mean_squared_error", optimizer="adam")
    # print("> Compilation Time : ", time.time() - start)
    return model

class History(Callback):
    """
    Callback that records events into a `History` object.

    This callback is automatically applied to
    every Keras model. The `History` object
    gets returned by the `fit` method of models.
    """

    def on_train_begin(self, logs=None):
        if not hasattr(self, 'epoch'):
            self.epoch = []
            self.history = {}

    def on_epoch_end(self, epoch, logs=None):

        logs = logs or {}
        self.epoch.append(epoch)
        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

class TradingModel(object):
    """Base class for trading models"""
    def __init__(self, stocks, stock_clusters, num_features, scaler="MinMaxScaler", feature_range=None):
        """
        Args:
            stocks : List of stock names
            stock_clusters : Dict with keys as stock name and value is the cluster number of that stock
            scaler : Feature scalers- "MinMaxScaler", "StandardScaler"
            feature_range : range of features after scaling (optional parameter in MinMaxScaler)
        """
        self.stocks = stocks
        self.stock_clusters = stock_clusters
        self.num_features = num_features
        self.num_clusters = len(set(self.stock_clusters.values()))
        scaler_method = getattr(scalers, scaler)
        self.normalizers = {cid : scaler_method(feature_range=feature_range) if feature_range else scaler_method() for cid in range(self.num_clusters)}
        self.models = {cid : None for cid in range(self.num_clusters)}
        try:
            with open('history.pkl', 'rb') as fp:
                self.history = pickle.load(fp)
        except:
            self.history = {cid : History() for cid in range(self.num_clusters)}
        self.stock_indexes = dict()
        for cid in range(self.num_clusters):
            self.stock_indexes[cid] = [idx for idx, stock in enumerate(stocks) if self.stock_clusters[stock] == cid]

    def save_model(self, model_name):
        make_keras_picklable()
        model_name = "%s.pkl" % (model_name) if model_name != "" else "checkpoint.pkl"
        with open(model_name, 'wb') as f:
            pickle.dump(self, f)
        with open('%s_history.pkl' % model_name, 'wb') as fp:
            pickle.dump(self.history, fp, protocol=pickle.HIGHEST_PROTOCOL)

    def update(self, stocks, features, labels, epochs=5, batch_size=64, initial_epoch=0, save=True, model_name="", validation_split=0.05):
        """
        Args:
            features : Numpy array of shape (N x d x s)
                       where d = dimension of a data point, N = Number of data points and s = Number of stocks
            labels : Numpy array of shape (N x s)
                     where N = Number of data points and s = Number of stocks
        """
        # assert features.shape[2] == len(self.stocks)
        feature_dict = {stock : features[:, :, s] for s, stock in enumerate(stocks)}
        label_dict = {stock : labels[:, s] for s, stock in enumerate(stocks)}
        self.fit(stocks, feature_dict, label_dict, epochs, batch_size, validation_split, initial_epoch=initial_epoch, update=True)
        if save:
            self.save_model(model_name)



class LSTM_model_1(TradingModel):
    """Stateless LSTM with no look-back"""
    def __init__(self, stocks, stock_clusters, num_features, scaler="MinMaxScaler", feature_range=None):
        super(LSTM_model_1, self).__init__(stocks, stock_clusters, num_features, scaler, feature_range)
        layers = [self.num_features, 50, 100, 1]
        for cid in range(self.num_clusters):
            self.models[cid] = build_model(layers, batch_size=None, timesteps=1, stateful=False)

    def fit(self, stocks, features, labels, epochs=30, batch_size=64, validation_split=0.05, initial_epoch=0, update=False, name=""):
        """
        Args:
            features : Dict with keys as stock name and value as Numpy array of shape (N x d)
                       where d = dimension of a data point and N = Number of data points
            labels : Dict with keys as stock name and value as Numpy array of shape (N x 1)
                     where N = Number of data points
        """
        try:
            with open('%s_history.pkl' % name, 'rb') as fp:
                self.history = pickle.load(fp)
        except:
            self.history = {cid : History() for cid in range(self.num_clusters)}

        for cid in range(self.num_clusters):
            X = [features[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            if len(X) == 0:
                continue
            if not update:
                self.normalizers[cid].fit(np.vstack(X))
            X = [self.normalizers[cid].transform(x) for x in X]
            X = np.vstack([np.expand_dims(x, axis=1) for x in X])
            Y = [labels[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            Y = np.vstack([y.reshape(-1, 1) for y in Y])
            print("CID:", cid, "X shape:", X.shape, "Y shape:", Y.shape)

            history = self.history[cid]

            checkpointer = ModelCheckpoint(filepath='checkpoints/%s_weights.h5' % name , verbose=1, save_best_only=True, period=1)
            # tensorboard = TensorBoard(log_dir='./model_logs', histogram_freq=1, write_graph=True, write_images=True, write_grads=True)
            self.models[cid].fit(X, Y,
                                 batch_size=batch_size,
                                 epochs=epochs,
                                 shuffle=True,
                                 validation_split=validation_split,
                                 initial_epoch=initial_epoch,
                                 callbacks=[checkpointer, history])
            print(history.epoch, history.history)

            del self.models[cid]
            self.models[cid]= load_model('checkpoints/%s_weights.h5' % name)
            self.history[cid] = history

    def evaluate(self, stocks, features, labels, batch_size=1):

        for cid in range(self.num_clusters):
            X = [features[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            if len(X) == 0:
                continue
            X = [self.normalizers[cid].transform(x) for x in X]
            X = np.vstack([np.expand_dims(x, axis=1) for x in X])
            Y = [labels[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            Y = np.vstack([y.reshape(-1, 1) for y in Y])
            print("CID:", cid, "X shape:", X.shape, "Y shape:", Y.shape)

            loss = self.models[cid].evaluate(X, Y,
                                 batch_size=batch_size,
                                 verbose=1)
            print(loss)

    def predict(self, X, stocks):
        """
        Args:
            X : Numpy array of shape (d x s)
                where d = Number of features and s = Number of stocks

        """
        # print(X.shape[1] , len(self.stocks))
        # assert X.shape[1] == len(self.stocks)
        predictions = pd.Series(index=stocks)
        for i in range(len(stocks)):
          #print(i,stocks[i])
          #print(X[:,i])
          stock =stocks[i]
          predictions[stock] = self.models[self.stock_clusters[stock]].predict(np.expand_dims(self.normalizers[self.stock_clusters[stock]].transform(np.expand_dims(X[:,i], axis=0)), axis=1))[0][0]


        return predictions
        #return [self.models[self.stock_clusters[stock]].predict(np.expand_dims(self.normalizers[self.stock_clusters[stock]].transform(np.expand_dims(X[:,i], axis=0)), axis=1))[0][0] for i, stock in enumerate(stocks)]


class LSTM_model_2(TradingModel):
    """Stateful LSTM with no look-back"""
    def __init__(self, stocks, stock_clusters, num_features, scaler="MinMaxScaler", feature_range=None):
        super(LSTM_model_2, self).__init__(stocks, stock_clusters, num_features, scaler, feature_range)
        layers = [self.num_features, 50, 100, 1]
        for cid in range(self.num_clusters):
            self.models[cid] = build_model(layers, batch_size=len(self.stock_indexes[cid]), timesteps=1, stateful=True)

    def fit(self, stocks, features, labels, epochs=30, batch_size=64, validation_split=0.05, initial_epoch=0, update=False):
        """
        Args:
            features : Dict with keys as stock name and value as Numpy array of shape (N x d)
                       where d = dimension of a data point and N = Number of data points
            labels : Dict with keys as stock name and value as Numpy array of shape (N x 1)
                     where N = Number of data points
            batch_size is ignored and is set to number of stocks in a cluster
            validation_split is ignored
        """
        for cid in range(self.num_clusters):
            X = [features[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            if len(X) == 0:
                continue
            assert len(self.stock_indexes[cid]) == len(X)
            if not update:
                self.normalizers[cid].fit(np.vstack(X))
            X = np.array([self.normalizers[cid].transform(x) for x in X])      # s x N x d
            X = np.swapaxes(X, 0, 1).reshape((-1, self.num_features))          # Interleaved stock datapoints (s+N) x d
            X = np.expand_dims(X, axis=1)                                      # (s+N) x 1 x d
            Y = [labels[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            Y = np.array([y.reshape(-1, 1) for y in Y])               # s x N x 1
            Y = np.swapaxes(Y, 0, 1).reshape(-1, 1)                            # Interleaved stock labels (s+N) x 1
            print("CID:", cid, "X shape:", X.shape, "Y shape:", Y.shape, "Num Stocks:", len(self.stock_indexes[cid]))
            for epoch in range(epochs):
                print("Epoch:", epoch)
                self.models[cid].reset_states()
                self.models[cid].fit(X, Y,
                                     batch_size=len(self.stock_indexes[cid]),
                                     epochs=1,
                                     shuffle=False,
                                     validation_split=0.0)
        # NOTE: Models' states are not reset after update. (It may prevent from cold start)

    def predict(self, X, stocks):
        """
        Args:
            X : Numpy array of shape (d x s)
                where d = Number of features and s = Number of stocks
        """
        # assert X.shape[1] == len(self.stocks)
        predictions = np.empty(X.shape[1])
        for cid in range(self.num_clusters):
            predictions.put(self.stock_indexes[cid], self.models[cid].predict_on_batch(np.expand_dims(self.normalizers[cid].transform(X[:, self.stock_indexes[cid]].T), axis=1)).flatten())

        return predictions


class LSTM_model_3(TradingModel):
    """Stateless LSTM with look-back at T timesteps"""
    def __init__(self, stocks, stock_clusters, num_features, timesteps, scaler="MinMaxScaler", feature_range=None):
        super(LSTM_model_3, self).__init__(stocks, stock_clusters, num_features, scaler, feature_range)
        layers = [self.num_features, 50, 100, 1]
        self.timesteps = timesteps
        self.lookback_data = []
        for cid in range(self.num_clusters):
            self.models[cid] = build_model(layers, batch_size=None, timesteps=timesteps, stateful=False)

    def fit(self, stocks, features, labels, epochs=30, batch_size=64, validation_split=0.05, update=False, name=""):
        """
        Args:
            features : Dict with keys as stock name and value as Numpy array of shape (N x d)
                       where d = dimension of a data point and N = Number of data points
            labels : Dict with keys as stock name and value as Numpy array of shape (N x 1)
                     where N = Number of data points
            validation_split and batch_size are ignored
        """
        timesteps = self.timesteps
        for cid in range(self.num_clusters):
            X = [features[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            if len(X) == 0:
                continue
            if not update:
                self.normalizers[cid].fit(np.vstack(X))
            X = np.array([self.normalizers[cid].transform(x) for x in X])
            Y = [labels[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            Y = np.array([y.reshape(-1, 1) for y in Y])                 # s x N x 1
            # shifted_X = np.empty([X.shape[0], X.shape[1], 0, X.shape[2]])
            shifted_X = []
            for w in range(timesteps):
                # print(w)
                shifted_X.insert(0, np.expand_dims(np.roll(X, shift=w, axis=1), axis=2))
            del X
            gc.collect()
            shifted_X = np.concatenate(shifted_X, axis=2)
            X = shifted_X[:, (timesteps-1):, :, :]          # (num_stocks x (N-timesteps) x timesteps x d)
            X = X.reshape(-1, timesteps, X.shape[3])
            Y = Y[:, (timesteps-1):].reshape(-1, 1)
            print("CID:", cid, "X shape:", X.shape, "Y shape:", Y.shape)
            checkpointer = ModelCheckpoint(filepath='checkpoints/%s_weights.h5' % name , verbose=1, save_best_only=True)
            self.models[cid].fit(X, Y,
                                 batch_size=batch_size,
                                 epochs=epochs,
                                 shuffle=True,
                                 validation_split=validation_split,
                                 callbacks=[checkpointer])
            del self.models[cid]
            self.models[cid]= load_model('checkpoints/%s_weights.h5' % name)
            print(self.models[cid].summary())


            # for epoch in range(epochs):
            #     loss = 0
            #     for w in range(X.shape[1] - timesteps):
            #         x = X[:, w:w+timesteps, :]
            #         y = Y[:, w+timesteps-1]
            #         print("xx shape:", x.shape, "yy shape:", y.shape)
            #         loss += self.models[cid].train_on_batch(x, y)
            #     print("Epoch:", epoch, "Loss:", loss/(X.shape[1] - timesteps))

    def updateLookback(self, x, fill=False):
        """
        Args:
            x : numpy array of shape = (d x s)
        """
        if fill:
            self.lookback_data = [x.copy() for i in range(self.timesteps - 1)]
            return
        self.lookback_data.pop()
        self.lookback_data.append(x.copy())
        assert len(self.lookback_data) == (self.timesteps -1)

    def predict(self, X, stocks):
        """
        Args:
            X : Numpy array of shape (d x s)
                where d = Number of features and s = Number of stocks
        """
        # assert X.shape[1] == len(self.stocks)
        return [self.models[self.stock_clusters[stock]].predict(np.expand_dims(self.normalizers[self.stock_clusters[stock]].transform(np.vstack([np.array(self.lookback_data)[:,:,i], X[:,i]])), axis=0))[0][0] for i, stock in enumerate(stocks)]


class LSTM_model_4(TradingModel):
    """Stateful LSTM with look-back at T timesteps"""
    def __init__(self, stocks, stock_clusters, num_features, timesteps, scaler="MinMaxScaler", feature_range=None):
        super(LSTM_model_4, self).__init__(stocks, stock_clusters, num_features, scaler, feature_range)
        self.timesteps = timesteps
        self.lookback_data = []
        layers = [self.num_features, 50, 100, 1]
        for cid in range(self.num_clusters):
            self.models[cid] = build_model(layers, batch_size=len(self.stock_indexes[cid]), timesteps=timesteps, stateful=True)

    def fit(self, stocks, features, labels, epochs=30, batch_size=64, validation_split=0.05, update=False):
        """
        Args:
            features : Dict with keys as stock name and value as Numpy array of shape (N x d)
                       where d = dimension of a data point and N = Number of data points
            labels : Dict with keys as stock name and value as Numpy array of shape (N x 1)
                     where N = Number of data points
            batch_size is ignored and is set to number of stocks in a cluster
            validation_split is ignored
        """
        timesteps = self.timesteps
        for cid in range(self.num_clusters):
            X = [features[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            if len(X) == 0:
                continue
            if not update:
                self.normalizers[cid].fit(np.vstack(X))
            X = np.array([self.normalizers[cid].transform(x) for x in X])
            Y = [labels[stock] for stock in stocks if self.stock_clusters[stock] == cid]
            Y = np.array([y.reshape(-1, 1) for y in Y])
            for epoch in range(epochs):
                loss = 0
                self.models[cid].reset_states()
                for w in range(X.shape[1] - timesteps):
                    x = X[:, w:w+timesteps, :]
                    y = Y[:, w+timesteps-1]
                    print("xx shape:", x.shape, "yy shape:", y.shape)
                    loss += self.models[cid].train_on_batch(x, y)
                print("Epoch:", epoch, "Loss:", loss/(X.shape[1] - timesteps))

    def updateLookback(self, x, fill=False):
        """
        Args:
            x : numpy array of shape = (d x s)
        """
        if fill:
            self.lookback_data = [x.copy() for i in range(self.timesteps - 1)]
            return
        self.lookback_data.pop()
        self.lookback_data.append(x.copy())
        assert len(self.lookback_data) == (self.timesteps -1)

    def predict(self, X, stocks):
        """
        Args:
            X : Numpy array of shape (d x s)
                where d = Number of features and s = Number of stocks
        """
        # assert X.shape[1] == len(self.stocks)

        predictions = np.empty(X.shape[1])
        xd = np.vstack(np.array(self.lookback_data), np.expand_dims(X, axis=0))
        for cid in range(self.num_clusters):
            predictions.put(self.stock_indexes[cid], self.models[cid].predict_on_batch(self.normalizers[cid].transform(np.rollaxis(xd[:,:, self.stock_indexes[cid]], axis=2))).flatten())

        return predictions
