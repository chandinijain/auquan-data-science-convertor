from backtester.trading_system_parameters import TradingSystemParameters
from backtester.executionSystem.basis_execution_system import BasisExecutionSystem
from backtester.constants import *
from backtester.features.feature import Feature
from backtester.logger import *
from backtester.trading_system import TradingSystem
import pandas as pd
from trading_models import *
import numpy as np

import pickle
import requests
import keras
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
import types
import tempfile
import datetime

def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

def is_in_expiry_week(date):
    last_thursday = calc_last_thursday(date)
    return (last_thursday - date).days < 7

class LSTMTradingFunctions():

    def __init__(self, stocks, model=None, model_path='', startdate='20170101', period=5, expiry=None, update=True):
        self.stocks = stocks
        self.count = 0
        self.__featureKeys = []
                        #'KFW': 1, 'VKE': 1, 'IFL': 8
        self.X = []     # Container to store features for update
        self.Y = []     # Container to store labels for update
        self.period = period
        self.halflife = 4*period
        self.update = update
        self.FLAG = True
        self.today = startdate
        self.expiry = expiry
        self.normal_trading_model = None
        self.expiry_trading_model = None
        if model is None:
            self.loadModel(filename=model_path)
        else:
            self.normal_trading_model = model['normal']
            self.expiry_trading_model = model['expiry']

        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)

    def getSymbolsToTrade(self):
        print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]

    def loadModel(self, filename=[]):
        '''
        Load model paramters for each stock using pickle file
        '''
        make_keras_picklable()
        with open(filename[0], "rb") as f:
            self.normal_trading_model = pickle.load(f)
        with open(filename[1], "rb") as f:
            self.expiry_trading_model = pickle.load(f)
        print(self.normal_trading_model)
        print(self.normal_trading_model.stocks)
        print(self.normal_trading_model.stock_clusters)

    def evaluateModel(self, outf="", evalCluster=False):
        if evalCluster:
            err_df = pd.DataFrame(index=list(np.arange(self.trading_model.num_clusters))+self.stocks, columns=["name", "mse", "rmse", "clusterID"])
        else:
            err_df = pd.DataFrame(index=self.stocks, columns=["name", "mse", "rmse", "clusterID"])
        X = np.array(self.X)
        Y = np.array(self.Y)
        for s, stock in enumerate(self.stocks):
            mse = self.trading_model.models[self.trading_model.stock_clusters[stock]].evaluate(
                        x=np.expand_dims(self.trading_model.normalizers[self.trading_model.stock_clusters[stock]].transform(X[:,:,s]), axis=1),
                        y=Y[:,s],
                        batch_size=1)
            rmse = np.sqrt(mse)
            err_df.loc[stock] = [self.unscrambler.loc[stock].values[0], mse, rmse, self.trading_model.stock_clusters[stock]]
        if evalCluster:
            for cid in range(self.trading_model.num_clusters):
                x = np.vstack([X[:,:,s] for s,stock in enumerate(self.stocks) if self.trading_model.stock_clusters[stock] == cid])
                y = np.vstack([Y[:,s].reshape(-1,1) for s,stock in enumerate(self.stocks) if self.trading_model.stock_clusters[stock] == cid])
                cluster_mse = self.trading_model.models[cid].evaluate(
                            x=np.expand_dims(self.trading_model.normalizers[cid].transform(x), axis=1),
                            y=y,
                            batch_size=1)
                cluster_rmse = np.sqrt(cluster_mse)
                err_df.loc[cid] = ["cluster_%s" % cid, cluster_mse, cluster_rmse, cid]
        print(err_df)
        err_df.to_csv(outf)

    def getInstrumentFeatureConfigDicts(self):

        generate_features = []
        featureList = []
        real_features = ['stockVWAP', 'futureVWAP', 'basis', 'stockTopBidPrice',
                        'stockTopAskPrice', 'futureTopBidPrice', 'futureTopAskPrice',
                        'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidPrice', 'futureNextAskPrice',
                        'stockAverageBidPrice', 'stockAverageAskPrice', 'futureAverageBidPrice', 'futureAverageAskPrice']
        for rf in real_features:
            ma = {'featureKey': 'ma' + rf,
                     'featureId': 'moving_average',
                     'params': {'period': self.period,
                                'featureName': rf}}
            moving_min = {'featureKey': 'moving_min' +rf,
                        'featureId': 'moving_min',
                        'params': {'period': self.period,
                                   'featureName': rf}}
            moving_max = {'featureKey': 'moving_max' +rf,
                        'featureId': 'moving_max',
                        'params': {'period': self.period,
                                   'featureName': rf}}
            sdev = {'featureKey': 'sdev' + rf,
                      'featureId': 'moving_sdev',
                      'params': {'period': self.period,
                                 'featureName': rf}}
            expma = {'featureKey': 'expma' + rf,
                   'featureId': 'exponential_moving_average',
                   'params': {'period': self.halflife,
                                'featureName': rf}}
            diff_ma = {'featureKey': 'diff_ma' + rf,
                     'featureId': 'diff_ma',
                     'params': {'period': self.period,
                                'featureName': rf}}
            # moving_sum = {'featureKey': 'moving_sum' + rf,
            #           'featureId': 'moving_sum',
            #           'params': {'period': self.period,
            #                      'featureName': rf}}

            generate_features += [ma, moving_min, moving_max, sdev, expma, diff_ma]
            featureList += [f_id + rf for f_id in ['ma', 'moving_min', 'moving_max', 'sdev', 'expma', 'diff_ma']]

        moving_sum = {'featureKey': 'moving_sum',
                  'featureId': 'moving_sum',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        delay = {'featureKey': 'delay',
                  'featureId': 'delay',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        diff = {'featureKey': 'diff',
                  'featureId': 'difference',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        rank = {'featureKey': 'rank',
                  'featureId': 'rank',
                  'params': {'period': self.period,
                             'featureName': 'basis'}}
        scale = {'featureKey': 'scale',
                  'featureId': 'scale',
                  'params': {'period': self.period,
                             'featureName': 'basis', 'scale': 3}}
        tte = {'featureKey': 'tte',
                 'featureId': 'time_to_expiry',
                 'params': {} if self.expiry is None else {'expiry':self.expiry}}

        generate_features += [moving_sum, delay, diff, rank, scale, tte]
        featureList += ['moving_sum', 'delay', 'diff', 'rank', 'scale', 'tte']

        # ratio of moving average
        sf_pairs = [('stockTopBidVol', 'futureTopBidVol'), ('stockTopAskVol', 'futureTopAskVol'),
        ('stockNextBidVol', 'futureNextBidVol'), ('stockNextAskVol', 'futureNextAskVol'),
        ('stockTotalBidVol', 'futureTotalBidVol'), ('stockTotalAskVol', 'futureTotalAskVol'),
        ('totalTradedStockVolume', 'totalTradedFutureVolume'), ('totalTradedStockValue', 'totalTradedFutureValue')]

        for S, F in sf_pairs:
            ratio_ma = {'featureKey': 'ratio_ma' + S + F,
                      'featureId': 'ratio_ma',
                      'params': {'period': self.period,
                                 'featureName1': F, 'featureName2': S}}
            generate_features += [ratio_ma]
            featureList += ['ratio_ma' + S + F]

        fairValueDict = {'featureKey': 'FairValue',
                     'featureId': 'moving_average',
                     'params': {'period': self.period,
                                'featureName': 'basis'}}

        generate_features += [fairValueDict]
        self.setFeatureKeys(featureList)
        print(self.__featureKeys)
        return generate_features

    def getPrediction(self, time, updateNum, instrumentManager):
        # holder for all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        #predictedFairValue = []
        #x = []
        #training_x = []
        #for f in self.__featureKeys:
        #    data = lookbackInstrumentFeatures.getFeatureDf(f).fillna(0)
        #    x.append(np.array(data.iloc[-1]))
        #    if len(data) > self.period:
        #        training_x.append(np.array(data.iloc[-self.period-1]))

        #if self.update:# and len(training_x)>0:
        #    fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')
        #    self.X.append(np.array(x))                        # shape = d x s
        #    self.Y.append(np.array(fairValueData.iloc[-1]))   # shape = 1 x s
        #    if (self.today != datetime.strftime(time.date(),'YYYYMMDD')):
        #        self.today = datetime.strftime(time.date(),'YYYYMMDD')
        #        print(self.today, "Updating model...")
        #        self.trading_model.update(self.stocks, np.array(self.X), np.array(self.Y), epochs=5, save=True, model_name="test_model_1.pkl")

        #x = np.array(x)     # shape = d x s
        print("Updatenum:", updateNum)
        x = []
        for f in self.__featureKeys:
            data = lookbackInstrumentFeatures.getFeatureDf(f).fillna(0)
            x.append(np.array(data.iloc[-1]))

        fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')
        x = np.array(x)     # shape = d x s
        if self.expiry_trading_model is None:
          predictedFairValue = self.normal_trading_model.predict(x, self.stocks)
        elif self.normal_trading_model is None:
          predictedFairValue = self.expiry_trading_model.predict(x, self.stocks)
        else:
          if is_in_expiry_week(time):
            print("EXPIRY WEEK")
            predictedFairValue = self.expiry_trading_model.predict(x, self.stocks)
          else:
            predictedFairValue = self.normal_trading_model.predict(x, self.stocks)
        idx = [self.unscrambler.loc[i, 'Name'] for i in self.stocks]
        predictedFairValue.rename(lambda x: self.unscrambler.loc[x, 'Name'], inplace=True)
        return predictedFairValue

    def getFeatureKeys(self):
        return self.__featureKeys

    def setFeatureKeys(self, featureList):
        self.__featureKeys = featureList
