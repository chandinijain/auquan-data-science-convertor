import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta as td
import os

def writecsv(csv_dir, results, m):
     # results = results.sort_index(axis=0, ascending=False)
      print('writing %s%s.csv' % (csv_dir, m))
      fileName = '%s%s.csv' % (csv_dir, m)
      if os.path.exists(fileName):
          csv_file = open('%s%s.csv' % (csv_dir, m), 'a')
          results.to_csv(csv_file, header=False)
      else:
          csv_file = open('%s%s.csv' % (csv_dir, m), 'wb')
          results.to_csv(csv_file, header=True)
      csv_file.close()
  

datasetId = 'OSDataP1'

scrambler = pd.read_csv('scrambler.csv',index_col=0)
#scrambler.set_index('Unnamed: 0', inplace=True)
print(scrambler)

for i in scrambler.index:
	try:
		data = pd.read_csv('historicalData/'+datasetId+'/'+scrambler.loc[i,'Name']+'.csv',index_col=0)
	except IOError:
		continue
	val = scrambler.loc[i,'AddVal']
	data['stockVWAP'] -= val 
	data['futureVWAP'] -= val 
	data['stockTopBidPrice'] -= val 
	data['stockTopAskPrice'] -= val 
	data['futureTopBidPrice'] -= val 
	data['futureTopAskPrice'] -= val 
	data['stockNextBidPrice'] -= val 
	data['stockNextAskPrice'] -= val 
	data['futureNextBidPrice'] -= val 
	data['futureNextAskPrice'] -= val 
	data['stockAverageBidPrice'] -= val 
	data['stockAverageAskPrice'] -= val 
	data['futureAverageBidPrice'] -= val 
	data['futureAverageAskPrice'] -= val
	data.dropna(inplace=True)
	missing_data = pd.read_csv('historicalData/unscrambledData/Oct9Data'+'/'+i,index_col=0)
	frames = [data, missing_data]
	result = pd.concat(frames)
	result.sort_index(inplace=True)
	# print(result[result.index.duplicated(keep=False)])
	result.fillna(method='pad',inplace=True)

	print(result.loc[missing_data.index[0:4]])
	print(result.iloc[0:4])
	del result['totalTradedFutureValue']
	del result['totalTradedFutureVolume']
	del result['totalTradedStockValue']
	del result['totalTradedStockVolume']
	# print(data.index+td(days=0))
	# data.set_index(data.index+td(days=0), inplace=True)
	writecsv('historicalData/unscrambledData/'+datasetId+'/', result, scrambler.loc[i,'Name'])
	# 2 = 77
	# 1 = 133
	# 3 = 189
