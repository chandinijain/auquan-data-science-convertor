from backtester.trading_system_parameters import TradingSystemParameters
from backtester.executionSystem.basis_execution_system import BasisExecutionSystem
from backtester.constants import *
from backtester.features.feature import Feature
from backtester.logger import *
from backtester.trading_system import TradingSystem
import numpy as np
import pandas as pd
from live_alpha_order_placer import LiveAlphaOrderPlacer
from alpha_grep_data_source import AlphaGrepDataSource
from live_time_rule import LiveTimeRule
from backtesting_time_rule import BacktestingTimeRule
from datetime import datetime, timedelta
from backtester.timeRule.us_time_rule import USTimeRule
from backtester.orderPlacer.backtesting_order_placer import BacktestingOrderPlacer

import pickle
import requests
import keras
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
import types
import tempfile


class LSTMTradingFunctions():

    def __init__(self, stocks, model_path):
        self.stocks = stocks
        self.loadModelParams(filename=model_path)
        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)
        self.count = 0
        self.params = {}
        self.__featureKeys = []

    def getSymbolsToTrade(self):
        print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]

    def loadModelParams(self, filename=""):
        """
        Load model paramters for each stock using pickle file
        """
        with open(filename, "rb") as f:
            self.stock_cluster, self.normalizer = pickle.load(f)
            # print(self.stock_cluster)
            # print(self.normalizer)

        self.model = dict()
        for i in range(6):
            self.model[i] = load_model("problem1_lstm_%d.h5" % i)
            # print(self.model[i].summary())

    def getInstrumentFeatureConfigDicts(self):

        basisDict = {'featureKey': 'basis',
                     'featureId': 'basis_calculator',
                     'params': {'instr1bid': 'stockTopBidPrice',
                                'instr1ask': 'stockTopAskPrice',
                                'instr2bid': 'futureTopBidPrice',
                                'instr2ask': 'futureTopAskPrice'}}

        real_features = ['basis', 'stockVWAP', 'futureVWAP', 'stockTopBidPrice', 'stockTopAskPrice', 'futureTopBidPrice',
                         'futureTopAskPrice', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidPrice',
                         'futureNextAskPrice', 'stockAverageBidPrice', 'stockAverageAskPrice', 'futureAverageBidPrice',
                         'futureAverageAskPrice']

        # generate_features = [basisDict]
        generate_features = []
        featureList = []
        for rf in real_features:
            ma_5 = {'featureKey': 'ma_5' + rf,
                    'featureId': 'moving_average',
                    'params': {'period': 5*60/10,
                               'featureName': rf}}
            expma = {'featureKey': 'expma' + rf,
                     'featureId': 'exponential_moving_average',
                     'params': {'period': 20*60/10,
                                'featureName': rf}}
            sdev_5 = {'featureKey': 'sdev_5' + rf,
                      'featureId': 'moving_sdev',
                      'params': {'period': 5*60/10,
                                 'featureName': rf}}

            generate_features += [ma_5, sdev_5, expma]
            featureList += [f_id + rf for f_id in ['ma_5', 'sdev_5', 'expma']]

        moving_min = {'featureKey': 'moving_min',
                      'featureId': 'moving_min',
                      'params': {'period': 5*60/10,
                                 'featureName': 'basis'}}
        moving_sum = {'featureKey': 'moving_sum',
                      'featureId': 'moving_sum',
                      'params': {'period': 5*60/10,
                                 'featureName': 'basis'}}
        delay = {'featureKey': 'delay',
                 'featureId': 'delay',
                 'params': {'period': 5*60/10,
                            'featureName': 'basis'}}
        diff = {'featureKey': 'diff',
                'featureId': 'difference',
                'params': {'period': 5*60/10,
                             'featureName': 'basis'}}
        rank = {'featureKey': 'rank',
                'featureId': 'rank',
                'params': {'period': 5*60/10,
                             'featureName': 'basis'}}
        scale = {'featureKey': 'scale',
                 'featureId': 'scale',
                 'params': {'period': 5*60/10,
                            'featureName': 'basis', 'scale': 3}}

        generate_features += [moving_min, moving_sum, delay, diff, rank, scale]
        featureList += ['moving_min', 'moving_sum', 'delay', 'diff', 'rank', 'scale']
        self.setFeatureKeys(featureList)
        return generate_features

    def getPrediction(self, time, updateNum, instrumentManager):

        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        predictedFairValue = []
        x_star = []             # Data point at time t (whose FairValue will be predicted)
        for f in self.__featureKeys:
            data = lookbackInstrumentFeatures.getFeatureDf(f).fillna(0)
            x_star.append(np.array(data.iloc[-1]))
        x_star = np.array(x_star)                                      # shape = d x s
        x_star = np.vstack((x_star, np.power(x_star[0, :], 2)))          # shape = (d+1) x s

        predictedFairValue = [self.model[self.stock_cluster[stock]].predict(np.expand_dims(self.normalizer[self.stock_cluster[stock]].transform(np.array([x_star[:, i]])), axis=1), batch_size=1)[
            0][0] for i, stock in enumerate(self.stocks)]
        print(predictedFairValue)

        # print("UPDATENUM", updateNum)
        return predictedFairValue

    def getFeatureKeys(self):
        return self.__featureKeys

    def setFeatureKeys(self, featureList):
        self.__featureKeys = featureList
