import pandas as pd
from backtester.timeRule.us_time_rule import USTimeRule
import pandas as pd

testDt = USTimeRule('2012/02/07', '2012/02/27', frequency='M').createBusinessMinSeries()

csv = pd.read_csv('market_data.csv')

symbols = ['SIZ','MLQ','MAI','PVV','IPV','DHP','EKA','EYC','YSB','SEP','INS','IIZ','DFY','OAX']
j = 1
for s in symbols:
    f = open(s+".csv", "a")
    f.write('datetime,ask,bid\n')
    i = 0
    for dt in testDt:
      f.write("%s,%s,%s\n"%(dt,csv['ask_'+str(j)][i],csv['bid_'+str(j)][i]))
      i+=1
    j+=1