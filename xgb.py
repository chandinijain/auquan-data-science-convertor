from backtester.features.feature import Feature
from backtester.trading_system import TradingSystem
from backtester.sample_scripts.fair_value_params import FairValueTradingParams
from backtester.version import updateCheck
import os
import pickle
import datetime
import numpy as np
import pandas as pd
import xgboost as xgb



class XGBTradingFunctions():

    def __init__(self, stocks, model_path, startdate, update=True):
        self.stocks = stocks
        self.count = 0
        self.today = startdate
        self.loadModel(filename=model_path)
        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)

    def getSymbolsToTrade(self):
        print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]

    def getCustomFeatures(self):
        return {'my_custom_feature': MyCustomFeature}

    def loadModel(self, filename=""):
        ################## DOWNLOAD MY FINAL_MODEL.P FILE ##############################
        print "Downloading Model File"
        os.system('wget https://www.dropbox.com/s/aghemtvia70ft17/final_modelsP1_opt.p')
        final_model = pickle.load(open('./final_modelsP1_opt.p'))

        print "Model File Downloaded"

    def getInstrumentFeatureConfigDicts(self):
        features = []
        ema1Dict = {'featureKey': 'ema_5',
                   'featureId': 'exponential_moving_average',
                   'params': {'period': 5,
                              'featureName': 'basis'}}
        features.append(ema1Dict)
        ema2Dict = {'featureKey': 'ema_15',
                   'featureId': 'exponential_moving_average',
                   'params': {'period': 15,
                              'featureName': 'basis'}}
        features.append(ema2Dict)
        ema3Dict = {'featureKey': 'ema_60',
                   'featureId': 'exponential_moving_average',
                   'params': {'period': 60,
                              'featureName': 'basis'}}
        features.append(ema3Dict)
        ema4Dict = {'featureKey': 'ema_180',
                   'featureId': 'exponential_moving_average',
                   'params': {'period': 180,
                              'featureName': 'basis'}}
        features.append(ema4Dict)
        sdev1Dict = {'featureKey': 'sdev_5',
                    'featureId': 'moving_sdev',
                    'params': {'period': 5,
                               'featureName': 'basis'}}
        features.append(sdev1Dict)
        sdev2Dict = {'featureKey': 'sdev_15',
                    'featureId': 'moving_sdev',
                    'params': {'period': 15,
                               'featureName': 'basis'}}
        features.append(sdev2Dict)
        sdev3Dict = {'featureKey': 'sdev_60',
                    'featureId': 'moving_sdev',
                    'params': {'period': 60,
                               'featureName': 'basis'}}
        features.append(sdev3Dict)
        sdev4Dict = {'featureKey': 'sdev_180',
                    'featureId': 'moving_sdev',
                    'params': {'period': 180,
                               'featureName': 'basis'}}
        features.append(sdev4Dict)
        maxDict = {'featureKey': 'max_30',
                    'featureId': 'moving_max',
                    'params': {'period': 30,
                               'featureName': 'basis'}}
        features.append(maxDict)
        minDict = {'featureKey': 'min_30',
                    'featureId': 'moving_min',
                    'params': {'period': 30,
                               'featureName': 'basis'}}
        features.append(minDict)
        mom1Dict = {'featureKey': 'mom_1',
                    'featureId': 'momentum',
                    'params': {'period': 1,
                               'featureName': 'basis'}}
        features.append(mom1Dict)
        mom2Dict = {'featureKey': 'mom_2',
                    'featureId': 'momentum',
                    'params': {'period': 2,
                               'featureName': 'basis'}}
        features.append(mom2Dict)
        mom3Dict = {'featureKey': 'mom_5',
                    'featureId': 'momentum',
                    'params': {'period': 5,
                               'featureName': 'basis'}}
        features.append(mom3Dict)
        mom4Dict = {'featureKey': 'mom_10',
                    'featureId': 'momentum',
                    'params': {'period': 10,
                               'featureName': 'basis'}}
        features.append(mom4Dict)
        bollingerDict = {'featureKey': 'bollinger_20',
                    'featureId': 'bollinger_bands',
                    'params': {'period': 20,
                               'featureName': 'basis'}}
        # features.append(bollingerDict)
        macd1Dict = {'featureKey': 'mac_15',
                    'featureId': 'macd',
                    'params': {'period1': 5,
                                'period2': 15,
                               'featureName': 'basis'}}
        features.append(macd1Dict)
        macd2Dict = {'featureKey': 'mac_60',
                    'featureId': 'macd',
                    'params': {'period1': 5,
                                'period2': 60,
                               'featureName': 'basis'}}
        features.append(macd2Dict)


        return features

    def getPrediction(self, time, updateNum, instrumentManager):
        # Global ARIMA MODELS
        global stocks, final_model, features
        # holder for all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        # dataframe for a historical instrument feature (ma_5 in this case). The index is the timestamps
        # atmost upto lookback data points. The columns of this dataframe are the stock symbols/instrumentIds.
        tempdf = pd.DataFrame(data=None, columns=stocks)

        basisData = lookbackInstrumentFeatures.getFeatureDf('basis')
        tempdf.loc['basis'] = basisData.iloc[-1]
        ema5Data = lookbackInstrumentFeatures.getFeatureDf('ema_5')
        tempdf.loc['ema_5'] = ema5Data.iloc[-1]
        ema15Data = lookbackInstrumentFeatures.getFeatureDf('ema_15')
        tempdf.loc['ema_15'] = ema15Data.iloc[-1]
        ema60Data = lookbackInstrumentFeatures.getFeatureDf('ema_60')
        tempdf.loc['ema_60'] = ema60Data.iloc[-1]
        ema180Data = lookbackInstrumentFeatures.getFeatureDf('ema_180')
        tempdf.loc['ema_180'] = ema180Data.iloc[-1]
        sdev5Data = lookbackInstrumentFeatures.getFeatureDf('sdev_5')
        tempdf.loc['sdev_5'] = sdev5Data.iloc[-1]
        sdev15Data = lookbackInstrumentFeatures.getFeatureDf('sdev_15')
        tempdf.loc['sdev_15'] = sdev15Data.iloc[-1]
        sdev60Data = lookbackInstrumentFeatures.getFeatureDf('sdev_60')
        tempdf.loc['sdev_60'] = sdev60Data.iloc[-1]
        sdev180Data = lookbackInstrumentFeatures.getFeatureDf('sdev_180')
        tempdf.loc['sdev_180'] = sdev180Data.iloc[-1]
        max30Data = lookbackInstrumentFeatures.getFeatureDf('max_30')
        tempdf.loc['max_30'] = max30Data.iloc[-1]
        min30Data = lookbackInstrumentFeatures.getFeatureDf('min_30')
        tempdf.loc['min_30'] = min30Data.iloc[-1]
        mom1Data = lookbackInstrumentFeatures.getFeatureDf('mom_1')
        tempdf.loc['mom_1'] = mom1Data.iloc[-1]
        mom2Data = lookbackInstrumentFeatures.getFeatureDf('mom_2')
        tempdf.loc['mom_2'] = mom2Data.iloc[-1]
        mom5Data = lookbackInstrumentFeatures.getFeatureDf('mom_5')
        tempdf.loc['mom_5'] = mom5Data.iloc[-1]
        mom10Data = lookbackInstrumentFeatures.getFeatureDf('mom_10')
        tempdf.loc['mom_10'] = mom10Data.iloc[-1]
        mac15Data = lookbackInstrumentFeatures.getFeatureDf('mac_15')
        tempdf.loc['mac_15'] = mac15Data.iloc[-1]
        mac60Data = lookbackInstrumentFeatures.getFeatureDf('mac_60')
        tempdf.loc['mac_60'] = mac60Data.iloc[-1]

        # print tempdf
        # ema5 = ema5Data.iloc[-1]
        # return ema5

        # Returns a series with index as all the instrumentIds. This returns the value of the feature at the last
        # time update.
        # if(basisData.shape[0]<2):
        basis = basisData.iloc[-1]
        fairValue = pd.Series(data=None, index=basis.index)
        # print fairValue

        for i, cols in enumerate(basisData.columns):
            tmp_mdl = final_model[cols]
            test_series = tempdf[cols]
            test_df = pd.DataFrame([test_series])
            test_df = test_df[features]
            dtest = xgb.DMatrix(test_df)
            preds = tmp_mdl.predict(dtest)
            # print preds
            fairValue.loc[cols] = preds[0]

        return fairValue
        





    #     basis = basisData.iloc[-1]
    # # print basis
    #     try:
    #         fairValue = pd.Series(data=None, index=basis.index)
    #         for i,cols in enumerate(basisData.columns):
    #             tmp_mdl = arima_models[cols]
    #             start_date = basisData.index[basisData.shape[0]-1]
    #             end_date = start_date + datetime.timedelta(minutes=5)
    #             # print start_date, end_date
    #             # tmp_mdl.fit(basisData[cols])
    #             f = tmp_mdl.predict(start=start_date, end=end_date)
    #             fairValue.loc[cols] = np.mean(f)
                
    #         return fairValue
    #         # print cols, fairValue.loc[cols]
    #     except: 
    #         ema5 = ema5Data.iloc[-1]
    #         return ema5




class MyCustomFeature(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        # Custom parameter which can be used as input to computation of this feature
        param1Value = featureParams['param1']

        # A holder for the all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        # dataframe for a historical instrument feature (basis in this case). The index is the timestamps
        # atmost upto lookback data points. The columns of this dataframe are the stocks/instrumentIds.
        lookbackInstrumentBasis = lookbackInstrumentFeatures.getFeatureDf('basis')

        # The last row of the previous dataframe gives the last calculated value for that feature (basis in this case)
        # This returns a series with stocks/instrumentIds as the index.
        currentBasisValue = lookbackInstrumentBasis.iloc[-1]

        if param1Value == 'value1':
            return currentBasisValue * 0.1
        else:
            return currentBasisValue * 0.5


# stocks = ['DCO', 'XYR', 'PQS', 'NSL', 'TWK', 'PFK', 'IUE', 'KAA', 'LPQ', 'ZLX', 'ILW', 'BSN', 'OMP', 'SCN', 'YHW', 'AGW', 'CUN', 'KKG', 'XPV', 'FFA']
# features = ['basis','ema_5','ema_15','ema_60','ema_180','sdev_5','sdev_15','sdev_60','sdev_180','max_30','min_30','mom_2', 'mom_5', 'mom_10','mac_15','mac_60']

if __name__ == "__main__":
    if updateCheck():
        print('Your version of the auquan toolbox package is old. Please update by running the following command:')
        print('pip install -U auquan_toolbox')
    else:
        problem1Solver = Problem1Solver()
        tsParams = FairValueTradingParams(problem1Solver)
        tradingSystem = TradingSystem(tsParams)
        # Set shouldPlot to True to quickly generate csv files with all the features
        # Set onlyAnalyze to False to run a full backtest
        # Set makeInstrumentCsvs to True to make instrument specific csvs in runLogs. This degrades the performance of the backtesting system
        tradingSystem.startTrading(onlyAnalyze=False, shouldPlot=True, makeInstrumentCsvs=False)