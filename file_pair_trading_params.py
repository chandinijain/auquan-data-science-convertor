# TODO: This is a temporay way so that you can run this anywhere and backtester from auquantoolbox
# can be anywhere.
import sys
sys.path.insert(0, "../auquantoolbox")

from backtester.trading_system_parameters import TradingSystemParameters
from backtester.executionSystem.basis_execution_system import BasisExecutionSystem
from backtester.constants import *
from backtester.features.feature import Feature
from backtester.logger import *
from backtester.trading_system import TradingSystem
import numpy as np
import pandas as pd
from live_alpha_order_placer import LiveAlphaOrderPlacer
from alpha_grep_data_source import AlphaGrepDataSource
from live_time_rule import LiveTimeRule
from backtesting_time_rule import BacktestingTimeRule
from datetime import datetime, timedelta
from backtester.timeRule.us_time_rule import USTimeRule
from backtester.orderPlacer.backtesting_order_placer import BacktestingOrderPlacer
import os
import pickle
import requests
import keras
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
import types
import tempfile

from LSTM2 import LSTMTradingFunctions

STOCKS = ['AGW','AIO','BSN','CHV','CUN','DCO',
          'FFA','FFS','IES','ILW','IMC','IUQ',
          'IYU','JYW','KAA','KKG','KRZ','LPQ',
          'MUF','NYO','OMP','PFK','PQS','PUO',
          'QRK','TWK','WAG','XPV','XYR','YHW','ZLX']

DATE_TO_TRADE = '20170831'
BOOK_DATA_FILE = '../repo.auquan/bookData-' + DATE_TO_TRADE

ORDER_PLACER_FILE = '../repo.auquan/orderPlacer.txt'
ORDER_CONFIRMER_FILE = '../repo.auquan/orderConfirmer-' + DATE_TO_TRADE
INSTRUMENT_UPDATE_PROCESSED_FILE = "../repo.auquan/instrumentUpdateProcessed.txt"
FREQUENCY_TRADE_SECS = 30

IS_LIVE = False


class LivePairTradingParams(TradingSystemParameters):

    def __init__(self, tradingFunctions):
        self.__tradingFunctions = tradingFunctions
        self.__bookDataFile = BOOK_DATA_FILE
        self.__orderPlacerFile = ORDER_PLACER_FILE
        self.__orderConfirmerFile = ORDER_CONFIRMER_FILE
        self.__instrumentUpdateProcessedFile = INSTRUMENT_UPDATE_PROCESSED_FILE
        super(LivePairTradingParams, self).__init__()
        if IS_LIVE:
            self.__startTime = datetime.now()
            self.__endTime = self.__startTime + timedelta(seconds=900)
        else:
            self.__startTime = datetime.strptime(DATE_TO_TRADE, '%Y%m%d') + timedelta(hours=9, minutes=15)
            self.__endTime = datetime.strptime(DATE_TO_TRADE, '%Y%m%d') + timedelta(hours=15, minutes=30)
        self.__instrumentIds = self.__tradingFunctions.getSymbolsToTrade()
        self.__lotSizes = pd.read_csv('../fo_mktlots.csv',index_col=1)
        self.__lotSizes.set_index(self.__lotSizes.index.str.strip(), inplace=True)

    def getStartingCapital(self):
        return 200000000

    def getDataParser(self):
        # Change last parameter to false, if you want to read from an already written file as opposed to tailing from a file.
        return AlphaGrepDataSource(self.__instrumentIds, self.__bookDataFile, self.__instrumentUpdateProcessedFile, False, IS_LIVE)

    def getTimeRuleForUpdates(self):
        # TODO: Better implementation of this and end time here matching actual trading end time
        # TODO: Might have to take care of trading days too in this later on
        return LiveTimeRule(self.__startTime, FREQUENCY_TRADE_SECS, self.__endTime)

    def getBenchmark(self):
        return None


    def getCustomFeatures(self):
        return {'prediction_feature': PredictionFeature,
               'enter_price':EnterPrice,
               'enter_flag':EnterFlag,
                'basis_calculator': BasisCalculator,
                'spread': SpreadCalculator,
                'total_fees': TotalFeesCalculator}

    def getInstrumentFeatureConfigDicts(self):
        stockFeatureConfigs = self.__tradingFunctions.getInstrumentFeatureConfigDicts()

        fairValuePrediction = {'featureKey': 'prediction',
                               'featureId': 'prediction_feature',
                               'params': {'lstm': self.__tradingFunctions}}
        enterPriceDict = {'featureKey': 'enter_price',
                     'featureId': 'enter_price',
                     'params': {'price': self.getPriceFeatureKey()}}
        enterFlagDict = {'featureKey': 'enter_flag',
                     'featureId': 'enter_flag',
                     'params': {}}
        scoreDict = {'featureKey': 'score',
                     'featureId': 'prob1_score',
                     'params': {'predictionKey': 'prediction',
                                'price': 'basis'}}
        sdevDictForExec = {'featureKey': 'sdev_5_for_exec',
                           'featureId': 'moving_sdev',
                           'params': {'period': 60*60/FREQUENCY_TRADE_SECS,
                                      'featureName': 'basis'}}
        spreadConfigDict = {'featureKey': 'spread',
                            'featureId': 'spread',
                            'params': {'instr1bid': 'stockTopBidPrice',
                                       'instr1ask': 'stockTopAskPrice',
                                       'instr2bid': 'futureTopBidPrice',
                                       'instr2ask': 'futureTopAskPrice'}}
        feesConfigDict = {'featureKey': 'fees',
                          'featureId': 'total_fees',
                          'params': {'price': 'stockVWAP',
                                     'feesDict': {1: 0.0001, -1: 0.0001, 0: 0},
                                     'spread': 'spread'}}
        profitlossConfigDict = {'featureKey': 'pnl',
                                'featureId': 'pnl',
                                'params': {'price': self.getPriceFeatureKey(),
                                           'fees': 'fees'}}
        capitalConfigDict = {'featureKey': 'capital',
                             'featureId': 'capital',
                             'params': {'price': 'stockVWAP',
                                        'fees': 'fees',
                                        'capitalReqPercent': 0.15}}

        return {INSTRUMENT_TYPE_STOCK: stockFeatureConfigs +
                [fairValuePrediction, sdevDictForExec, scoreDict,
                 spreadConfigDict, feesConfigDict,
                 profitlossConfigDict, capitalConfigDict, enterPriceDict, enterFlagDict]}

    def getMarketFeatureConfigDicts(self):

        scoreDict = {'featureKey': 'score',
                     'featureId': 'prob1_score',
                     'params': {'price': 'basis',
                                'instrument_score_feature': 'score',
                                'benchmark_score_feature': 'benchmark_score'}}
        # TODO
        return []


    def getExecutionSystem(self):
        lots = self.__lotSizes[self.__lotSizes.columns[1]][self.__instrumentIds].astype(float)
        print(lots)
        return BasisExecutionSystem(basisEnter_threshold=.5, basisExit_threshold=0.1,
                                    basisLongLimit=10*lots, basisShortLimit=10*lots,
                                    basisCapitalUsageLimit=0.1, basisLotSize=lots,
                                    basisLimitType='L', basis_thresholdParam='sdev_5_for_exec',
                                    price=self.getPriceFeatureKey(), feeDict=0.0001, feesRatio=1.2,
                                    spreadLimit=0.1)

    def getOrderPlacer(self):
        #return LiveAlphaOrderPlacer(self.__orderPlacerFile, self.__orderConfirmerFile)
        return BacktestingOrderPlacer()

    def getLookbackSize(self):
        return 375*60/FREQUENCY_TRADE_SECS

    def getPriceFeatureKey(self):
        return 'basis'


class PredictionFeature(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        tf = featureParams['lstm']
        return pd.Series(tf.getPrediction(time, updateNum, instrumentManager),
                         index=instrumentManager.getAllInstrumentsByInstrumentId())


class BasisCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        try:
            currentStockBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1bid']).iloc[-1]
            currentStockAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1ask']).iloc[-1]
            currentFutureBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2bid']).iloc[-1]
            currentFutureAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2ask']).iloc[-1]
        except KeyError:
            logError('Bid and Ask Price Feature Key does not exist')

        basis = currentStockAskPrice + currentStockBidPrice - currentFutureAskPrice - currentFutureBidPrice
        return basis / 2.0


class SpreadCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        try:
            currentStockBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1bid']).iloc[-1]
            currentStockAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1ask']).iloc[-1]
            currentFutureBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2bid']).iloc[-1]
            currentFutureAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2ask']).iloc[-1]
        except KeyError:
            logError('Bid and Ask Price Feature Key does not exist')

        currentSpread = currentStockAskPrice - currentStockBidPrice + currentFutureAskPrice - currentFutureBidPrice
        return currentSpread / 4.0


class TotalFeesCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()

        positionData = instrumentLookbackData.getFeatureDf('position')
        feesDict = featureParams['feesDict']
        currentPosition = positionData.iloc[-1]
        previousPosition = 0 if updateNum <= 2 else positionData.iloc[-2]
        changeInPosition = pd.Series(0, index=instrumentManager.getAllInstrumentsByInstrumentId()) if updateNum <= 2 else (currentPosition - previousPosition)
        changeInPosition.fillna(0)
        f = [feesDict[np.sign(x)] for x in changeInPosition]
        fees = np.abs(changeInPosition) * [feesDict[np.sign(x)] for x in changeInPosition]
        if 'price' in featureParams:
            try:
                priceData = instrumentLookbackData.getFeatureDf(featureParams['price'])
                currentPrice = priceData.iloc[-1]
            except KeyError:
                logError('Price Feature Key does not exist')

            fees = fees * currentPrice
        # TODO: Implement
        total = 2 * fees \
           # + (np.abs(changeInPosition) * instrumentLookbackData.getFeatureDf(featureParams['spread']).iloc[-1])
        return total
class EnterPrice(Feature):
    problem1Solver = None

    @classmethod
    def setProblemSolver(cls, problem1Solver):
        Problem1PredictionFeature.problem1Solver = problem1Solver

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        try:
            priceData = instrumentLookbackData.getFeatureDf(featureParams['price'])
            currentPrice = priceData.iloc[-1]
        except KeyError:
            logError('Price Feature Key does not exist')
        # import pdb;pdb.set_trace()
        positionData = instrumentLookbackData.getFeatureDf('position')
        previousPosition = 0 if updateNum <= 2 else positionData.iloc[-2]
        currentPosition = 0*currentPrice if updateNum <= 2 else positionData.iloc[-1]
        changeInPosition = 0 if updateNum <= 2 else positionData.iloc[-1] - positionData.iloc[-2]
        instrumentsDict = instrumentManager.getAllInstrumentsByInstrumentId()
        for instrumentId in instrumentsDict:
            print(instrumentId , instrumentManager.getInstrument(instrumentId).getCurrentPosition())
        avgEnterPrice = 0*currentPrice if updateNum <= 2 else instrumentLookbackData.getFeatureDf(featureKey).iloc[-1]
        avgEnterPrice[currentPosition!=0] = (previousPosition*avgEnterPrice + changeInPosition * currentPrice)/currentPosition
        avgEnterPrice[currentPosition==0] = 0

        return avgEnterPrice


class EnterFlag(Feature):
    problem1Solver = None

    @classmethod
    def setProblemSolver(cls, problem1Solver):
        Problem1PredictionFeature.problem1Solver = problem1Solver

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        positionData = instrumentLookbackData.getFeatureDf('position')
        previousPosition = 0 if updateNum <= 2 else positionData.iloc[-2]
        currentPosition = 0*instrumentLookbackData.getFeatureDf('basis').iloc[-1] if updateNum <= 2 else positionData.iloc[-1]
        changeInPosition = currentPosition - previousPosition
        enterFlag = 0*currentPosition if updateNum <= 2 else instrumentLookbackData.getFeatureDf(featureKey).iloc[-1]
        enterFlag[changeInPosition!=0] = True
        enterFlag[changeInPosition==0] = False

        return enterFlag



if __name__ == "__main__":
    model_path = "clustered_minmax_lstm_model_1_10_clusters.pkl" #"problem1_clustered_params.pkl"
    tf = LSTMTradingFunctions(stocks=STOCKS, model_path=model_path,
                                        startdate=DATE_TO_TRADE, update=False)
    tsParams = LivePairTradingParams(tf)
    tradingSystem = TradingSystem(tsParams)
    try:
        file = open(BOOK_DATA_FILE,'r')
    except IOError:
        file = open(BOOK_DATA_FILE,'w')
    file.close()
    try:
        file = open(INSTRUMENT_UPDATE_PROCESSED_FILE, 'r')
    except IOError:
        file = open(INSTRUMENT_UPDATE_PROCESSED_FILE, 'w')
    file.close()
    try:
        file = open(ORDER_CONFIRMER_FILE, 'r')
    except IOError:
        file = open(ORDER_CONFIRMER_FILE, 'w')
    file.close()

    if IS_LIVE:
        tradingSystem.startTradingLive(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)
    else:
        tradingSystem.startTrading(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)
