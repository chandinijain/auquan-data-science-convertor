# TODO: This is a temporay way so that you can run this anywhere and backtester from auquantoolbox
# can be anywhere.
import sys
sys.path.insert(0, "../auquantoolbox")

from backtester.trading_system_parameters import TradingSystemParameters
from backtester.executionSystem.basis_execution_system import BasisExecutionSystem
from backtester.executionSystem.live_basis_execution_system import LiveBasisExecutionSystem
from backtester.constants import *
from backtester.features.feature import Feature
from backtester.logger import *
from backtester.trading_system import TradingSystem
import numpy as np
import pandas as pd
from live_alpha_order_placer import LiveAlphaOrderPlacer
from alpha_grep_data_source import AlphaGrepDataSource
from live_time_rule import LiveTimeRule
from backtesting_time_rule import BacktestingTimeRule
from datetime import datetime, timedelta
from backtester.timeRule.us_time_rule import USTimeRule
from backtester.orderPlacer.backtesting_order_placer import BacktestingOrderPlacer
import os
import pickle
import requests
import keras
from trading_models import *
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
import types
import tempfile
from dateutil.relativedelta import relativedelta, TH

from LSTM3 import LSTMTradingFunctions


def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

def is_in_expiry_week(date):
    last_thursday = calc_last_thursday(date)
    return (last_thursday - date).days < 7

#Set Display Options
pd.options.display.max_rows = 99
pd.set_option('expand_frame_repr', False)
pd.set_option('display.float_format', lambda x: '%.4f' % x)
prod = pd.read_csv(sys.argv[2], header=None, index_col = 0)
STOCKS = prod.index
#STOCKS = ['AIO', 'AUZ', 'BWU', 'CBT', 'CHV', 'CUN', 'DVV', 'EGV', 'IES', 'IFL']

DATE_TO_TRADE = sys.argv[1]
BOOK_DATA_FILE = '../repo.auquan/bookData-' + DATE_TO_TRADE

ORDER_PLACER_FILE = '../repo.auquan/orderPlacer.txt'
ORDER_CONFIRMER_FILE = '../repo.auquan/orderConfirmer-' + DATE_TO_TRADE
INSTRUMENT_UPDATE_PROCESSED_FILE = "../repo.auquan/instrumentUpdateProcessed.txt"
FREQUENCY_TRADE_SECS = 30

INITIALIZER = None #'savedData20180516'

IS_LIVE = False
FROM_SERVER = False

LOT_SIZE_FILE = '../repo.auquan/fo_mktlots_Sep2017.csv'
THRESHOLD_FILE = '../live_order_placer/rmse_clustered_minmax_lstm_model_1_10_clusters.csv'

EXPIRY = calc_last_thursday(datetime.strptime(DATE_TO_TRADE, '%Y%m%d'))

class LivePairTradingParams(TradingSystemParameters):

    def __init__(self, tradingFunctions):
        self.__tradingFunctions = tradingFunctions
        self.__bookDataFile = BOOK_DATA_FILE
        self.__orderPlacerFile = ORDER_PLACER_FILE
        self.__orderConfirmerFile = ORDER_CONFIRMER_FILE
        self.__instrumentUpdateProcessedFile = INSTRUMENT_UPDATE_PROCESSED_FILE
        super(LivePairTradingParams, self).__init__()
        if IS_LIVE:
            self.__startTime = datetime.now()
            self.__endTime = self.__startTime + timedelta(seconds=900)
        else:
            self.__startTime = datetime.strptime(DATE_TO_TRADE, '%Y%m%d') + timedelta(hours=9, minutes=15)
            self.__endTime = datetime.strptime(DATE_TO_TRADE, '%Y%m%d') + timedelta(hours=15, minutes=29)
        self.__instrumentIds = self.__tradingFunctions.getSymbolsToTrade()
        self.__lotSizes = pd.read_csv(LOT_SIZE_FILE,index_col=1)
        self.__lotSizes.set_index(self.__lotSizes.index.str.strip(), inplace=True)

    def getStartingCapital(self):
        return 200000000

    def getDataParser(self):
        # Change last parameter to false, if you want to read from an already written file as opposed to tailing from a file.
        return AlphaGrepDataSource(self.__instrumentIds, self.__bookDataFile, self.__instrumentUpdateProcessedFile, FROM_SERVER , IS_LIVE)

    def getTimeRuleForUpdates(self):
        # TODO: Better implementation of this and end time here matching actual trading end time
        # TODO: Might have to take care of trading days too in this later on
        return LiveTimeRule(self.__startTime, FREQUENCY_TRADE_SECS, self.__endTime)

    def getBenchmark(self):
        return None


    def getCustomFeatures(self):
        return {'diff_ma': DiffInMovingAverage, 'time_to_expiry' : TimeToExpiry, 'ratio_ma' : RatioFutureStock,
                'prediction_feature': PredictionFeature,
                'prediction_error' : PredictionErrorFeature,
                'enter_price':EnterPrice,
               'enter_flag':EnterFlag,
                'basis_calculator': BasisCalculator,
                'spread': SpreadCalculator,
                'total_fees': TotalFeesCalculator}

    def getInstrumentFeatureConfigDicts(self):
        stockFeatureConfigs = self.__tradingFunctions.getInstrumentFeatureConfigDicts()

        fairValuePrediction = {'featureKey': 'prediction',
                               'featureId': 'prediction_feature',
                               'params': {'function': self.__tradingFunctions}}
        enterPriceDict = {'featureKey': 'enter_price',
                     'featureId': 'enter_price',
                     'params': {'price': self.getPriceFeatureKey()}}
        enterFlagDict = {'featureKey': 'enter_flag',
                     'featureId': 'enter_flag',
                     'params': {}}
        scoreDict = {'featureKey': 'score',
                     'featureId': 'prob1_score',
                     'params': {'predictionKey': 'prediction',
                                'price': 'basis'}}
        sdevDictForExec =  {'featureKey': 'basis_sdev',
                           'featureId': 'moving_sdev',
                           'params': {'period': 60*60/FREQUENCY_TRADE_SECS,
                                      'featureName': 'basis'}}
        predError = {'featureKey': 'execution_threshold',
                           'featureId': 'prediction_error',
                           'params': {'fileName': THRESHOLD_FILE}}
        spreadConfigDict = {'featureKey': 'spread',
                            'featureId': 'spread',
                            'params': {'instr1bid': 'stockTopBidPrice',
                                       'instr1ask': 'stockTopAskPrice',
                                       'instr2bid': 'futureTopBidPrice',
                                       'instr2ask': 'futureTopAskPrice'}}
        feesConfigDict = {'featureKey': 'fees',
                          'featureId': 'total_fees',
                          'params': {'price': 'stockVWAP',
                                     'feesDict': {1: 0.00012, -1: 0.00012, 0: 0},
                                     'spread': 'spread'}}
        profitlossConfigDict = {'featureKey': 'pnl',
                                'featureId': 'pnl',
                                'params': {'price': self.getPriceFeatureKey(),
                                           'fees': 'fees'}}
        capitalConfigDict = {'featureKey': 'capital',
                             'featureId': 'capital',
                             'params': {'price': 'stockVWAP',
                                        'fees': 'fees',
                                        'capitalReqPercent': 0.15}}

        return {INSTRUMENT_TYPE_STOCK: stockFeatureConfigs +
                [fairValuePrediction, sdevDictForExec, predError, scoreDict,
                 spreadConfigDict, feesConfigDict,
                 profitlossConfigDict, capitalConfigDict, enterPriceDict, enterFlagDict]}

    def getMarketFeatureConfigDicts(self):

        scoreDict = {'featureKey': 'score',
                     'featureId': 'prob1_score',
                     'params': {'price': 'basis',
                                'instrument_score_feature': 'score',
                                'benchmark_score_feature': 'benchmark_score'}}
        # TODO
        return []


    def getExecutionSystem(self):
        lots = self.__lotSizes[self.__lotSizes.columns[1]][self.__instrumentIds].astype(float)
        print(lots)
        threshold = pd.read_csv(THRESHOLD_FILE, index_col=1)
        print(threshold['rmse'][self.__instrumentIds].astype(float))
        return LiveBasisExecutionSystem(basisEnter_threshold=.5, basisExit_threshold=0.1,
                                    basisLongLimit=10*lots, basisShortLimit=10*lots,
                                    basisCapitalUsageLimit=0.1, basisLotSize=lots,
                                    basisLimitType='L', basis_thresholdParam=threshold['rmse'][self.__instrumentIds].astype(float),
                                    price=self.getPriceFeatureKey(), feeDict=0.0001, feesRatio=1.25,
                                    spreadLimit=0.075)

    def getOrderPlacer(self):
        if FROM_SERVER:
            return LiveAlphaOrderPlacer(self.__orderPlacerFile, self.__orderConfirmerFile)
        else:
            return BacktestingOrderPlacer()

    def getLookbackSize(self):
        return 360*60/FREQUENCY_TRADE_SECS

    def getPriceFeatureKey(self):
        return 'basis'

    def getInitializer(self):
        return INITIALIZER

class DiffInMovingAverage(Feature):
    """docstring for DiffInMovingAverage."""
    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        # A holder for the all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
        featureDf = lookbackInstrumentFeatures.getFeatureDf(featureParams['featureName'])
        moving_avgs = featureDf.rolling(window=featureParams['period'], min_periods=1).mean()
        # moving_avg = featureDf[-featureParams['period']:].mean()

        if len(featureDf.index) < featureParams['period']:
            instrumentDict = instrumentManager.getAllInstrumentsByInstrumentId()
            zeroSeries = pd.Series([0] * len(instrumentDict), index=instrumentDict.keys())
            return zeroSeries
        return moving_avgs.iloc[-1] - moving_avgs.iloc[-featureParams['period']]


class RatioFutureStock(Feature):
    """docstring for DiffInMovingAverage."""
    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        # A holder for the all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
        featureDf1 = lookbackInstrumentFeatures.getFeatureDf(featureParams['featureName1'])
        featureDf2 = lookbackInstrumentFeatures.getFeatureDf(featureParams['featureName2'])
        moving_avg1 = featureDf1[-featureParams['period']:].mean()
        moving_avg2 = featureDf2[-featureParams['period']:].mean()

        ratio =  moving_avg1 / moving_avg2
        ratio[ratio == np.Inf] = 0
        return ratio


class TimeToExpiry(Feature):
    """docstring for TimeToExpiry."""
    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        """Assuming the "date" is not equal to "last_thursday" """
        if 'expiry' in featureParams:
          last_thursday = featureParams['expiry']
        else:
          end_of_month = time + relativedelta(day=31)
          last_thursday = end_of_month + relativedelta(weekday=TH(-1))
          if last_thursday < time:
            end_of_next_month = time + relativedelta(months=1) + relativedelta(day=31)
            last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))

        timediff = last_thursday.replace(hour=15, minute=30) - time
        days, seconds = timediff.days, timediff.seconds

        return days*24 + seconds // 3600


class PredictionFeature(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        tf = featureParams['function']
        prediction = tf.getPrediction(time, updateNum, instrumentManager)

        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
        d = pd.DataFrame(index=instrumentManager.getAllInstrumentsByInstrumentId(),
                         columns=['basis','predictions','position','spread'])
        if len(lookbackInstrumentFeatures.getFeatureDf('basis')) > 0:
          d['basis'] = lookbackInstrumentFeatures.getFeatureDf('basis').iloc[-1]
        if len(lookbackInstrumentFeatures.getFeatureDf('position')) > 0:
          d['position'] = lookbackInstrumentFeatures.getFeatureDf('position').iloc[-1]
          d['enter_price'] = lookbackInstrumentFeatures.getFeatureDf('enter_price').iloc[-1]
          d['spread'] = lookbackInstrumentFeatures.getFeatureDf('spread').iloc[-1]
          d['sdev'] = lookbackInstrumentFeatures.getFeatureDf('basis_sdev').iloc[-1]
          d['threshold'] = lookbackInstrumentFeatures.getFeatureDf('execution_threshold').iloc[-1]
        instrumentsDict = instrumentManager.getAllInstrumentsByInstrumentId()
        currentPosition = pd.Series([instrumentManager.getInstrument(x).getCurrentPosition() for x in instrumentsDict], index= instrumentsDict)
        d['position'] = currentPosition
        d['predictions'] = prediction
        print(d)

        return prediction

class PredictionErrorFeature(Feature):
      @classmethod
      def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        if len(instrumentLookbackData.getFeatureDf(featureKey)) <= 1:
          threshold = pd.read_csv(featureParams['fileName'], index_col=1)
          idx = instrumentManager.getAllInstrumentsByInstrumentId()
          return threshold['rmse'][idx].astype(float)
        rmse = instrumentLookbackData.getFeatureDf(featureKey).iloc[-1]
        #error = (instrumentLookbackData.getFeatureDf('prediction').iloc[-5] - instrumentLookbackData.getFeatureDf('FairValue').iloc[-1])**2
        # TODO: Update rmse realtime
        return rmse


class BasisCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        try:
            currentStockBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1bid']).iloc[-1]
            currentStockAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1ask']).iloc[-1]
            currentFutureBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2bid']).iloc[-1]
            currentFutureAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2ask']).iloc[-1]
        except KeyError:
            logError('Bid and Ask Price Feature Key does not exist')

        basis = currentStockAskPrice + currentStockBidPrice - currentFutureAskPrice - currentFutureBidPrice
        return basis / 2.0


class SpreadCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        return 0
        #instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        #try:
        #    currentStockBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1bid']).iloc[-1]
        #    currentStockAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1ask']).iloc[-1]
        #    currentFutureBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2bid']).iloc[-1]
        #    currentFutureAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2ask']).iloc[-1]
        #except KeyError:
        #    logError('Bid and Ask Price Feature Key does not exist')

        #currentSpread = currentStockAskPrice - currentStockBidPrice + currentFutureAskPrice - currentFutureBidPrice
        #return np.maximum(0.075, currentSpread / 4.0)


class TotalFeesCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()

        positionData = instrumentLookbackData.getFeatureDf('position')
        feesDict = featureParams['feesDict']
        currentPosition = positionData.iloc[-1]
        previousPosition = 0 if updateNum <= 2 else positionData.iloc[-2]
        changeInPosition = pd.Series(0, index=instrumentManager.getAllInstrumentsByInstrumentId()) if updateNum <= 2 else (currentPosition - previousPosition)
        changeInPosition.fillna(0)
        f = [feesDict[np.sign(x)] for x in changeInPosition]
        fees = np.abs(changeInPosition) * [feesDict[np.sign(x)] for x in changeInPosition]
        if 'price' in featureParams:
            try:
                priceData = instrumentLookbackData.getFeatureDf(featureParams['price'])
                currentPrice = priceData.iloc[-1]
            except KeyError:
                logError('Price Feature Key does not exist')

            fees = fees * currentPrice
        # TODO: Implement
        total = 2 * fees \
           # + (np.abs(changeInPosition) * instrumentLookbackData.getFeatureDf(featureParams['spread']).iloc[-1])
        return total
class EnterPrice(Feature):
    problem1Solver = None

    @classmethod
    def setProblemSolver(cls, problem1Solver):
        Problem1PredictionFeature.problem1Solver = problem1Solver

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        # import pdb;pdb.set_trace()
        instrumentsDict = instrumentManager.getAllInstrumentsByInstrumentId()
        positionData = instrumentLookbackData.getFeatureDf('position')
        previousPosition = pd.Series(0, index= instrumentsDict) if updateNum <= 2 else positionData.iloc[-1]
        currentPosition = pd.Series([instrumentManager.getInstrument(x).getCurrentPosition() for x in instrumentsDict], index= instrumentsDict)
        currentPrice = pd.Series([instrumentManager.getInstrument(x).getLastTradePrice() for x in instrumentsDict], index = instrumentsDict)
        changeInPosition = 0 if updateNum <= 2 else (currentPosition - previousPosition)
        #for instrumentId in instrumentsDict:
        #    print(instrumentId , instrumentManager.getInstrument(instrumentId).getCurrentPosition())
        avgEnterPrice = 0*currentPrice if updateNum <= 2 else instrumentLookbackData.getFeatureDf(featureKey).iloc[-1]
        avgEnterPrice[(currentPosition!=0) & (currentPosition.abs()- previousPosition.abs()>0)] = (previousPosition*avgEnterPrice + changeInPosition * currentPrice)/currentPosition
        avgEnterPrice[currentPosition==0] = 0

        return avgEnterPrice


class EnterFlag(Feature):
    problem1Solver = None

    @classmethod
    def setProblemSolver(cls, problem1Solver):
        Problem1PredictionFeature.problem1Solver = problem1Solver

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        instrumentsDict = instrumentManager.getAllInstrumentsByInstrumentId()
        positionData = instrumentLookbackData.getFeatureDf('position')
        previousPosition = 0 if updateNum <= 2 else positionData.iloc[-1]
        currentPosition = pd.Series([instrumentManager.getInstrument(x).getCurrentPosition() for x in instrumentsDict], index= instrumentsDict)
        changeInPosition = currentPosition - previousPosition
        enterFlag = 0*currentPosition if updateNum <= 2 else instrumentLookbackData.getFeatureDf(featureKey).iloc[-1]
        enterFlag[changeInPosition!=0] = True
        enterFlag[changeInPosition==0] = False

        return enterFlag



if __name__ == "__main__":
    model_path = ['../live_order_placer/models/normal_week_model_1.pkl', '../live_order_placer/models/expiry_week_model_1.pkl'] #"problem1_clustered_params.pkl"
    make_keras_picklable()
    trading_model = {'normal':None, 'expiry':None}
    print('Loading Keras Models')
    if is_in_expiry_week(datetime.strptime(DATE_TO_TRADE, '%Y%m%d')):
      with open(model_path[1], "rb") as f:
        trading_model['expiry'] = pickle.load(f)
        print('EXPIRY WEEK')
        print(trading_model['expiry'])
    else:
      with open(model_path[0], "rb") as f:
        trading_model['normal'] = pickle.load(f)
        print(trading_model['normal'])
    print('Keras Loaded')

    tf = LSTMTradingFunctions(stocks=STOCKS, model = trading_model, model_path='',
                                        startdate=DATE_TO_TRADE, period=60*60/FREQUENCY_TRADE_SECS, expiry=EXPIRY, update=True)
    print('Trading functions loaded')
    tsParams = LivePairTradingParams(tf)
    print('TS params loaded')
    tradingSystem = TradingSystem(tsParams)
    print('Trading System loaded')
    print('start trading now')
    if IS_LIVE:
        tradingSystem.startTradingLive(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)
    else:
        tradingSystem.startTrading(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)

    print('Done')
    #tf.evaluateModel(outf="rmse_5_sept.csv")
