from backtester.features.feature import Feature
from backtester.trading_system import TradingSystem
from backtester.sample_scripts.fair_value_params import FairValueTradingParams
from backtester.version import updateCheck

#Machine Learning
import numpy as np
import pandas as pd
from sklearn.ensemble import ExtraTreesRegressor


from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
##other
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)


class GBTradingFunctions():


    def __init__(self, stocks, frequency, startdate):
        self.stocks = stocks
        self.count = 0
        self.today = startdate
        self.frequency = frequency
        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)

    def getSymbolsToTrade(self):
        print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]


    def getCustomFeatures(self):
        return {'percentB': MyCustomFeature}

    def getInstrumentFeatureConfigDicts(self):

        ma1 = {'featureKey': 'ma_5',
                   'featureId': 'moving_average',
                   'params': {'period': 5*60/self.frequency,
                              'featureName': 'basis'}}
        ma2 = {'featureKey': 'ma_10',
                   'featureId': 'moving_average',
                   'params': {'period': 10*60/self.frequency,
                              'featureName': 'stockTopAskPrice'}}
        expma = {'featureKey': 'expma20',
                 'featureId': 'exponential_moving_average',
                 'params': {'period': 20*60/self.frequency,
                              'featureName': 'basis'}}
        sdevDict = {'featureKey': 'sdev_5',
                    'featureId': 'moving_sdev',
                    'params': {'period': 5*60/self.frequency,
                               'featureName': 'basis'}}
        diff = {'featureKey': 'price_change',
                'featureId': 'difference',
                'params': {'period': 5*60/self.frequency,
                           'featureName': 'basis'}}
        macd = {'featureKey': 'macd',
                'featureId': 'macd',
                'params': {'period1': 5*60/self.frequency,
                           'period2': 10*60/self.frequency,
                           'featureName': 'basis'}}
        bbandlower = {'featureKey': 'lowerBB',
                          'featureId': 'bollinger_bands_lower',
                          'params': {'period': 20*60/self.frequency,
                                     'featureName': 'stockVWAP'}}
        bbandupper = {'featureKey': 'upperBB',
                          'featureId': 'bollinger_bands_upper',
                          'params': {'period': 20*60/self.frequency,
                                     'featureName': 'stockVWAP'}}
        bbandlower1 = {'featureKey': 'lowerBBbasis',
                          'featureId': 'bollinger_bands_lower',
                          'params': {'period': 20*60/self.frequency,
                                     'featureName': 'basis'}}
        bbandupper1 = {'featureKey': 'upperBBbasis',
                          'featureId': 'bollinger_bands_upper',
                          'params': {'period': 20*60/self.frequency,
                                     'featureName': 'basis'}}
        rsi = {'featureKey': 'rsi',
               'featureId': 'rsi',
               'params': {'period': 5*60/self.frequency,
                          'featureName': 'basis'}}
        pcntB = {'featureKey': 'percentB',
                 'featureId': 'percentB'}


        return [ma1, ma2, expma, sdevDict, diff, macd,
                bbandlower, bbandupper, rsi, bbandlower1, bbandupper1]


    def getPrediction(self, time, updateNum, instrumentManager):
        # holder for all the instrument features
        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
        features = ['basis', 'stockVWAP', 'futureVWAP', #'benchmark_score',
        			'exponential_moving_average','ma_10','ma_5', 'sdev_5',
        			'price_change', 'macd', 'lowerBB', 'upperBB', 'rsi' ]

        basis = lookbackInstrumentFeatures.getFeatureDf('basis')
        stockVWAP = lookbackInstrumentFeatures.getFeatureDf('stockVWAP')
        futureVWAP = lookbackInstrumentFeatures.getFeatureDf('futureVWAP')
        ma_5 = lookbackInstrumentFeatures.getFeatureDf('ma_5')
        ma_10 = lookbackInstrumentFeatures.getFeatureDf('ma_10')
        expma20 = lookbackInstrumentFeatures.getFeatureDf('expma20')
        sdev_5 =lookbackInstrumentFeatures.getFeatureDf('sdev_5')
        price_change =lookbackInstrumentFeatures.getFeatureDf('price_change')
        macd =lookbackInstrumentFeatures.getFeatureDf('macd')
        lowerBB =lookbackInstrumentFeatures.getFeatureDf('lowerBB')
        upperBB =lookbackInstrumentFeatures.getFeatureDf('upperBB')
        rsi =lookbackInstrumentFeatures.getFeatureDf('rsi')
        lowerBB1 =lookbackInstrumentFeatures.getFeatureDf('lowerBBbasis')
        upperBB1 =lookbackInstrumentFeatures.getFeatureDf('upperBBbasis')
        #percentB =lookbackInstrumentFeatures.getFeatureDf('percentB')

        stocks = list(ma_5.columns)


        ma5 = ma_5.iloc[-1]
        percentB = (basis - lowerBB1)/(lowerBB1 - upperBB1)

        if len(ma_5.index) > 5*60/self.frequency:
            out = pd.Series()
            for i in stocks:
                X=pd.concat([basis[i],stockVWAP[i], futureVWAP[i], #benchmark_score[i],
                             ma_5[i], ma_10[i], expma20[i], sdev_5[i], price_change[i],
                             macd[i], lowerBB[i], upperBB[i], rsi[i], percentB[i]],
                            axis=1, keys=features)
                from sklearn.preprocessing import Imputer
                X = Imputer().fit_transform(X)

                y = ma_5[i]

                #making testing training
                X_train = X[:-(5*60/self.frequency)]
                y_train = y[(5*60/self.frequency):]
                X_test = X[-1:]

                model = ExtraTreesRegressor()

                model.fit(X_train,y_train.values.ravel())
                p = model.predict(X_test)
                out = out.append(pd.Series([float(p)], index=[i]))
        else:
            out = ma5

        d = pd.DataFrame(index=instrumentManager.getAllInstrumentsByInstrumentId(),
                         columns=['basis','predictions','position','spread'])
        if len(lookbackInstrumentFeatures.getFeatureDf('basis')) > 0:
          d['basis'] = lookbackInstrumentFeatures.getFeatureDf('basis').iloc[-1]
        if len(lookbackInstrumentFeatures.getFeatureDf('position')) > 0:
          d['position'] = lookbackInstrumentFeatures.getFeatureDf('position').iloc[-1]
          d['enter_price'] = lookbackInstrumentFeatures.getFeatureDf('enter_price').iloc[-1]
          d['spread'] = lookbackInstrumentFeatures.getFeatureDf('spread').iloc[-1]
          d['sdev'] = lookbackInstrumentFeatures.getFeatureDf('execution_threshold').iloc[-1]
        instrumentsDict = instrumentManager.getAllInstrumentsByInstrumentId()
        currentPosition = pd.Series([instrumentManager.getInstrument(x).getCurrentPosition() for x in instrumentsDict], index= instrumentsDict)
        d['position'] = currentPosition
        d['predictions'] = out
        print(d)

        return out


class MyCustomFeature(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):

        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        lookbackInstrumentBasis = lookbackInstrumentFeatures.getFeatureDf('basis')
        last = lookbackInstrumentBasis.iloc[-1]
        avg = lookbackInstrumentBasis[-20*60/self.frequency:].mean()
        sdev = lookbackInstrumentBasis[-20*60/self.frequency:].std()

        lbb = avg - sdev
        ubb = avg + sdev

        return (last - lbb) / (ubb - lbb)

