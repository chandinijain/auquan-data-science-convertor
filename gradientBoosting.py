from backtester.features.feature import Feature
from backtester.trading_system import TradingSystem
from backtester.sample_scripts.fair_value_params import FairValueTradingParams
from backtester.version import updateCheck
import pandas as pd
import numpy as np
from sklearn import ensemble

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)


class GBTradingFunctions():


    def __init__(self, stocks, frequency, startdate):
        self.stocks = stocks
        self.count = 0
        self.today = startdate
        self.frequency = frequency
        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)
        self.model={}
        for i in self.stocks:
          self.model[self.unscrambler.loc[i, 'Name']] = ensemble.GradientBoostingRegressor()

    def getSymbolsToTrade(self):
        print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]

    def getInstrumentFeatureConfigDicts(self):
        ma1Dict = {'featureKey': 'ma_5',
                   'featureId': 'moving_average',
                   'params': {'period': 5*60/self.frequency,
                              'featureName': 'basis'}}
        expma = {'featureKey': 'exponential_moving_average',
                 'featureId': 'exponential_moving_average',
                 'params': {'period': 20*60/self.frequency,
                              'featureName': 'basis'}}
        sdevDict = {'featureKey': 'sdev_5',
                    'featureId': 'moving_sdev',
                    'params': {'period': 5*60/self.frequency,
                               'featureName': 'basis'}}
        # customFeatureDict = {'featureKey': 'custom_inst_feature',
        #                      'featureId': 'my_custom_feature',
        #                      'params': {'param1': 'value1'}}
        rsiDict = {'featureKey': 'rsi',
                    'featureId': 'rsi',
                    'params': {'period': 20*60/self.frequency,
                               'featureName': 'stockVWAP'}}
        rsiDict2 = {'featureKey': 'rsi2',
                    'featureId': 'rsi',
                    'params': {'period': 20*60/self.frequency,
                               'featureName': 'futureVWAP'}}
        macdDict = {'featureKey': 'macd',
                    'featureId': 'macd',
                    'params': {'period1': 15*60/self.frequency,
                                'period2': 50*60/self.frequency,
                               'featureName': 'stockVWAP'}}
        macdDict2 = {'featureKey': 'macd2',
                    'featureId': 'macd',
                    'params': {'period1': 15*60/self.frequency,
                                'period2': 50*60/self.frequency,
                               'featureName': 'futureVWAP'}}

        return [ma1Dict, sdevDict, expma,rsiDict,rsiDict2,macdDict,macdDict2]

    def getPrediction(self, time, updateNum, instrumentManager):

        # holder for all the instrument features
        lif = instrumentManager.getLookbackInstrumentFeatures()
        emv = lif.getFeatureDf('exponential_moving_average')
        std = lif.getFeatureDf('sdev_5')
        pr = lif.getFeatureDf('ma_5')
        bd = lif.getFeatureDf('basis')
        res =  pd.Series(0,index=instrumentManager.getAllInstrumentsByInstrumentId())
        emv = emv.tail(65*60/self.frequency)
        std = std.tail(65*60/self.frequency)
        pr = pr.tail(65*60/self.frequency)
        bd = bd.tail(65*60/self.frequency)
        sv = lif.getFeatureDf('stockVWAP').tail(65*60/self.frequency)
        fv = lif.getFeatureDf('futureVWAP').tail(65*60/self.frequency)
        sbp = lif.getFeatureDf('stockTopBidPrice').tail(65*60/self.frequency)
        sap = lif.getFeatureDf('stockTopAskPrice').tail(65*60/self.frequency)
        fbp = lif.getFeatureDf('futureTopBidPrice').tail(65*60/self.frequency)
        fap = lif.getFeatureDf('futureTopAskPrice').tail(65*60/self.frequency)
        md = lif.getFeatureDf('macd').tail(65*60/self.frequency)
        md2 = lif.getFeatureDf('macd2').tail(65*60/self.frequency)
        rsi = lif.getFeatureDf('rsi').tail(65*60/self.frequency)
        rsi2 = lif.getFeatureDf('rsi2').tail(65*60/self.frequency)
        for i in res.index:
            #print(emv.index[-100:])
            #ind=list(emv.index[-100:])
            #ind=[str(i) for i in ind]
            #ind=(pd.Series(ind))
            #print(ind)
            #print(emv)
            sprice=(sbp[i]+sap[i])/2
            fprice=(fbp[i]+fap[i])/2
            e1=emv[i].values
            st1=std[i].values
            #print(pd.Series(e1.values))
            df=pd.DataFrame(pd.Series(e1),columns=['emv'])
            #print(df)
            df['st'] = pd.Series(st1)
            df['s1']=(sv[i]-sprice).values
            df['f1']=(fv[i]-fprice).values
            df['macd']=md[i].values
            df['macd2']=md2[i].values
            df['rsi']=rsi[i].values
            df['rsi2']=rsi2[i].values
            df['basis']=bd[i].values
            df['pred']=pd.Series(pr[i].values)
            df=df.fillna(method="ffill")
            df=df.fillna(method="bfill")
            df=df.fillna(0)
            #print(df)
            x=0
            xtest = [emv[i].values[-1], std[i].values[-1], (sv[i]-sprice).values[-1], (fv[i]-fprice).values[-1],
            		md[i].values[-1], md2[i].values[-1], rsi[i].values[-1], rsi2[i].values[-1], bd[i].values[-1]]
            if df.shape[0]>5*60/self.frequency:
                if self.count%(5*60/self.frequency) == 0:
                  self.model[i].fit(df.drop('pred',1)[:-(5*60/self.frequency)],df['pred'][(5*60/self.frequency):])
                x=self.model[i].predict(df.drop('pred',1)[-1:])[0]
            else:
                x=e1[0]
            res[i] = x
        self.count +=1
        return res


