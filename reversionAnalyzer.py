import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta as td
import os
import numpy as np

def frange(start, stop, step):
	i = start
	r = []
	while i < stop:
		r += [i]
		i += step

	return r
  

# runLogFolder = 'runLog_20180227_172025'


# data = pd.read_csv('runLogs/'+runLogFolder+'/'+'marketFeatures.csv',index_col=0, parse_dates=True)
# predictions = data['predictionString'].apply(lambda x: x.split(', '))
# df = pd.DataFrame(predictions.values.tolist(), index=data.index).astype(float)
# pnl = data['pnlString'].apply(lambda x: x.split(', '))
# pnldf = pd.DataFrame(pnl.values.tolist(), index=data.index).astype(float)
# position = data['positionString'].apply(lambda x: x.split(', '))
# positiondf = pd.DataFrame(position.values.tolist(), index=data.index).astype(float)
dataset = 'OSData2P1'
unscrambler = pd.read_csv('unscrambler.csv',index_col=0) 
stocks=['AGW','AIO','AUZ','BSN','BWU','CBT','CHV','CSR','CUN',
		'CYD','DCO','DFZ','DVV','DYE','EGV','FCY','FFA','FFS',
		'FKI','FRE','FUR','GFQ','GGK','GYJ','GYV','HHK','IES',
		'ILW','IMC','IUE','IUQ','IYU','JSG','JYW','KAA','KKG',
		'KMV','KRZ','LDU','LKB','LPQ','MAS','MQK','MUF','NDG',
		'NSL','NYO','OED','OGU','OMP','PFK','PLX','PMS','PQS',
		'PUO','QRK','SCN','SVG','TGI','TWK','UBF','UWC','UWD',
		'VML','VND','VSL','VTN','WAG','WTJ','XAD','XCS','XFD',
		'XIT','XPV','XYR','XZR','YGC','YHW','YUZ','ZEW','ZLX']

print(unscrambler)
df={}
for i in range(len(stocks)):
	print(unscrambler.loc[stocks[i],'Name'])
	data = pd.read_csv('historicalData/'+dataset+'/'+unscrambler.loc[stocks[i],'Name']+'.csv',index_col=0, parse_dates=True)
	data=data.iloc[:22380]
	basis = data['basis']
	# for period in range(10,130,10):
	# 	diff = (np.sign(basis - basis.shift(period))==np.sign(basis.shift(period) - basis.shift(2*period)))
	# 	sign = 0*data['basis']
	# 	sign[diff==True] = 1
	# 	sign[diff==False] = 0
	# 	print(period, np.mean(np.abs(sign)), np.mean(np.abs(basis - basis.rolling(2*period).mean())))
	# df.set_index(data.index, inplace=True)
	# pnldf.set_index(data.index, inplace=True)
	# positiondf.set_index(data.index, inplace=True)
	print('Plotting %s'%unscrambler.loc[stocks[i],'Name'])
	period = 375
	df[i]=basis.rolling(2*period).mean().shift(-period)
	diff = df[i]-data.basis
	z = (diff/(100*data.basis.rolling(375).std()/375.0))[500:]
	z = z.replace([np.inf, -np.inf], np.nan).dropna()
	print(z[z < - 1.5 ].count()/float(len(z)), z[z > 1.5 ].count()/float(len(z)))
	print(z[z < - 1 ].count()/float(len(z)), z[z > 1 ].count()/float(len(z)))
	print(np.mean(data.basis.rolling(375).std()))
	# print(pnldf[i].iloc[-1])
	plt.plot(df[i].iloc[500:-5],'.')
	# plt.plot(data.FairValue)
	# plt.plot(data.basis.rolling(5).mean().shift(-5).iloc[500:-5])
	plt.plot(data.basis.iloc[500:-5])
	plt.show()
	plt.plot(diff[500:])
	plt.plot(0.45 + diff.rolling(375).mean()[500:])#(100*data.basis.rolling(375).std()/375.0)[500:] 
	plt.plot(-0.45 + (df[i]-data.basis).rolling(375).mean()[500:])
	plt.show()

	# fig, ax1 = plt.subplots()
	# ax1.plot(pnldf[i], 'b.')
	# ax2 = ax1.twinx()
	# ax2.plot(positiondf[i], 'r.')
	# plt.show()
	
	print(z.hist(bins = frange(np.min(z), np.max(z), (np.max(z)-np.min(z))/20.0), align = 'mid', density=True, cumulative=True))
	plt.hist(z, bins = frange(np.min(z), np.max(z), (np.max(z)-np.min(z))/20.0), align = 'mid', density=True, cumulative=False)
	plt.show()


	

