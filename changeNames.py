import pandas as pd
import numpy as np
import os
import datetime as dt
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, TH

def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

def Price_Momentum(data, period):
        """
        1-Month Price Momentum:
        1-month closing price rate of change.
        https://www.pnc.com/content/dam/pnc-com/pdf/personal/wealth-investments/WhitePapers/FactorAnalysisFeb2014.pdf # NOQA
        Notes:
        High value suggests momentum (shorter term)
        Equivalent to analysis of returns (1-month window)
        """
        return data - data.shift(period)


def Price_Oscillator(data, p1, p2):
        """
        4/52-Week Price Oscillator:
        Average close prices over 4-weeks divided by average close
        prices over 52-weeks all less 1.
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests momentum
        """
        out = data.rolling(p1).mean() / data.rolling(p2).mean() - 1.
        return out

def Trendline(df,p):
		"""
		52-Week Trendline:
		Slope of the linear regression across a 1 year lookback window.
		https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
		Notes:
		High value suggests momentum
		Calculated using the MLE of the slope of the regression
		"""

		# using MLE for speed

		# prepare X matrix (x_is - x_bar)
		import pdb;pdb.set_trace()
		data=df.copy()
		X = np.array(range(p))/75.0
		X_bar = np.nanmean(X)
		X_vector = X - X_bar
		X_matrix = np.tile(X_vector, (len(data.T), 1)).T

		# prepare Y matrix (y_is - y_bar)
		Y_bar = np.nanmean(data, axis=0)
		Y_bars = np.tile(Y_bar, (p, 1))
		Y_matrix = data.values.reshape(-1) - Y_bars

		# prepare variance of X
		X_var = np.nanvar(X)

		# multiply X matrix an Y matrix and sum (dot product)
		# then divide by variance of X
		# this gives the MLE of Beta
		out = (np.sum((X_matrix * Y_matrix), axis=0) / X_var)/ (p)
		window = p
		a = np.array([np.nan] * len(df))
		b = [np.nan] * len(df)  # If betas required.
		y_ = df.values
		x_ = X_matrix
		for n in range(window, len(df)):
		    y = y_[(n - window):n]
		    X = x_[(n - window):n]
		    # betas = Inverse(X'.X).X'.y
		    betas = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(y)
		    y_hat = betas.dot(x_[n, :])
		    a[n] = y_hat
		    b[n] = betas.tolist()  # If betas required.

		return out

def Mean_Reversion(data,p1,p2):
        """
        1-Month Mean Reversion:
        1-month return less 12-month average of monthly return, all over
        standard deviation of 12-month average of monthly returns.
        https://www.pnc.com/content/dam/pnc-com/pdf/personal/wealth-investments/WhitePapers/FactorAnalysisFeb2014.pdf # NOQA
        Notes:
        High value suggests momentum (short term)
        Equivalent to analysis of returns (12-month window)

        """

        rets = data/data.shift(p1) - 1

        out = (rets - rets.rolling(p2).mean()) /rets.rolling(p2).std()

        return out

def Mean_Reversion_Rolling(data,p1,p2):
        """
        1-Month Mean Reversion:
        1-month return less 12-month average of monthly return, all over
        standard deviation of 12-month average of monthly returns.
        https://www.pnc.com/content/dam/pnc-com/pdf/personal/wealth-investments/WhitePapers/FactorAnalysisFeb2014.pdf # NOQA
        Notes:
        High value suggests momentum (short term)
        Equivalent to analysis of returns (12-month window)

        """
        rets = data/data.shift(p1) - 1

        mean = rets.groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).mean().reset_index(0,drop=True).sort_index()
        sdev = rets.groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).std().reset_index(0,drop=True).sort_index()
        out = (rets - mean) /sdev
        # import pdb;pdb.set_trace()
        out.fillna(method='pad', inplace=True)

        return out

    # 10 Day MACD signal line
def MACD_Signal(data,p1,p2,p3):
        """
        10 Day Moving Average Convergence/Divergence (MACD) signal line:
        10 day Exponential Moving Average (EMA) of the difference between
        12-day and 26-day EMA of close price
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests turning point in positive momentum
        Low value suggests turning point in negative momentum
        """
        window_length = 60

        sig_lines = []

        fastperiod=p1
        slowperiod=p2
        signalperiod=p3
        out = (data.ewm(halflife=fastperiod).mean()-data.ewm(halflife=slowperiod).mean()).ewm(halflife=signalperiod).mean()
        return out

def Vol(data,p1, p2):
        """
        3-month Volatility:
        Standard deviation of returns over 3 months
        http://www.morningstar.com/invglossary/historical_volatility.aspx
        Notes:
        High Value suggests that equity price fluctuates wildly
        """

        rets = data/data.shift(p1) - 1

        out = rets.rolling(p2).std()
        return out

def Vol_Rolling(data,p1, p2):
        """
        3-month Volatility:
        Standard deviation of returns over 3 months
        http://www.morningstar.com/invglossary/historical_volatility.aspx
        Notes:
        High Value suggests that equity price fluctuates wildly
        """

        rets = data/data.shift(p1) - 1
        sdev = rets.groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).std().reset_index(0,drop=True).sort_index()
        return sdev

    # 20-day Stochastic Oscillator
def Stochastic_Oscillator(data,p,p2):
        """
        20-day Stochastic Oscillator:
        K = (close price - 5-day low) / (5-day high - 5-day low)
        D = 100 * (average of past 3 K's)
        We use the slow-D period here (the D above)
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests turning point in positive momentum (expected decrease)
        Low value suggests turning point in negative momentum (expected increase)
        """
        high = data.rolling(p).max()
        low = data.rolling(p).min()
        k = (data-low)/(high-low)
        d = 100*k.rolling(p2).mean()
        return d

    # 5-day Money Flow / Volume

def Moneyflow_Volume(data,volume,p1, p2):
        """
        5-day Money Flow / Volume:
        Numerator: if price today greater than price yesterday, add price * volume, otherwise subtract
        Denominator: prices and volumes multiplied and summed
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests turning point in positive momentum (expected decrease)
        Low value suggests turning point in negative momentum (expected increase)
        """
        diff = data - data.shift(p1)

        n = (np.sign(diff)*data*volume).rolling(p2).sum()
        d = (data*volume).rolling(p2).sum()

        return n/d.astype(float)



def Moneyflow_Volume_Rolling(data,volume,p1, p2):
        """
        5-day Money Flow / Volume:
        Numerator: if price today greater than price yesterday, add price * volume, otherwise subtract
        Denominator: prices and volumes multiplied and summed
        https://www.math.nyu.edu/faculty/avellane/Lo13030.pdf
        Notes:
        High value suggests turning point in positive momentum (expected decrease)
        Low value suggests turning point in negative momentum (expected increase)
        """
        diff = data - data.shift(p1)

        volDiff = volume-volume.shift(1)
        volDiff.loc[volDiff<0] = 0

        resampledVol = volDiff.rolling(p1).sum()

        n = (np.sign(diff)*data*resampledVol).groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).sum().reset_index(0,drop=True).sort_index()
        d = (data*resampledVol).groupby(np.array(range(len(data.index)))%p1).rolling(p2/p1).sum().reset_index(0,drop=True).sort_index()

        return n/d.astype(float)

def ATR(data,p1):

	p = 75
	high = data.rolling(p).max()
	low = data.rolling(p).min()
	TR=(high-low).combine((high-data.shift(p)).abs(), max, 0).combine((low-data.shift(p)).abs(), max, 0)
	ATR = TR.ewm(span = p1).mean()
	return ATR

def RSI(data,p):
	p=75
	delta = data - data.shift(p)

	up, down = delta.copy(), delta.copy()
	up[up < 0] = 0
	down[down > 0] = 0

	# Calculate the EWMA
	roll_up1 = pd.stats.moments.ewma(up, p)
	roll_down1 = pd.stats.moments.ewma(down.abs(), p)

	# Calculate the RSI based on EWMA
	RS1 = roll_up1 / roll_down1
	RSI1 = 100.0 - (100.0 / (1.0 + RS1))
	return RSI1


def normalize_minmax(s):
    result = s.copy()
    max_value = s.max()
    min_value = s.min()
    result = (s - min_value) / (max_value - min_value)
    return result

def normalize_mean(s):
    result = s.copy()
    mean = s.mean()
    std = s.std()
    result = (s - mean) / std
    return result

def normalize_by_return(s,k):
    result = s.copy()
    rets = s/s.iloc[0]
    result = k*rets

    return result

def percentile(data):
	return data.rank(pct = True)

def transform(data):
	data['CLOSE'] = normalize_by_return(data['CLOSE'],100)
	data['RETURNS'] = data['RETURNS']*100
	data['Volume'] = (data['Volume']*data['CLOSE'].iloc[0]/100)/10000
	data['ADJCLOSE'] = normalize_by_return(data['ADJCLOSE'],100)
	data['ADJVOLUME'] = (data['ADJVOLUME']*data['ADJCLOSE'].iloc[0]/100)/10000


for root, dirs, files in os.walk('/Users/chandinijain/Auquan/DataQQ3'):
	print(dirs)

root = '/Users/chandinijain/Auquan/DataQQ3'
dirs = 'QQDataTest'
files = next(os.walk('%s/%s'%(root,dirs)))[2]
useful = []
acc = None
scrambled = pd.read_csv('%s' % 'scramblerQQ3.csv', index_col=0)  # 
# pd.DataFrame(index=files, columns=['AddVal', 'Name'])
print(scrambled)
# ### Training Params
# dateshift = -1309
# start='2014-1-1'
# end = '2016-09-08'

# ### Test Params
dateshift = 0
start='2015-03-01'
end = '2017-02-28'

scrambleddirs = 'QQDataTest'
scrambledfiles = next(os.walk('%s/%s'%(root,scrambleddirs)))[2]
for name in files[:]:
		# dateparse = lambda dates: [pd.datetime.strptime(d, '%d/%m/%y %H:%M:%S') for d in dates]

		'''
		returns*100
		scalings: Momentum: None + percentile
		Osc = *100
		MR : None + percentile
		SO: None
		Vol *100
		DR*100
		Vol*100
		'''
		try:
			print(name[:-4])
			fakenames = scrambled.loc[name[:-4], 'Name']
		except:
			print('%s likely corrupt, assigning fakename'%name)
			fakenames = 'sofake'
		if '%s.csv'%fakenames in scrambledfiles:
			print('skipping %s %s'%(name, fakenames))
			continue
		print(name, fakenames)
		data = pd.read_csv(os.path.join(root, dirs, name), index_col=0, parse_dates=True)#, parse_dates=[['DATE', 'TIME']], date_parser=dateparse)	
		data.to_csv('/Users/chandinijain/Auquan/DataQQ3/QQDataTest/%s.csv'% scrambled.loc[name[:-4], 'Name'], index=True, float_format='%.3f')
			## writecsv('C:/Users/Chandini/Auquan_Beta/%s' % finalloc, data, '%s.csv' )
                   
            