import numpy as np
import pandas as pd
import os
from json_utils import getFinalJSON

submissionId = '5a521364dc30ff0004abbbbc'
problemId = 'qq2p1'
idx = 0
strategy_name = 'LSTM'

last_cnt = 4 if idx ==0 else idx+1
for i in range(idx,last_cnt):
	if i == 0:
		continue
	path = '/Users/chandinijain/Auquan/qq2solver-data/historicalData/testData' + str(i) + '/' 
	print(path)
	names = os.listdir(path)
	names.remove('stock_list.txt')
	XDatatemp = None
	XData = None

	for name in names:
		data = pd.read_csv(path + name,index_col=0,parse_dates=True)
		data_avg = data.mean(axis=0)
		data_avg['basisVar'] = data['basis'].var()

		if XDatatemp is None:
			XDatatemp = pd.DataFrame(index = names, columns = data_avg.index)
		XDatatemp.loc[name] = data_avg.transpose()

	if XData is None:
		XData = XDatatemp
	else:
		XData = pd.concat(XData, XDatatemp, axis=1).mean(axis=1)

XData['stockAvgVolume'] = (XData['stockTotalBidVol'] + XData['stockTotalAskVol'])/2
XData['futureAvgVolume'] = (XData['futureTotalBidVol'] + XData['futureTotalAskVol'])/2
XData['stockPrice'] = (XData['stockAverageBidPrice'] + XData['stockAverageAskPrice'])/2
XData['futurePrice'] = (XData['futureAverageBidPrice'] + XData['futureAverageAskPrice'])/2

del XData['stockTopBidPrice']
del XData['stockTopAskPrice']
del XData['futureTopBidPrice']
del XData['futureTopAskPrice']
del XData['stockNextBidPrice']
del XData['stockNextAskPrice']
del XData['futureNextBidPrice']
del XData['futureNextAskPrice']
del XData['stockTopBidVol']
del XData['stockTopAskVol']
del XData['futureTopBidVol']
del XData['futureTopAskVol']
del XData['stockNextBidVol']
del XData['stockNextAskVol']
del XData['futureNextBidVol']
del XData['futureNextAskVol']
del XData['FairValue']
del XData['benchmark_score']
del XData['stockVWAP']
del XData['futureVWAP']
del XData['stockTotalBidVol']
del XData['stockTotalAskVol']
del XData['futureTotalBidVol']
del XData['futureTotalAskVol']
del XData['stockAverageBidPrice']
del XData['stockAverageAskPrice']
del XData['futureAverageBidPrice']
del XData['futureAverageAskPrice']

print(XData)

import json
target = pd.Series(index = names)

if idx == -1:
	d = getFinalJSON(submissionId, problemId)


else:
	with open(submissionId + 'result' + str(idx) + '.json') as json_data:
	    d = json.load(json_data)

instruments = d['instrument_names']
stats = d['instrument_stats']
print(stats)
for i in range(len(instruments)):
	target[instruments[i] +'.csv'] = (84*float(stats[i]['total_pnl']))
	target[instruments[i] +'.csv'] = target[instruments[i] +'.csv']*100 if idx !=0 else target[instruments[i] +'.csv']
target.dropna(inplace=True)
print(target[28:])
Ytarget = pd.Series(index = target.index)
Ytarget[target>50] = 2
Ytarget[(target>10) & (target <50)] = 1
Ytarget[(target>-5) & (target <10)] = 0
Ytarget[target<-5] = -1

XData = XData[XData.index.isin(target.index)]
from sklearn import tree
clf = tree.DecisionTreeClassifier(criterion='gini', splitter='best', max_depth=4, min_samples_split=10, min_samples_leaf=5,
								min_weight_fraction_leaf=0.0, max_features=None, random_state=None, max_leaf_nodes=None,
								min_impurity_decrease=0.01, class_weight=None, presort=False)
clf = clf.fit(XData, Ytarget)

import graphviz
dot_data = tree.export_graphviz(clf, out_file=None, 
                         feature_names=XData.columns,  
                         class_names=['-1','0','1','2'],  
                         filled=True, rounded=True,  
                         special_characters=True)  
graph = graphviz.Source(dot_data)  
graph.render(strategy_name + str(idx)) 

print(XData)#
print(target[ (XData['basis']<0) &(XData['basisVar']>0.2)])