import pandas as pd
import numpy as np
import os
import datetime as dt
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, TH


# for root, dirs, files in os.walk('/Users/chandinijain/Auquan/DataQQ3'):
files = next(os.walk('/Users/chandinijain/Auquan/repo.auquan'))[2]
useful = []
acc = None
root = '/Users/chandinijain/Auquan/DataQQ3'
dirs = 'Volume'
Data = {}
old_month = 'Apr'
dates = []
for f in files:
	if f[:5]=='basis':
		dates.append(f[6:])
# dates = [20171004, 20171005, 20171006, 20171009, 20171010, 20171011, 20171012, 20171013, 20171016, 20171017, 20171018, 20171019]
dates.remove('20171019')
dates.remove('20171102')
dates.remove('20171106')
dates.remove('20171108')
dates.remove('20171110')
dates.remove('20171114')
dates.remove('20171116')
dates.remove('20171120')
dates.remove('20171122')
dates.remove('20171124')
dates.remove('20171128')
dates.remove('20171130')
print(dates)
stocks = ['VEDL','SUNPHARMA','L&TFH','BHARTIARTL','TATAMOTORS','PFC','TATASTEEL','INFY','HDFCBANK','ICICIBANK']
for s in stocks:
	Data[s] = pd.DataFrame(columns=['basis', 'pred'])#, 'stab', 'stap', 'ftab','ftap'])
for name in dates:
	#files[2:]:
	print(name)

	basis_data = pd.read_csv('basis_%s'%name, index_col=0)
	pred_data = pd.read_csv('pred_%s'%name, index_col=0)
	# stab_data = pd.read_csv('stab_%s'%name, index_col=0)
	# stap_data = pd.read_csv('stap_%s'%name, index_col=0)
	# ftap_data = pd.read_csv('ftap_%s'%name, index_col=0)
	# ftab_data = pd.read_csv('ftab_%s'%name, index_col=0)#, parse_dates=[['DATE', 'TIME']], date_parser=dateparse)		
	# import pdb;pdb.set_trace()
	temp = pd.DataFrame(index = basis_data.index, columns=['basis', 'pred', 'stab', 'stap', 'ftab','ftap'])
	try:
		for s in stocks:
			temp['basis'] = Data[s]['basis'].append(basis_data[s])
			temp['pred'] = Data[s]['pred'].append(pred_data[s])
		# temp['stab'] = Data[s]['stab'].append(stab_data[s])
		# temp['stap'] = Data[s]['stap'].append(stap_data[s])
		# temp['ftab'] = Data[s]['ftab'].append(ftab_data[s])
		# temp['ftap'] = Data[s]['ftap'].append(ftap_data[s])
			Data[s] = Data[s].append(temp)
	except ValueError:
		continue
for s in stocks:
	print(np.sqrt(np.sum((Data[s]['basis']-Data[s]['pred'])**2)))
	Data[s].to_csv(s, float_format='%.3f', header=True, index=True, index_label='datetime')
	
		# data = data.T
		# a, data['Time'] = data.index.str.split('v', 1).str
		# data['datetime'] = pd.to_datetime(date + ' ' + data['Time'], format='%Y%m%d %H%M' )
		# data.set_index(data['datetime'], drop=True, inplace=True)
		# del[data['Time']]
		# del[data['datetime']]
		# listcols = list(data.filter(regex = date[2:4]+month.upper()))
		# data = data[listcols]
		# # data.drop(data.columns not in listc, axis = 1, inplace = True)
		# data.rename(columns=lambda x: x[:-8], inplace=True)
		# for s in stocks:
		# 	if s in data.columns:
		# 		# print(data[s])
		# 		if old_month!=month:
		# 			# print(data[s], VolData[s])
		# 			VolData[s].to_csv('/Users/chandinijain/Auquan/DataQQ3/VolumeByStock/%s%s%s.csv'%(s,old_month,date[:4]), float_format='%.0f', index=True)
		# 			VolData[s] = pd.Series(index=[])
		# 		try:
		# 			VolData[s] = VolData[s].append(data[s])
		# 		except:
		# 			print(s)
		# 			print(data[s], VolData[s])
		# old_month = month

		# data = data[data['CLOSE']!=0]
		# print(name, ' Start: ', dt.datetime.strftime(data.index[0], '%Y-%m-%d'), \
		# 	' Close: ', dt.datetime.strftime(data.index[-1], '%Y-%m-%d'), \
		# 	' Length: ', len(data))
	## check if data exists from 2014-2018 
	# 	if data.index[-1] > dt.datetime.strptime('20180420', '%Y%m%d'):
	# 		if data.index[0] < dt.datetime.strptime('20140601', '%Y%m%d'):
	# 			useful = useful + [name]
	# print(useful) 
		# print(data[-1000:])
		# data.sort_index(inplace=True)
	## compare data with ACC to see missing points	
		# if acc is None:
		# 	acc = data['CLOSE']

		# fig, ax1 = plt.subplots()
		# ax1.plot(data['CLOSE'])
		# ax1.set_xlabel('time (s)')
		# # Make the y-axis label, ticks and tick labels match the line color.
		# ax1.set_ylabel('%s'%name, color='b')
		# ax1.tick_params('y', colors='b')

		# ax2 = ax1.twinx()

		# ax2.plot(acc)
		# ax2.set_ylabel('ACC', color='r')
		# ax2.tick_params('y', colors='r')

		# fig.tight_layout()
		# plt.title('%s'%name)
		# plt.show() 