from backtester.timeRule.us_time_rule import USTimeRule
import pandas as pd

testDt = USTimeRule('2018/01/08', '2018/01/23', frequency='M').createBusinessMinSeries()


symbols = ['ABC', 'DEF', 'GHI', 'JKL', 'MNO']
j = 1
for s in symbols:
	csv = pd.read_csv('%s.csv'%s)
	print(csv)
	newcsv = pd.DataFrame(index=range(len(testDt)), columns=csv.columns)
	for c in csv.columns:
		newcsv[c] = csv.loc[0:len(testDt), c]
	newcsv.set_index(testDt, inplace=True)
	print(newcsv)
	newcsv.to_csv(path_or_buf='fixedData/%s.csv'%s, sep=',', float_format='%.2f', header=True, index=True, index_label='datetime')