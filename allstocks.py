import pandas as pd

periods = [5, 10,15,20,30,60]
unscrambler = pd.read_csv('unscrambler.csv',index_col=0) 

stocks = ['AGW','AIO','AUZ','BSN','BWU','CBT','CHV','CSR','CUN','CYD','DCO','DFZ',
        'DVV','DYE','EGV','FCY','FFA','FFS','FKI','FRE','FUR','GFQ','GGK','GYJ',
        'GYV','HHK','IES','ILW','IMC','IUE','IUQ','IYU','JSG','JYW','KAA','KKG',
        'KMV','KRZ','LDU','LKB','LPQ','MAS','MQK','MUF','NDG','NSL','NYO','OED',
        'OGU','OMP','PFK','PLX','PMS','PQS','PUO','QRK','SCN','SVG','TGI','TWK',
        'UBF','UWC','UWD','VML','VND','VSL','VTN','WAG','WTJ','XAD','XCS','XFD',
        'XIT','XPV','XYR','XZR','YGC','YHW','YUZ','ZEW','ZLX']

idx = unscrambler.loc[stocks,'Name']
data={}
profits = pd.DataFrame(index = idx, columns = periods)
for p in periods:
	try:
		data[p] = pd.read_csv('analysis%s'%p, index_col=0)
	except:
		data[p] = pd.read_csv('analysis%s.csv'%p, index_col=0)
	profits[p] = data[p]['% P']

print(profits)
for i in idx:
	opp = pd.DataFrame(index = periods, columns = data[periods[0]].columns)
	for p in periods:
		opp.loc[p] = data[p].loc[i]

	print(i)
	print(opp)


