import numpy as np
import pandas as pd
import os, sys, gc
from sklearn.cluster import KMeans
from dateutil.relativedelta import relativedelta, TH
from trading_models import *


def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

def is_in_expiry_week(date):
    last_thursday = calc_last_thursday(date)
    return (last_thursday - date).days < 7 


class TrainModel(object):
    """docstring for TrainModel."""
    def __init__(self, stocks, daterange, dset, cluster_path='', clusters=None, periods=None, factor=1, unscrambler=None, volumepath='',filterData=True):
        self.dset_path = dset
        self.cluster_path = cluster_path
        # self.standard_models = {i : None for i in range(num_clusters)}
        # self.minmax_models = {i : None for i in range(num_clusters)}
        # self.standard_normalizers = {i : None for i in range(num_clusters)}
        # self.minmax_normalizers = {i : None for i in range(num_clusters)}
        # self.stocks = ['TWK', 'FUR', 'NDG', 'IFL', 'IYU', 'DFZ']#, 'JYW', 'MAS', 'VTN', 'KFW', 'GGK', 'VND']
        # self.stocks = ['AGW']
        self.stocks = stocks
        #['AGW','AIO','AUZ','BSN','BWU','CBT','CHV','CSR','CUN','CYD','DCO','DFZ','DVV','DYE','EGV','FCY','FFA','FFS','FKI','FRE','FUR','GFQ','GGK','GYJ','GYV','HHK','IES','IFL','ILW','IMC','IUE','IUQ','IYU','JSG','JYW','KAA','KFW','KKG','KMV','KRZ','LDU','LKB','LPQ','MAS','MQK','MUF','NDG','NSL','NYO','OED','OGU','OMP','PFK','PLX','PMS','PQS','PUO','QRK','SCN','SVG','TGI','TWK','UBF','UWC','UWD','VKE','VML','VND','VSL','VTN','WAG','WTJ','XAD','XCS','XFD','XIT','XPV','XYR','XZR','YGC','YHW','YUZ','ZEW','ZLX']
        if clusters is None:
            self.loadClusters()
        else:
            self.stock_clusters = clusters

        self.loadData(daterange, unscrambler, volumepath)
        if filterData:
            self.filterData()
        self.training_features = {s : None for s in self.stocks}
        self.training_labels = {s : None for s in self.stocks}
        if periods:
            feature_periods =  {k : int(factor * periods[k]) for k in periods.keys()}
        self.computeLabels(periods)
        self.computeFeatures(feature_periods)
        self.num_features = self.training_features[self.stocks[0]].shape[1]
        print("Num features:", self.num_features)
        del self.stock_df
        gc.collect()

    def loadData(self, daterange, unscrambler, volumepath):
        self.stock_df = dict()
        for stock in self.stocks:
            df = pd.read_csv(os.path.join(self.dset_path, stock + ".csv"), index_col=0, parse_dates=True)
            self.stock_df[stock] = pd.DataFrame()
            if daterange:
                if type(daterange[0]) is tuple:
                    for d in daterange:
                        self.stock_df[stock] = pd.concat([self.stock_df[stock], df[d[0]:d[1]]])
                else:
                    self.stock_df[stock] = df[daterange[0]:daterange[1]].copy()
            else:
                self.stock_df[stock] = df.copy()
            # print(self.stock_df[stock]['basis'])

            if volumepath is not '' :
                volumedata = pd.read_csv(os.path.join(volumepath, unscrambler.loc[stock, 'Name'] + ".csv"), index_col=0, parse_dates=True)
                self.stock_df[stock] = self.stock_df[stock].join(volumedata[daterange[0]:daterange[1]], how='inner')
                # ['totalTradedStockVolume']   = volumedata['totalTradedStockVolume']
                # self.stock_df[stock]['totalTradedFutureVolume'] = volumedata['totalTradedFutureVolume']
                # self.stock_df[stock]['totalTradedStockValue'] = volumedata['totalTradedStockValue']
                # self.stock_df[stock]['totalTradedFutureValue'] = volumedata['totalTradedFutureValue']

        self.all_features = ['stockVWAP', 'futureVWAP', 'basis', 'stockTopBidVol', 'stockTopAskVol', 'stockTopBidPrice',
                        'stockTopAskPrice', 'futureTopBidVol', 'futureTopAskVol', 'futureTopBidPrice', 'futureTopAskPrice',
                        'stockNextBidVol', 'stockNextAskVol', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidVol',
                        'futureNextAskVol', 'futureNextBidPrice', 'futureNextAskPrice', 'stockTotalBidVol',
                        'stockTotalAskVol', 'futureTotalBidVol', 'futureTotalAskVol', 'stockAverageBidPrice',
                        'stockAverageAskPrice', 'futureAverageBidPrice', 'futureAverageAskPrice',
                        'totalTradedStockVolume', 'totalTradedFutureVolume', 'totalTradedStockValue',
                        'totalTradedFutureValue']

        self.features = ['stockVWAP', 'futureVWAP', 'basis', 'stockTopBidPrice',
                        'stockTopAskPrice', 'futureTopBidPrice', 'futureTopAskPrice',
                        'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidPrice', 'futureNextAskPrice', 
                        'stockAverageBidPrice', 'stockAverageAskPrice', 'futureAverageBidPrice', 'futureAverageAskPrice']
        # self.features = self.stock_df[self.stocks[0]].columns.values
        # self.features = np.delete(self.features, np.argwhere(self.features=="FairValue"))
        # self.features = np.delete(self.features, np.argwhere(self.features=="benchmark_score"))
        self.label = "FairValue"
        # print(self.features.tolist())

    def updateData(self, path, daterange, unscrambler, volumepath, filterData=True):
        self.stock_df = dict()
        for stock in self.stocks:
            df = pd.read_csv(os.path.join(path, stock + ".csv"), index_col=0, parse_dates=True)
            self.stock_df[stock] = df[daterange[0]:daterange[1]]
            if volumepath is not '' :
                volumedata = pd.read_csv(os.path.join(volumepath, unscrambler.loc[stock, 'Name'] + ".csv"), index_col=0, parse_dates=True)
                self.stock_df[stock] = self.stock_df[stock].join(volumedata[daterange[0]:daterange[1]], how='inner')
        if filterData:
            self.filterData()
        self.computeFeatures(period=5, halflife=20)
        del self.stock_df
        gc.collect()


    def filterData(self):
        # print(self.stock_df[self.stocks[0]].loc[dates[0][:5]])
        timestamps = self.stock_df[self.stocks[0]].index
        for stock in self.stocks[1:]:
            timestamps = timestamps.intersection(self.stock_df[stock].index)
        for stock in self.stocks:
            self.stock_df[stock] = self.stock_df[stock].loc[timestamps.values]

    def loadClusters(self):
        with open(self.cluster_path) as f:
            self.stock_clusters = pickle.load(f)
        print(self.stock_clusters)
        # self.num_clusters = len(set(self.stock_clusters.values()))

    # def createClusters(self, num_clusters=6):
    #     stock_averages = np.array([np.average(self.training_features[stock], axis=0) for stock in self.stocks])
    #     print(stock_averages.shape)
    #     stock_averages = (stock_averages - stock_averages.mean(axis=0)) / (stock_averages.max(axis=0) - stock_averages.min(axis=0))
    #     kmeans=KMeans(n_clusters=num_clusters)
    #     kmeansoutput=kmeans.fit(stock_averages)
    #     clusters = dict(zip(self.stocks, kmeansoutput.labels_))
    #     print(clusters)
    #     with open("clusters/kmeans_stock_clusters_%d.pkl" % num_clusters, "wb") as f:
	   #      pickle.dump(clusters, f)

    # def createFairClusters(self, num_clusters=6):
    #     stock_averages = np.array([np.average(self.training_labels[stock], axis=0) for stock in self.stocks]).reshape(-1, 1)
    #     print(stock_averages.shape)
    #     stock_averages = (stock_averages - stock_averages.mean(axis=0)) / (stock_averages.max(axis=0) - stock_averages.min(axis=0))
    #     kmeans=KMeans(n_clusters=num_clusters, n_init=50)
    #     kmeansoutput=kmeans.fit(stock_averages)
    #     clusters = dict(zip(self.stocks, kmeansoutput.labels_))
    #     print(clusters)
    #     with open("clusters/kmeans_stock_clusters_fair_%d.pkl" % num_clusters, "wb") as f:
	   #      pickle.dump(clusters, f)

    def computeLabels(self, minutes):
        for stock in self.stocks:
            # fv_df = basis[::-1].rolling(window=minutes, min_periods=1).mean()[::-1]
            fv = self.stock_df[stock]['basis'].rolling('%ds' % (minutes[stock]*60)).mean()
            fv = fv.shift(-minutes[stock], freq='Min')
            labels = pd.Series(index=self.stock_df[stock]['basis'].index)
            labels.update(fv)
            labels.fillna(method='ffill', inplace=True)
            self.training_labels[stock] = labels.as_matrix()


    def computeFeatures(self, period, halflife=None):
        for stock in self.stocks:
            num_rows = len(self.stock_df[stock])
            stock_features = np.empty((num_rows, 0))
            for feature in self.features:
                ma = self.stock_df[stock][feature].rolling(window=period[stock], min_periods=1).mean()
                stock_features = np.append(stock_features, ma.as_matrix().reshape(-1, 1), axis=1)
                stock_features = np.append(stock_features, self.stock_df[stock][feature].rolling(window=period[stock], min_periods=1).min().as_matrix().reshape(-1, 1), axis=1)
                stock_features = np.append(stock_features, self.stock_df[stock][feature].rolling(window=period[stock], min_periods=1).max().as_matrix().reshape(-1, 1), axis=1)
                stddev = self.stock_df[stock][feature].rolling(window=period[stock], min_periods=1).std().as_matrix()
                stddev.put(0, 0.0)
                stock_features = np.append(stock_features, stddev.reshape(-1,1), axis=1)
                halflife = halflife if halflife else period[stock]*4
                stock_features = np.append(stock_features, self.stock_df[stock][feature].ewm(halflife=halflife, adjust=False).mean().as_matrix().reshape(-1, 1), axis=1)

                # diff in moving average
                stock_features = np.append(stock_features, (ma - ma.shift(period[stock])).as_matrix().reshape(-1, 1), axis=1)

            stock_features = np.append(stock_features, self.stock_df[stock]['basis'].rolling(window=period[stock], min_periods=1).sum().as_matrix().reshape(-1, 1), axis=1)
            stock_features = np.append(stock_features, self.stock_df[stock]['basis'].shift(period[stock]).as_matrix().reshape(-1, 1), axis=1)
            stock_features = np.append(stock_features, (self.stock_df[stock]['basis'] - self.stock_df[stock]['basis'].shift(period[stock])).as_matrix().reshape(-1, 1), axis=1)
            pctrank = lambda x: pd.Series(x).rank(pct=False).iloc[-1]

            stock_features = np.append(stock_features, self.stock_df[stock]['basis'].rolling(window=period[stock], min_periods=1).apply(pctrank).as_matrix().reshape(-1, 1), axis=1)
            stock_features = np.append(stock_features, (3*self.stock_df[stock]['basis']/self.stock_df[stock]['basis'].abs().rolling(window=period[stock], min_periods=1).sum()).as_matrix().reshape(-1, 1), axis=1)

            # TTE feature
            def tte(t):
                timediff = calc_last_thursday(t.to_pydatetime()).replace(hour=15, minute=30) - t
                days, seconds = timediff.days, timediff.seconds
                return days*24 + seconds // 3600
            stock_features = np.append(stock_features, self.stock_df[stock].index.to_series().apply(tte).as_matrix().reshape(-1, 1), axis=1)

            # ratio of moving average
            sf_pairs = [('stockTopBidVol', 'futureTopBidVol'), ('stockTopAskVol', 'futureTopAskVol'),
            ('stockNextBidVol', 'futureNextBidVol'), ('stockNextAskVol', 'futureNextAskVol'),
            ('stockTotalBidVol', 'futureTotalBidVol'), ('stockTotalAskVol', 'futureTotalAskVol'),
            ('totalTradedStockVolume', 'totalTradedFutureVolume'), ('totalTradedStockValue', 'totalTradedFutureValue')]

            for S, F in sf_pairs:
                F_ma = self.stock_df[stock][F].rolling(window=period[stock], min_periods=1).mean()
                S_ma = self.stock_df[stock][S].rolling(window=period[stock], min_periods=1).mean()
                stock_features = np.append(stock_features, (F_ma / S_ma).as_matrix().reshape(-1, 1), axis=1)
            
            stock_features = np.nan_to_num(stock_features)
            print(stock, stock_features.shape)
            if self.training_features[stock] is None:
                self.training_features[stock] = stock_features.copy()
                # self.training_labels[stock] = self.stock_df[stock][self.label].as_matrix()
            else:
                print("CONCATENATE")
                self.training_features[stock] = np.concatenate((self.training_features[stock], stock_features), axis=0)
                # self.training_labels[stock] = np.append(self.training_labels[stock], self.stock_df[stock][self.label].as_matrix())

        del stock_features
        gc.collect()


    def computeModelParams(self, model, epochs=30, batch_size=64, validation_split=0.05, initial_epoch=0, update=False, name=""):
        model.fit(self.stocks, self.training_features, self.training_labels, epochs, batch_size, validation_split, initial_epoch, update, name)
        model.save_model("saved_models/"+name)

        gc.collect()


    def evaluateModelParams(self, model, batch_size=1):
        model.evaluate(self.stocks, self.training_features, self.training_labels, batch_size)

        gc.collect()


if __name__ == '__main__':
    problem1 = TrainModel("../trainingData" ,"clusters/problem1_kmeans_clusters_old.pkl", filterData=True)
    problem1.computeModelParams(epochs=2, batch_size=64, validation_split=0.05)
