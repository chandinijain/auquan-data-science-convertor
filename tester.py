from backtester.features.feature import Feature
from backtester.trading_system import TradingSystem
from backtester.sample_scripts.fair_value_params import FairValueTradingParams
from backtester.version import updateCheck
import pandas as pd
import numpy as np
from trading_models import *
import numpy as np

class Tester():

	def __init__(self, dataset, model_path, startdate, update=True):
        self.dataset = dataset
        self.stocks = ['AGW','AIO','AUZ','BSN','BWU','CBT','CHV','CSR','CUN','CYD','DCO','DFZ','DVV','DYE','EGV','FCY','FFA','FFS','FKI','FRE','FUR','GFQ','GGK','GYJ','GYV','HHK','IES','IFL','ILW','IMC','IUE','IUQ','IYU','JSG','JYW','KAA','KFW','KKG','KMV','KRZ','LDU','LKB','LPQ','MAS','MQK','MUF','NDG','NSL','NYO','OED','OGU','OMP','PFK','PLX','PMS','PQS','PUO','QRK','SCN','SVG','TGI','TWK','UBF','UWC','UWD','VKE','VML','VND','VSL','VTN','WAG','WTJ','XAD','XCS','XFD','XIT','XPV','XYR','XZR','YGC','YHW','YUZ','ZEW','ZLX']
        self.X = []     # Container to store features for update
        self.Y = []     # Container to store labels for update
        self.period = 5
        self.halflife = 20
        self.update = update
        self.FLAG = True
        self.today = startdate
        self.loadModel(filename=model_path)
        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)


    def getSymbolsToTrade(self):
        print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
        return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]

    def getFeatureConfigDicts(self, df):
        real_features = ['stockVWAP', 'futureVWAP', 'basis', 'stockTopBidVol', 'stockTopAskVol', 'stockTopBidPrice',
                        'stockTopAskPrice', 'futureTopBidVol', 'futureTopAskVol', 'futureTopBidPrice', 'futureTopAskPrice',
                        'stockNextBidVol', 'stockNextAskVol', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidVol',
                        'futureNextAskVol', 'futureNextBidPrice', 'futureNextAskPrice', 'stockTotalBidVol',
                        'stockTotalAskVol', 'futureTotalBidVol', 'futureTotalAskVol', 'stockAverageBidPrice',
                        'stockAverageAskPrice', 'futureAverageBidPrice', 'futureAverageAskPrice',
                        'totalTradedStockVolume', 'totalTradedFutureVolume', 'totalTradedStockValue',
                        'totalTradedFutureValue']
        generate_features = []
        for rf in real_features:
        	generate_features += df[rf].rolling(self.period).mean().iloc[-1]
        	generate_features += df[rf].rolling(self.period).min().iloc[-1]
        	generate_features += df[rf].rolling(self.period).max().iloc[-1]
        	generate_features += df[rf].rolling(self.period).sdev().iloc[-1]
        	generate_features += df[rf].ewm(halflife=self.halflife).mean().iloc[-1]

        generate_features += df['basis'].rolling(self.period).sum().iloc[-1]
        generate_features += df['basis'].iloc[-np.min(self.period, len(df))]
        generate_features += df['basis'].iloc[-1] - df['basis'].iloc[-np.min(self.period, len(df))]
        generate_features += df['basis'].iloc[-np.min(self.period, len(df)):].rank(pct=False).iloc[-1]
        generate_features += df['basis'].iloc[-1]*3

        print(generate_features)

        return generate_features


    def getFairValue(df):

        predictedFairValue = []
        x = []
        training_x = []
        for f in self.stocks:
            data = df[f].fillna(0)
            x.append(np.array(data.iloc[-1]))
            if len(data) > self.period:
                training_x.append(np.array(data.iloc[-self.period-1]))

        if self.update and len(training_x)>0:
            fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')
            self.X.append(np.array(training_x))                        # shape = d x s
            self.Y.append(np.array(fairValueData.iloc[-1]))   # shape = 1 x s
            if self.today != str(time.date()):
                self.today = str(time.date())
                print(self.today, "Updating model...")
                self.trading_model.update(np.transpose(np.array(self.X)), np.array(self.Y), epochs=5, save=False, model_name="test_model_1.pkl")

        x = np.transpose(np.array(x))    # shape = d x s

        predictedFairValue = self.trading_model.predict(x)
        return predictedFairValue