# TODO: This is a temporay way so that you can run this anywhere and backtester from auquantoolbox
# can be anywhere.
import sys
sys.path.insert(0, "../auquantoolbox")

from backtester.dataSource.data_source import DataSource
from backtester.instrumentUpdates import *
from backtester.constants import *
import datetime
import threading
import time as t

DATA_SIGNAL = 'data_signal'
DATA_SENDER = 'data_sender'


def asyncTrackBookDataFile(dataSource):
    dataSource.startTrackBookDataFile()


def castKeyValToInt(dict, keyName):
    if keyName in dict:
        dict[keyName] = int(dict[keyName])


def castKeyValToFloat(dict, keyName, div=1.0):
    if keyName in dict:
        dict[keyName] = float(dict[keyName])/div


def isStock(symbolId):
    return "-" not in symbolId


def getStockSymbolFromFuture(futureSymbolId):
    return futureSymbolId[0:-13]

def getDefaultBookData():
    bookDataFeatureKeys = ['basis', 'stockinstrumentUpdateNum','futureinstrumentUpdateNum', 'future_symbolid', 'stockVWAP', 'futureVWAP', 'totalTradedStockVolume',
                'totalTradedFutureVolume','totalTradedStockValue', 'totalTradedFutureValue', 'stockTopBidVol',
                'stockTopAskVol', 'stockTopBidPrice', 'stockTopAskPrice','futureTopBidVol','futureTopAskVol',
                'futureTopBidPrice','futureTopAskPrice','stockNextBidVol','stockNextAskVol','stockNextBidPrice',
                'stockNextAskPrice','futureNextBidVol','futureNextAskVol','futureNextBidPrice','futureNextAskPrice',
                'stockTotalBidVol','stockTotalAskVol','futureTotalBidVol','futureTotalAskVol','stockAverageBidPrice',
                'stockAverageAskPrice','futureAverageBidPrice','futureAverageAskPrice']
    toRtn = {}
    for featureName in bookDataFeatureKeys:
        toRtn[featureName] = 0.0
    toRtn['future_symbolid'] = 'dummy'
    return toRtn


def writeInstrumentUpdateProcessed(instrumentUpdateFile, instrumentUpdate):
    bookData = instrumentUpdate.getBookData()
    stockUpdateNum = bookData['stockinstrumentUpdateNum']
    futureUpdateNum = bookData['futureinstrumentUpdateNum']
    with open(instrumentUpdateFile, "a") as mfile:
         mfile.write(str(stockUpdateNum) + '\n')
         mfile.write(str(futureUpdateNum) + '\n')


class AlphaGrepDataSource(DataSource):
    def __init__(self, instrumentIds, bookDataFilename, instrumentUpdateProcessedFilename, isTailFile=True, isLive=True):
        self.__isLive = isLive
        self.__isTailFile = isTailFile
        self.__instrumentIds = instrumentIds
        self.__instrumentUpdates = InstrumentsInitializer(instrumentIds, getDefaultBookData()).initialize()
        self.__unfinishedLine = ''
        self.__bookDataFilename = bookDataFilename
        self.__bookDataFile = open(bookDataFilename, "r")
        self.__instrumentUpdateProcessedFile = instrumentUpdateProcessedFilename
        self.__shouldTrack = True
        self.__unsentInstrumentUpdates = []
        for instrumentId in instrumentIds:
            self.__unsentInstrumentUpdates.append(self.__instrumentUpdates[instrumentId].getStockInstrumentUpdate())
        if isTailFile:
            self.__bookDataFile.seek(0, 2)
            self.__thread = threading.Thread(target=asyncTrackBookDataFile, args=(self,))
            self.__thread.start()
        else:
            with self.__bookDataFile as file:
                for bookDataLine in file:
                    self.processLine(bookDataLine)

    # returns a list of instrument identifiers
    def getInstrumentIds(self):
        return self.__instrumentIds

    # returns a list of feature keys which are already present in the data.
    def getBookDataFeatures(self):
        return ['basis', 'stockinstrumentUpdateNum', 'futureinstrumentUpdateNum','future_symbolid', 'stockVWAP', 
                'futureVWAP', 'totalTradedStockVolume', 'totalTradedFutureVolume','totalTradedStockValue', 
                'totalTradedFutureValue', 'stockTopBidVol', 'stockTopAskVol', 'stockTopBidPrice', 'stockTopAskPrice',
                'stockTradedBidPrice', 'stockTradedAskPrice', 'futureTopBidVol','futureTopAskVol', 'futureTopBidPrice',
                'futureTopAskPrice', 'futureTradedBidPrice', 'futureTradedAskPrice', 'stockNextBidVol','stockNextAskVol',
                'stockNextBidPrice','stockNextAskPrice', 'futureNextBidVol','futureNextAskVol','futureNextBidPrice',
                'futureNextAskPrice', 'stockTotalBidVol', 'stockTotalAskVol','futureTotalBidVol','futureTotalAskVol',
                'stockAverageBidPrice', 'stockAverageAskPrice','futureAverageBidPrice','futureAverageAskPrice']

    def emitInstrumentUpdates(self):
        # TODO: This needs fixing. Doesnt adhere to normal schema of data source
        if self.__isLive:
            for instrumentUpdate in self.__unsentInstrumentUpdates:
                yield(instrumentUpdate)
                writeInstrumentUpdateProcessed(self.__instrumentUpdateProcessedFile, instrumentUpdate)
            self.__unsentInstrumentUpdates = []
        else:
            while True:
                for instrumentUpdate in self.__unsentInstrumentUpdates:
                    yield([instrumentUpdate.getTimeOfUpdate(), [instrumentUpdate]])
                    writeInstrumentUpdateProcessed(self.__instrumentUpdateProcessedFile, instrumentUpdate)
                self.__unsentInstrumentUpdates = []
                t.sleep(0.1)

    # you have to process both types of lines, stocks and instruments
    # they will be of the form
    # key1|value1,key2|value2,key3|value3
    # key1 and key2 will be symbolId and timestamp. Rest all are arbitrary
    def processLine(self, bookDataLine):
        bookDataComponents = bookDataLine.strip().split(',')
        bookDataUpdate = {}
        for bookDataComponent in bookDataComponents:
            x = bookDataComponent.split('|')
            bookDataUpdate[x[0]] = x[1]
        if 'symbolId' not in bookDataUpdate:
            return
        symbolId = bookDataUpdate['symbolId'].strip()
        epochtime = int(bookDataUpdate['timestamp'])/1e9
        # ghaati tarika for making sure its in india timezone for backtesting
        timeOfUpdate = datetime.datetime.utcfromtimestamp(epochtime) + datetime.timedelta(seconds=19800)
        bookDataUpdate.pop('symbolId', None)
        bookDataUpdate.pop('timestamp', None)
        instrumentUpdate = None
        if isStock(symbolId):
            stockBookData = self.parseStockBookDataUpdate(bookDataUpdate)
            self.__instrumentUpdates[symbolId].updateStockData(timeOfUpdate, stockBookData)
            instrumentUpdate = self.__instrumentUpdates[symbolId].getStockInstrumentUpdate()
        else:
            futureBookData = self.parseFutureBookDataUpdate(bookDataUpdate)
            stockSymbolId = getStockSymbolFromFuture(symbolId)
            bookDataUpdate['future_symbolid'] = symbolId
            self.__instrumentUpdates[stockSymbolId].updateFutureData(timeOfUpdate, futureBookData)
            instrumentUpdate = self.__instrumentUpdates[stockSymbolId].getStockInstrumentUpdate()
        if instrumentUpdate:
            self.__unsentInstrumentUpdates.append(instrumentUpdate)

    def startTrackBookDataFile(self):
        while True:
            readLine = self.__bookDataFile.readline()
            if readLine:
                self.__unfinishedLine = self.__unfinishedLine + readLine
                if self.__unfinishedLine.endswith('\n'):
                    try:
                        self.processLine(self.__unfinishedLine)
                    except:
                        self.__unfinishedLine = ''
                    self.__unfinishedLine = ''
            else:
                t.sleep(0.1)
            if not self.__shouldTrack:  # TODO: Lock this to prevent race condition
                break

    def parseStockBookDataUpdate(self, bookDataUpdate):
        # TODO: Change from string to ints/floats when needed. Add more here
        castKeyValToFloat(bookDataUpdate, 'VWAP',100.0)
        castKeyValToFloat(bookDataUpdate, 'last_traded_price',100.0)
        castKeyValToFloat(bookDataUpdate, 'total_traded_value',100.0)
        castKeyValToInt(bookDataUpdate, 'total_traded_quantity')
        castKeyValToFloat(bookDataUpdate, 'TopAskPrice',100.0)
        castKeyValToFloat(bookDataUpdate, 'TopBidPrice',100.0)
        castKeyValToInt(bookDataUpdate, 'TopAskVol')
        castKeyValToInt(bookDataUpdate, 'TopBidVol')
        castKeyValToFloat(bookDataUpdate, 'NextAskPrice',100.0)
        castKeyValToFloat(bookDataUpdate, 'NextBidPrice',100.0)
        castKeyValToInt(bookDataUpdate, 'NextAskVol')
        castKeyValToInt(bookDataUpdate, 'NextBidVol')
        castKeyValToFloat(bookDataUpdate, 'AverageAskPrice',100.0)
        castKeyValToFloat(bookDataUpdate, 'AverageBidPrice',100.0)
        castKeyValToInt(bookDataUpdate, 'TotalAskVol')
        castKeyValToInt(bookDataUpdate, 'TotalBidVol')
        castKeyValToInt(bookDataUpdate, 'instrumentUpdateNum')
        castKeyValToFloat(bookDataUpdate, 'TradedAskPrice',100.0)
        castKeyValToFloat(bookDataUpdate, 'TradedBidPrice',100.0)
        for key in bookDataUpdate.keys():
            bookDataUpdate['stock' + key] = bookDataUpdate[key]
            bookDataUpdate.pop(key, None)
        # TODO: Hacky. Fix in basis execustion system to not assume these key names
        bookDataUpdate['totalTradedStockVolume'] = bookDataUpdate['stocktotal_traded_quantity']
        bookDataUpdate.pop('stocktotal_traded_quantity', None)
        bookDataUpdate['totalTradedStockValue'] = bookDataUpdate['stocktotal_traded_value']
        bookDataUpdate.pop('stocktotal_traded_value', None)

        return bookDataUpdate

    def parseFutureBookDataUpdate(self, bookDataUpdate):
        # TODO: Change from string to ints/floats when needed, change keys. Add more here
        castKeyValToFloat(bookDataUpdate, 'VWAP',100.0)
        castKeyValToFloat(bookDataUpdate, 'last_traded_price',100.0)
        castKeyValToFloat(bookDataUpdate, 'total_traded_value',100.0)
        castKeyValToInt(bookDataUpdate, 'total_traded_quantity')
        castKeyValToFloat(bookDataUpdate, 'TopAskPrice',100.0)
        castKeyValToFloat(bookDataUpdate, 'TopBidPrice',100.0)
        castKeyValToInt(bookDataUpdate, 'TopAskVol')
        castKeyValToInt(bookDataUpdate, 'TopBidVol')
        castKeyValToFloat(bookDataUpdate, 'NextAskPrice',100.0)
        castKeyValToFloat(bookDataUpdate, 'NextBidPrice',100.0)
        castKeyValToInt(bookDataUpdate, 'NextAskVol')
        castKeyValToInt(bookDataUpdate, 'NextBidVol')
        castKeyValToFloat(bookDataUpdate, 'AverageAskPrice',100.0)
        castKeyValToFloat(bookDataUpdate, 'AverageBidPrice',100.0)
        castKeyValToInt(bookDataUpdate, 'TotalAskVol')
        castKeyValToInt(bookDataUpdate, 'TotalBidVol')
        castKeyValToInt(bookDataUpdate, 'instrumentUpdateNum')
        castKeyValToFloat(bookDataUpdate, 'TradedAskPrice',100.0)
        castKeyValToFloat(bookDataUpdate, 'TradedBidPrice',100.0)
        for key in bookDataUpdate.keys():
            bookDataUpdate['future' + key] = bookDataUpdate[key]
            bookDataUpdate.pop(key, None)
        # TODO: Hacky. Fix in basis execustion system to not assume these key names
        bookDataUpdate['totalTradedFutureVolume'] = bookDataUpdate['futuretotal_traded_quantity']
        bookDataUpdate.pop('futuretotal_traded_quantity', None)
        bookDataUpdate['totalTradedFutureValue'] = bookDataUpdate['futuretotal_traded_value']
        bookDataUpdate.pop('futuretotal_traded_value', None)

        return bookDataUpdate

    def cleanup(self):
        if self.__isTailFile:
            self.__shouldTrack = False  # TODO: Lock this to prevent race condition
            self.__thread.join()


class InstrumentsInitializer():
    def __init__(self, instrumentIds, defaultBookData):
        self.instrumentIds = instrumentIds
        self.__defaultBookData = defaultBookData

    def initialize(self):
        # TODO have the close prices to initialize with everyday
        instrument_data_hash = {}

        for instrumentId in self.instrumentIds:
            instrument_data_hash[instrumentId] = InstrumentUpdateData(instrumentId, self.__defaultBookData)

        return instrument_data_hash


class InstrumentUpdateData():
    def __init__(self, instrumentId, defaultBookData):
        self.instrumentId = instrumentId
        self.bookData = defaultBookData.copy()
        self.timeOfUpdate = datetime.datetime.now() - datetime.timedelta(days=1000)  # TODO Better fix please
        self.hasReceivedStockUpdate = True
        self.hasReceivedFutureUpdate = True

    def updateBasis(self):
        self.bookData['basis'] = (self.bookData['stockTopAskPrice'] + self.bookData['stockTopBidPrice'] - self.bookData['futureTopAskPrice'] - self.bookData['futureTopBidPrice']) / 2.0

    def updateStockData(self, timeOfUpdate, stockDataUpdate):
        self.timeOfUpdate = timeOfUpdate
        self.hasReceivedStockUpdate = True
        self.bookData.update(stockDataUpdate)

    def updateFutureData(self, timeOfUpdate, futureDataUpdate):
        self.timeOfUpdate = timeOfUpdate
        self.hasReceivedFutureUpdate = True
        self.bookData.update(futureDataUpdate)

    def getStockInstrumentUpdate(self):
        if not self.hasReceivedFutureUpdate or not self.hasReceivedStockUpdate:
            return None
        self.updateBasis()
        return StockInstrumentUpdate(stockInstrumentId=self.instrumentId,
                                     tradeSymbol=self.instrumentId,
                                     timeOfUpdate=self.timeOfUpdate,
                                     bookData=self.bookData.copy())


if __name__ == "__main__":
    placer = AlphaGrepDataSource(['VEDL'], 'bookData.txt')
    print ("done")
