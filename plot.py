import pandas as pd
import numpy as np
import os
import datetime as dt
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, TH


# for root, dirs, files in os.walk('/Users/chandinijain/Auquan/DataQQ3'):
files = next(os.walk('/Users/chandinijain/Auquan/repo.auquan'))[2]
stocks = ['VEDL','SUNPHARMA','L&TFH','BHARTIARTL','TATAMOTORS','PFC','TATASTEEL','INFY','HDFCBANK','ICICIBANK']
for s in stocks:
	data = pd.read_csv('%s'%s, index_col=0, parse_dates=True)
	z = data.resample('60T').mean()
	z = z.dropna(axis=1, how='all')
	z = z.dropna(axis=0, how='all')
	print(s, np.sqrt(np.mean((z['basis']-z['pred'])**2)))
	plt.plot(z['basis'])
	plt.plot(z['pred'])
	plt.show()
	
	z.to_csv(s, float_format='%.3f', header=True, index=True, index_label='datetime')