from retrain_models import *
from trading_models import *
import pickle
import requests
import keras
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
import numpy as np
import datetime, gc
from datetime import timedelta
import sys

def loadModel(filename=""):
        '''
        Load model paramters for each stock using pickle file
        '''
        make_keras_picklable()
        with open(filename, "rb") as f:
            trading_model = pickle.load(f)
        # print(trading_model)
        # print(trading_model.stocks)
        print(trading_model.stock_clusters)
        return trading_model
dates = [ '20170904', '20170905', '20170906']#, '20170907', 
		 # '20170908', '20170911', '20170912', '20170913', '20170914',
		 # '20170915', '20170918', '20170919']
for j in range(len(dates)-2):
	model_path = 'saved_models/clustered_minmax_lstm_model_1_10_clusters'+dates[j]+'.pkl'

	#model_path ='clustered_minmax_lstm_model_1_10_clusters.pkl'
	#
	#clustered_minmax_lstm_model_1_10_clusters.pkl'

def evaluateModel(trading_model, stocks, X, Y, unscrambler, outf="", evalCluster=False):
    if evalCluster:
        err_df = pd.DataFrame(index=list(np.arange(trading_model.num_clusters))+stocks, columns=["name", "mse", "rmse", "clusterID"])
    else:
        err_df = pd.DataFrame(index=stocks, columns=["name", "mse", "rmse", "clusterID"])
    # X = np.array(X)
    # Y = np.array(Y)
    # print(X.shape, Y.shape)
    for stock in stocks:
        mse = trading_model.models[trading_model.stock_clusters[stock]].evaluate(
                    x=np.expand_dims(trading_model.normalizers[trading_model.stock_clusters[stock]].transform(X[stock]), axis=1),
                    y=Y[stock],
                    batch_size=1)
        rmse = np.sqrt(mse)
        err_df.loc[stock] = [unscrambler.loc[stock].values[0], mse, rmse, trading_model.stock_clusters[stock]]

    if evalCluster:
        for cid in range(trading_model.num_clusters):
            x = [X[stock] for stock in stocks if trading_model.stock_clusters[stock] == cid]
            if len(x) == 0:
                continue
            x = np.vstack(x)
            y = np.vstack([Y[stock].reshape(-1,1) for stock in stocks if trading_model.stock_clusters[stock] == cid])
            cluster_mse = trading_model.models[cid].evaluate(
                        x=np.expand_dims(trading_model.normalizers[cid].transform(x), axis=1),
                        y=y,
                        batch_size=1)
            cluster_rmse = np.sqrt(cluster_mse)
            err_df.loc[cid] = ["cluster_%s" % cid, cluster_mse, cluster_rmse, cid]
    print(err_df)
    if outf != "":
        err_df.to_csv(outf)


# stocks = ['AIO', 'AUZ', 'BWU', 'CBT', 'CHV', 'CUN', 'DFZ', 'DVV', 'EGV', 'FFA', 'FFS', 'FKI', 'FUR', 'GGK', 'GYV', 'HHK', 'IES', 'IFL', 'ILW', 'IMC', 'IUQ', 'IYU', 'JSG', 'KFW', 'KRZ', 'LDU', 'LKB', 'MAS', 'NDG', 'NYO', 'OED', 'OGU', 'PFK', 'PLX', 'PMS', 'PUO', 'QRK', 'SVG', 'UBF', 'UWC', 'UWD', 'VKE', 'VND', 'WAG', 'XAD', 'XCS', 'XFD', 'XIT', 'YUZ', 'ZEW']
stocks = ['AIO','CUN']
stocks = ['AIO', 'AUZ', 'BWU', 'CBT', 'CHV', 'CUN', 'DVV', 'EGV', 'IES', 'IFL']
#['AGW','AIO','BSN','CHV','CUN','DCO']
         # 'FFA','FFS','IES','ILW','IMC','IUQ',
         # 'IYU','JYW','KAA','KKG','KRZ','LPQ',
         # 'MUF','NYO','OMP','PFK','PQS','PUO',
         # 'QRK','TWK','WAG','XPV','XYR','YHW','ZLX']
print(stocks)
print(len(stocks))
startdate = datetime.date(2017, 9, 1)
months = 2
normal_weeks, expiry_week = [], []

for m in range(months):
    last_thursday = calc_last_thursday(startdate)
    normal_weeks += [((startdate).strftime("%Y%m%d"), (last_thursday + timedelta(days=-7)).strftime("%Y%m%d"))]
    expiry_week += [((last_thursday + timedelta(days=-6)).strftime("%Y%m%d"), (last_thursday).strftime("%Y%m%d"))]
    startdate = last_thursday + timedelta(days=1)

print(normal_weeks)
print(expiry_week)

dset = 'testData'
unscrambler = pd.read_csv('unscrambler.csv',index_col=0)
volumepath = '' #/Users/chandinijain/Auquan/qq2solver-data/historicalData/cleanDataVolume'
windows = [normal_weeks, expiry_week]
week_name = ["normal_week_model", "expiry_week_model"]
factor = 1
stock_periods = pd.read_csv('period.csv', index_col=0)
stock_periods = stock_periods.to_dict()['period']
stock_clusters = {s : i for i,s in enumerate(stocks)}


for i, w in enumerate(windows):
    print(w)
    model_name = '%s_%d' % (week_name[i], factor)
    # model_name = 'new_lstm_combined_model'
    model_path = '../models/new/%s.pkl' % model_name
    trading_model = loadModel(filename=model_path)
    problem1 = TrainModel(stocks, w, dset, cluster_path='', clusters=stock_clusters, periods=stock_periods,
                        factor=factor, unscrambler=unscrambler, volumepath=volumepath, filterData=False)
    evaluateModel(trading_model, stocks, problem1.training_features, problem1.training_labels,
                  unscrambler, outf="rmse/rmse_new_%s.csv" % (model_name))
    # sys.exit()
    # del problem1, trading_model
    gc.collect()

def calc_rmse_manually():
    dates = ['20170904', '20170905', '20170906', '20170907',
    		 '20170908', '20170911', '20170912', '20170913', '20170914',
    		 '20170915', '20170918', '20170919']
    for j in range(len(dates)-2):
    	#model_path = 'saved_models/clustered_minmax_lstm_model_1_10_clusters'+dates[j]+'.pkl'

    	model_path ='clustered_minmax_lstm_model_1_10_clusters.pkl'
    	#
    	#clustered_minmax_lstm_model_1_10_clusters.pkl'

    	trading_model = loadModel(filename=model_path)

	stocks = ['BSN']#trading_model.stocks
	#['AGW','AIO','BSN','CHV','CUN','DCO']
	         # 'FFA','FFS','IES','ILW','IMC','IUQ',
	         # 'IYU','JYW','KAA','KKG','KRZ','LPQ',
	         # 'MUF','NYO','OMP','PFK','PQS','PUO',
	         # 'QRK','TWK','WAG','XPV','XYR','YHW','ZLX']


    	daterange = [dates[j+1],dates[j+2]]

    	dset = '/home/ec2-user/vaibhav/historicalData/mergedData/testData'

    	unscrambler = pd.read_csv('unscrambler.csv',index_col=0)

    	volumepath = '' #/Users/chandinijain/Auquan/qq2solver-data/historicalData/cleanDataVolume'

    	problem1 = TrainModel(stocks, daterange, dset, '', trading_model.stock_clusters, unscrambler, volumepath, filterData=True)

	predictions = np.zeros(shape=(problem1.training_features[stocks[0]].shape[0], len(stocks)))
	for i in range(len(problem1.training_features[stocks[0]])):
		X = np.zeros(shape=(len(stocks),problem1.training_features[stocks[0]].shape[1]))#Numpy array of shape (s x d)
		    #   where d = Number of features and s = Number of stocks
		for s in range(len(stocks)):
			X[s] = problem1.training_features[stocks[s]][i]
		if i < 10:
			print(X)
		X = np.transpose(X)

		predictions[i] = trading_model.predict(X, stocks)

	yActual = problem1.training_labels
	predictions = np.transpose(predictions)
	print(yActual)
	print(predictions)
	for s in range(len(stocks)):
		error = (predictions[s] - yActual[stocks[s]])

    		mse = np.mean(error**2)

    		print(stocks[s], dates[j+1], mse, np.sqrt(mse))
