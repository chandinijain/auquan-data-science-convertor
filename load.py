import json
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
submissionId = '5a521364dc30ff0004abbbbc'

idx = 1

with open(submissionId + 'result' + str(idx) + '.json') as json_data:
	    d = json.load(json_data)

instruments = d['instrument_names']
stats = d['instrument_stats']

# d['dates'] = [dt.datetime.strptime(date,'%Y-%m-%d %H:%M:%S') for date in d['dates']]
print(len(d['dates']))
target = pd.Series(index = instruments)

for i in range(len(instruments)):
	target[instruments[i] +'.csv'] = (84*float(stats[i]['total_pnl']))
	target[instruments[i] +'.csv'] = target[instruments[i] +'.csv']*100 if idx !=0 else target[instruments[i] +'.csv']
target.dropna(inplace=True)
print(target)
print(d['metrics_values'])
print(d['metrics'])
plt.plot(d['dates'],d['total_pnl'])
plt.xticks(np.linspace(0,len(d['dates'])-2,6))
ax = plt.gca()

# Rewrite the y labels
x_labels = ax.get_xticks()
ax.set_xticklabels([d['dates'][int(x)][5:10] for x in x_labels])
plt.show()
newjson = {}
newjson['dates'] = d['dates']
newjson['total_pnl'] = d['total_pnl']
newjson['metrics'] = {}
newjson['instrument_stats'] = {}
for j in range(len(d['metrics'])):
	newjson['metrics'][d['metrics'][j]] = d['metrics_values'][j]
for j in range(len(instruments)):
	newjson['instrument_stats'][instruments[j]] = target[instruments[j] +'.csv']
newjson['metrics']['Max Drawdown(%)'] = newjson['metrics']['Max Drawdown(%)']*100
newjson['metrics']['Total Pnl(%)'] = newjson['metrics']['Total Pnl(%)']*100
# newjson['metrics']['RoC(%)'] = newjson['metrics']['RoC(%)']*100
with open('OS_SepOct.txt', 'w') as outfile:
    json.dump(newjson, outfile)