# TODO: This is a temporay way so that you can run this anywhere and backtester from auquantoolbox
# can be anywhere.
import sys
sys.path.insert(0, "../auquantoolbox")

from backtester.trading_system_parameters import TradingSystemParameters
from backtester.executionSystem.live_basis_execution_system import LiveBasisExecutionSystem
from backtester.constants import *
from backtester.features.feature import Feature
from backtester.logger import *
from backtester.trading_system import TradingSystem
import numpy as np
import pandas as pd
from live_alpha_order_placer import LiveAlphaOrderPlacer
from alpha_grep_data_source import AlphaGrepDataSource
from live_time_rule import LiveTimeRule
from backtesting_time_rule import BacktestingTimeRule
from datetime import datetime, timedelta
from backtester.timeRule.us_time_rule import USTimeRule
from backtester.orderPlacer.backtesting_order_placer import BacktestingOrderPlacer
import os
import pickle
import requests
import types
import tempfile

from extratrees import GBTradingFunctions

pd.options.display.max_rows = 99
pd.set_option('expand_frame_repr', False)
pd.set_option('display.float_format', lambda x: '%.4f' % x)
prod = pd.read_csv(sys.argv[2], header=None, index_col = 0)
STOCKS = prod.index
#'AGW', 'AIO', 'AUZ', 'BSN','BWU', 'CBT', 'CHV', 'CSR','CUN', 'CYD', 'DCO', 'DFZ',
#'DVV', 'DYE', 'EGV', 'FCY','FFA', 'FFS', 'FKI', 'FRE','FUR', 'GFQ', 'GGK', 'GYJ','GYV', 'HHK', 'IES', 'IFL',
#'ILW', 'IMC', 'IUE', 'IUQ','IYU', 'JSG', 'JYW', 'KAA','KFW', 'KKG', 'KMV', 'KRZ','LDU','LKB']
#'LPQ', 'MAS','MQK', 'MUF', 'NDG', 'NSL','NYO', 'OED', 'OGU', 'OMP','PFK', 'PLX', 'PMS', 'PQS','PUO', 'QRK', 'SCN', 'SVG',
#'TGI', 'TWK', 'UBF', 'UWC','UWD', 'VKE', 'VML', 'VND','VSL', 'VTN', 'WAG', 'WTJ','XAD', 'XCS', 'XFD', 'XIT',
#'XPV', 'XYR', 'XZR', 'YGC','YHW', 'YUZ', 'ZEW', 'ZLX']

#'AGW','AIO','BSN','CHV','CUN','DCO',
#          'FFA','FFS','IES','ILW','IMC','IUQ',
#          'IYU','JYW','KAA','KKG','KRZ','LPQ',
#          'MUF','NYO','OMP','PFK','PQS','PUO',
#          'QRK','TWK','WAG','XPV','XYR','YHW','ZLX']

DATE_TO_TRADE = sys.argv[1]
BOOK_DATA_FILE = '../repo.auquan/bookData-' + DATE_TO_TRADE

ORDER_PLACER_FILE = '../repo.auquan/orderPlacer.txt'
ORDER_CONFIRMER_FILE = '../repo.auquan/orderConfirmer-' + DATE_TO_TRADE
INSTRUMENT_UPDATE_PROCESSED_FILE = "../repo.auquan/instrumentUpdateProcessed.txt"
FREQUENCY_TRADE_SECS = 10

IS_LIVE = False
FROM_SERVER = True

LOT_SIZE_FILE = '/home/cjain/fo_mktlots_Sep2017.csv'
THRESHOLD_FILE = '/home/cjain/live_order_placer/rmse_et.csv'
INITIALIZER = None #'savedData20180517'

class LivePairTradingParams(TradingSystemParameters):

    def __init__(self, tradingFunctions):
        self.__tradingFunctions = tradingFunctions
        self.__bookDataFile = BOOK_DATA_FILE
        self.__orderPlacerFile = ORDER_PLACER_FILE
        self.__orderConfirmerFile = ORDER_CONFIRMER_FILE
        self.__instrumentUpdateProcessedFile = INSTRUMENT_UPDATE_PROCESSED_FILE
        super(LivePairTradingParams, self).__init__()
        if IS_LIVE:
            self.__startTime = datetime.now()
            self.__endTime = self.__startTime + timedelta(seconds=900)
        else:
            self.__startTime = datetime.strptime(DATE_TO_TRADE, '%Y%m%d') + timedelta(hours=9, minutes=15, seconds=2)
            self.__endTime = datetime.strptime(DATE_TO_TRADE, '%Y%m%d') + timedelta(hours=15, minutes=30, seconds=2)
        self.__instrumentIds = self.__tradingFunctions.getSymbolsToTrade()
        self.__lotSizes = pd.read_csv(LOT_SIZE_FILE,index_col=1)
        self.__lotSizes.set_index(self.__lotSizes.index.str.strip(), inplace=True)

    def getStartingCapital(self):
        return 85*1000000

    def getDataParser(self):
        # Change last parameter to false, if you want to read from an already written file as opposed to tailing from a file.
        return AlphaGrepDataSource(self.__instrumentIds, self.__bookDataFile, self.__instrumentUpdateProcessedFile, FROM_SERVER , IS_LIVE)

    def getTimeRuleForUpdates(self):
        # TODO: Better implementation of this and end time here matching actual trading end time
        # TODO: Might have to take care of trading days too in this later on
        return LiveTimeRule(self.__startTime, FREQUENCY_TRADE_SECS, self.__endTime)

    def getBenchmark(self):
        return None


    def getCustomFeatures(self):
        return {'prediction_feature': PredictionFeature,
                'prediction_error' : PredictionErrorFeature,
                'enter_price':EnterPrice,
               'enter_flag':EnterFlag,
                'basis_calculator': BasisCalculator,
                'spread': SpreadCalculator,
                'total_fees': TotalFeesCalculator}
    def getInstrumentFeatureConfigDicts(self):
        stockFeatureConfigs = self.__tradingFunctions.getInstrumentFeatureConfigDicts()

        fairValuePrediction = {'featureKey': 'prediction',
                               'featureId': 'prediction_feature',
                               'params': {'function': self.__tradingFunctions}}
        enterPriceDict = {'featureKey': 'enter_price',
                     'featureId': 'enter_price',
                     'params': {'price': self.getPriceFeatureKey()}}
        enterFlagDict = {'featureKey': 'enter_flag',
                     'featureId': 'enter_flag',
                     'params': {}}
        scoreDict = {'featureKey': 'score',
                     'featureId': 'prob1_score',
                     'params': {'predictionKey': 'prediction',
                                'price': 'basis'}}
        sdevDictForExec = {'featureKey': 'basis_sdev',
                           'featureId': 'moving_sdev',
                            'params': {'period': 60*60/FREQUENCY_TRADE_SECS,
                                        'featureName': 'basis'}}
        predError = {'featureKey': 'execution_threshold',
                           'featureId': 'prediction_error',
                           'params': {'fileName': THRESHOLD_FILE}}
        spreadConfigDict = {'featureKey': 'spread',
                            'featureId': 'spread',
                            'params': {'instr1bid': 'stockTopBidPrice',
                                       'instr1ask': 'stockTopAskPrice',
                                       'instr2bid': 'futureTopBidPrice',
                                       'instr2ask': 'futureTopAskPrice'}}
        feesConfigDict = {'featureKey': 'fees',
                          'featureId': 'total_fees',
                          'params': {'price': 'stockVWAP',
                                     'feesDict': {1: 0.00012, -1: 0.00012, 0: 0},
                                     'spread': 'spread'}}
        profitlossConfigDict = {'featureKey': 'pnl',
                                'featureId': 'pnl',
                                'params': {'price': self.getPriceFeatureKey(),
                                           'fees': 'fees'}}
        capitalConfigDict = {'featureKey': 'capital',
                             'featureId': 'capital',
                             'params': {'price': 'stockVWAP',
                                        'fees': 'fees',
                                        'capitalReqPercent': 0.15}}

        return {INSTRUMENT_TYPE_STOCK: stockFeatureConfigs +
                [fairValuePrediction, sdevDictForExec, predError, scoreDict,
                 spreadConfigDict, feesConfigDict,
                 profitlossConfigDict, capitalConfigDict, enterPriceDict, enterFlagDict]}

    def getMarketFeatureConfigDicts(self):

        scoreDict = {'featureKey': 'score',
                     'featureId': 'prob1_score',
                     'params': {'price': 'basis',
                                'instrument_score_feature': 'score',
                                'benchmark_score_feature': 'benchmark_score'}}
        # TODO
        return []


    def getExecutionSystem(self):
        lots = self.__lotSizes[self.__lotSizes.columns[1]][self.__instrumentIds].astype(float)
        print(lots)
        threshold = pd.read_csv(THRESHOLD_FILE, index_col=1)
        print(threshold['rmse'][self.__instrumentIds].astype(float))
        return LiveBasisExecutionSystem(basisEnter_threshold=.5, basisExit_threshold=0.1,
                                    basisLongLimit=10*lots, basisShortLimit=10*lots,
                                    basisCapitalUsageLimit=0.1, basisLotSize=lots,
                                    basisLimitType='L', basis_thresholdParam=threshold['rmse'][self.__instrumentIds].astype(float),
                                    price=self.getPriceFeatureKey(), feeDict=0.0001, feesRatio=1.25,
                                    spreadLimit=0.075)

    def getOrderPlacer(self):
        if FROM_SERVER:
            return LiveAlphaOrderPlacer(self.__orderPlacerFile, self.__orderConfirmerFile)
        else:
            return BacktestingOrderPlacer()

    def getLookbackSize(self):
        return 360*60/FREQUENCY_TRADE_SECS

    def getPriceFeatureKey(self):
        return 'basis'

    def getInitializer(self):
        return INITIALIZER

class PredictionFeature(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        tf = featureParams['function']
        prediction = tf.getPrediction(time, updateNum, instrumentManager)

        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()
        d = pd.DataFrame(index=instrumentManager.getAllInstrumentsByInstrumentId(),
                         columns=['basis','predictions','position','spread'])
        if len(lookbackInstrumentFeatures.getFeatureDf('basis')) > 0:
          d['basis'] = lookbackInstrumentFeatures.getFeatureDf('basis').iloc[-1]
        if len(lookbackInstrumentFeatures.getFeatureDf('position')) > 0:
          d['position'] = lookbackInstrumentFeatures.getFeatureDf('position').iloc[-1]
          d['enter_price'] = lookbackInstrumentFeatures.getFeatureDf('enter_price').iloc[-1]
          d['spread'] = lookbackInstrumentFeatures.getFeatureDf('spread').iloc[-1]
          d['sdev'] = lookbackInstrumentFeatures.getFeatureDf('basis_sdev').iloc[-1]
          d['threshold'] = lookbackInstrumentFeatures.getFeatureDf('execution_threshold').iloc[-1]
        instrumentsDict = instrumentManager.getAllInstrumentsByInstrumentId()
        currentPosition = pd.Series([instrumentManager.getInstrument(x).getCurrentPosition() for x in instrumentsDict], index= instrumentsDict)
        d['position'] = currentPosition
        d['predictions'] = prediction
        print(d)

        return prediction

class PredictionErrorFeature(Feature):
      @classmethod
      def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        if len(instrumentLookbackData.getFeatureDf(featureKey)) <= 1:
          threshold = pd.read_csv(featureParams['fileName'], index_col=1)
          idx = instrumentManager.getAllInstrumentsByInstrumentId()
          return threshold['rmse'][idx].astype(float)
        rmse = instrumentLookbackData.getFeatureDf(featureKey).iloc[-1]
        #error = (instrumentLookbackData.getFeatureDf('prediction').iloc[-5] - instrumentLookbackData.getFeatureDf('FairValue').iloc[-1])**2
        # TODO: Update rmse realtime
        return rmse



class BasisCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        try:
            currentStockBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1bid']).iloc[-1]
            currentStockAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1ask']).iloc[-1]
            currentFutureBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2bid']).iloc[-1]
            currentFutureAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2ask']).iloc[-1]
        except KeyError:
            logError('Bid and Ask Price Feature Key does not exist')

        basis = currentStockAskPrice + currentStockBidPrice - currentFutureAskPrice - currentFutureBidPrice
        return basis / 2.0


class SpreadCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        return 0
        #instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        #try:
        #    currentStockBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1bid']).iloc[-1]
        #    currentStockAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1ask']).iloc[-1]
        #    currentFutureBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2bid']).iloc[-1]
        #    currentFutureAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2ask']).iloc[-1]
        #except KeyError:
        #    logError('Bid and Ask Price Feature Key does not exist')

        #currentSpread = currentStockAskPrice - currentStockBidPrice + currentFutureAskPrice - currentFutureBidPrice
        #return np.maximum(0.075, currentSpread / 4.0)


class TotalFeesCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()

        positionData = instrumentLookbackData.getFeatureDf('position')
        feesDict = featureParams['feesDict']
        currentPosition = positionData.iloc[-1]
        previousPosition = 0 if updateNum <= 2 else positionData.iloc[-2]
        changeInPosition = pd.Series(0, index=instrumentManager.getAllInstrumentsByInstrumentId()) if updateNum <= 2 else (currentPosition - previousPosition)
        changeInPosition.fillna(0)
        f = [feesDict[np.sign(x)] for x in changeInPosition]
        fees = np.abs(changeInPosition) * [feesDict[np.sign(x)] for x in changeInPosition]
        if 'price' in featureParams:
            try:
                priceData = instrumentLookbackData.getFeatureDf(featureParams['price'])
                currentPrice = priceData.iloc[-1]
            except KeyError:
                logError('Price Feature Key does not exist')

            fees = fees * currentPrice
        # TODO: Implement
        total = 2 * fees \
           # + (np.abs(changeInPosition) * instrumentLookbackData.getFeatureDf(featureParams['spread']).iloc[-1])
        return total
class EnterPrice(Feature):
    problem1Solver = None

    @classmethod
    def setProblemSolver(cls, problem1Solver):
        Problem1PredictionFeature.problem1Solver = problem1Solver

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        # import pdb;pdb.set_trace()
        instrumentsDict = instrumentManager.getAllInstrumentsByInstrumentId()
        positionData = instrumentLookbackData.getFeatureDf('position')
        previousPosition = pd.Series(0, index= instrumentsDict) if updateNum <= 2 else positionData.iloc[-1]
        currentPosition = pd.Series([instrumentManager.getInstrument(x).getCurrentPosition() for x in instrumentsDict], index= instrumentsDict)
        currentPrice = pd.Series([instrumentManager.getInstrument(x).getLastTradePrice() for x in instrumentsDict], index = instrumentsDict)
        changeInPosition = 0 if updateNum <= 2 else (currentPosition - previousPosition)
        #for instrumentId in instrumentsDict:
        #    print(instrumentId , instrumentManager.getInstrument(instrumentId).getCurrentPosition())
        avgEnterPrice = 0*currentPrice if updateNum <= 2 else instrumentLookbackData.getFeatureDf(featureKey).iloc[-1]
        avgEnterPrice[(currentPosition!=0) & (currentPosition.abs()- previousPosition.abs()>0)] = (previousPosition*avgEnterPrice + changeInPosition * currentPrice)/currentPosition
        avgEnterPrice[currentPosition==0] = 0

        return avgEnterPrice


class EnterFlag(Feature):
    problem1Solver = None

    @classmethod
    def setProblemSolver(cls, problem1Solver):
        Problem1PredictionFeature.problem1Solver = problem1Solver

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        instrumentsDict = instrumentManager.getAllInstrumentsByInstrumentId()
        positionData = instrumentLookbackData.getFeatureDf('position')
        previousPosition = 0 if updateNum <= 2 else positionData.iloc[-1]
        currentPosition = pd.Series([instrumentManager.getInstrument(x).getCurrentPosition() for x in instrumentsDict], index= instrumentsDict)
        changeInPosition = currentPosition - previousPosition
        enterFlag = 0*currentPosition if updateNum <= 2 else instrumentLookbackData.getFeatureDf(featureKey).iloc[-1]
        enterFlag[changeInPosition!=0] = True
        enterFlag[changeInPosition==0] = False

        return enterFlag



if __name__ == "__main__":
    tf = GBTradingFunctions(stocks=STOCKS, startdate=DATE_TO_TRADE, frequency=FREQUENCY_TRADE_SECS)
    print('Trading functions loaded')
    tsParams = LivePairTradingParams(tf)
    print('TS params loaded')
    tradingSystem = TradingSystem(tsParams)
    print('Trading System loaded')
    print('start trading now')
    if IS_LIVE:
        t = tradingSystem.startTradingLive(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)
    else:
        t = tradingSystem.startTrading(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)
    print('Done')
