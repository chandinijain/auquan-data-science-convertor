import numpy as np
import subprocess
import os
import sys
import pandas as pd
import re
import datetime

pd.options.display.max_rows = 999
pd.set_option('expand_frame_repr', False)
pd.set_option('display.float_format', lambda x: '%.2f' % x)

print('Pnl for date %s'%sys.argv[1])
prod = pd.read_csv(sys.argv[4], header=None, index_col = 1)
stocks = prod.index if len(sys.argv)<=5 else [sys.argv[5]]
total = 0
capital=0
filename = 'TradeLog'+sys.argv[1]
str = 'TRADE_'
headers = ['ResponseType','OrderID','Symbol','Side','Price','Quantity','AccountID','ErrorCode','TimeStamp','Exchange_Order_Id','ChildResponseType']
print(stocks)
totalpnl = pd.Series(0, index = stocks)
for s in stocks:
    pnldf=[]
    print('------------------------------')
    print('Stock Name: %s'%s)
    ### Read stock and future confirms from TradeLog
    if sys.argv[2]=='R':
      print("grep '%s' %s | grep '%s' >> %s.txt"%(str,filename, s, s))
      os.system("grep '%s' %s | grep '%s' > '%s.txt'"%(str,filename, s, s))

    ### Split and save data in a dataframe, apply the right headers
    f = open('%s.txt'%s)

    for line in f:
      word = line.find('ResponseType:TRADE_CONFIRM')
      if word >0:
        line = line[word:]
      l = re.split("[, \|!?:]+",line.strip())
      pnldf.append(l)

    df = pd.DataFrame(pnldf)
    #print(len(df))
    if len(df)==0:
      continue
    cols = {}
    for i in range(11):
      del df[2*i]
      cols[2*i+1] = headers[i]

    df = df.rename(cols, axis='columns')
    dt = [datetime.datetime.fromtimestamp(int(d)/1e9).replace(microsecond=0) for d in df['TimeStamp']]
    df['time'] = dt
    df.set_index(df['OrderID'], inplace=True)

    ### Replace B/S with 1,-1 and multiply with Qty
    df['Side'].replace({'S':-1.0, 'B':1.0}, inplace=True)
    df['Quantity'] =  df['Side']*df['Quantity'].astype(int)

    ### Aggregrate same orders on different lines into one row
    df['Total'] = df.groupby(['OrderID'])['Quantity'].cumsum()
    df.drop_duplicates('OrderID', keep='last', inplace=True)
    df['Quantity'] = df['Total']

    ### Calculate Pnl, Cumulative Basis Qty, Stock Qty, Future Qty
    df['Pnl'] = (-(df['Quantity']*df['Price'].astype(float))\
                 - np.abs(df['Quantity'])*df['Price'].astype(float)*0.0001)/100.0
    pos = pd.DataFrame(index=df.index, columns=['time','Cum Pnl', 'Cum Pos', 'S Pos', 'F Pos','S Pr','F Pr'])
    pos['time'] = df['time']
    pos['Cum Pnl'] = df['Pnl'].cumsum()
    pos['Cum Pos'] = df['Quantity'].cumsum().astype(int)
    pos['Cum Pnl'] = df['Pnl'].cumsum() + pos['Cum Pos']*df['Price'].astype(float)/100.0
    pos.loc[df['Symbol']==s, 'S Pos'] = df['Quantity'].astype(int)
    pos.loc[df['Symbol']!=s, 'F Pos'] = df['Quantity'].astype(int)
    pos.loc[df['Symbol']==s, 'S Pr'] = df['Price'].astype(float)
    pos.loc[df['Symbol']!=s, 'F Pr'] = df['Price'].astype(float)
    pos['S Pr'].fillna(method='ffill', inplace=True)
    pos['F Pr'].fillna(method='ffill', inplace=True)
    pos['B Pr'] = pos['S Pr']-pos['F Pr']
    pos.loc[pos['Cum Pos']!=0, 'B Pr'] = 0
    pos.fillna(0, inplace=True)
    pos['S Pos'] = pos['S Pos'].cumsum()
    pos['F Pos'] = pos['F Pos'].cumsum()
    pos['Cum Pnl'] = df['Pnl'].cumsum() + (pos['S Pos']*pos['S Pr'] + pos['F Pos']*pos['F Pr'])/100.0
    pos['Capital'] = 0.15*np.abs((pos['S Pos']*pos['S Pr'] - pos['F Pos']*pos['F Pr']))/100
    #pos.sort_values(by='time',axis=0,inplace=True)
    pos.set_index(pos['time'], inplace=True)
    del pos['time']
    print(pos)

    ### Print totals and calculate strategy total
    print('Final Unmatched Quantity: %.2f'%df['Quantity'].sum())
    print('Final Basis Qty: %.2f'%pos['S Pos'].iloc[-1])
    print('Pnl without fees: %.2f'%(-(df['Quantity']*df['Price'].astype(float)).sum()/100.0))
    print('fees: %.2f'%((np.abs(df['Quantity'])*df['Price'].astype(float)).sum()*0.0001/100.0))

    ### Account for unclosed positions:
    os.system("grep '%s,' bookData-%s | tail -n1 >> 'closeprice.txt'"%(s.ljust(10),sys.argv[1]))
    os.system("grep '%s,' bookData-%s | tail -n1 >> 'closeprice.txt'"%(s+sys.argv[3],sys.argv[1]))

    f = open('closeprice.txt')
    lines = f.readlines()
    stk = 0
    fut = 0
    for line in lines[-2:]:
      l = re.split("[, \|!?:]+",line.strip())
      if l[1]==s:
        stk = l[7]
      else:
        fut = l[7]
    #print(float(stk)/100.0, float(fut)/100.0)
    pnl = ((pos['Cum Pnl'].iloc[-1] -0.0001*(float(stk)*np.abs(pos['S Pos'].iloc[-1])+float(fut)*np.abs(pos['F Pos'].iloc[-1]))/100.0))
    #+ (float(stk)*pos['S Pos'].iloc[-1]+float(fut)*pos['F Pos'].iloc[-1])/100.0
          #  -0.0001*(float(stk)*np.abs(pos['S Pos'].iloc[-1])+float(fut)*np.abs(pos['F Pos'].iloc[-1]))/100.0))
    print('Pnl after closing: %.2f'%pnl)
    total += pnl
    totalpnl[s] = pnl
    capital += np.max(pos['Capital'])
print('----------------------------------------------')
print('TOTAL PNL %.2f'%total)
print('Total Capital Used %.2f' %capital)
print('ROC %.2f'%(100*total/capital))
print(totalpnl)

### Verify against OrderConfirmer
os.system("grep -v 'changeInPosition|0,tradeLoss|0' orderConfirmer-%s > temp; mv temp orderConfirmer-%s"%(sys.argv[1], sys.argv[1]))
tradesdf = []

### Split and save data in a dataframe, apply the right headers
f = open('orderConfirmer-%s'%sys.argv[1])
for line in f:
  l = re.split("[, \|!?:]+",line.strip())
  tradesdf.append(l)

df = pd.DataFrame(tradesdf)
headers = ['instrumentId','changeInPosition','tradeLoss', 'price']
cols = {}
for i in range(4):
    del df[2*i]
    cols[2*i+1] = headers[i]
df = df.rename(cols, axis='columns')

df['changeInPosition'] = df['changeInPosition'].astype(float)
df['tradeLoss'] = df['tradeLoss'].astype(float)
df['price'] = df['price'].astype(float)
grouped = df.groupby('instrumentId')
#for name, group in grouped:
#  print(name)
#  print(group)

print(grouped.aggregate(np.sum))
