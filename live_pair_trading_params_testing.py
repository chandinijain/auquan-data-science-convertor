# TODO: This is a temporay way so that you can run this anywhere and backtester from auquantoolbox
# can be anywhere.
import sys
sys.path.insert(0, "../auquantoolbox")

from backtester.trading_system_parameters import TradingSystemParameters
from backtester.executionSystem.basis_execution_system import BasisExecutionSystem
from backtester.constants import *
from backtester.features.feature import Feature
from backtester.logger import *
from backtester.trading_system import TradingSystem
import numpy as np
import pandas as pd
from live_alpha_order_placer import LiveAlphaOrderPlacer
from alpha_grep_data_source import AlphaGrepDataSource
from live_time_rule import LiveTimeRule
from backtesting_time_rule import BacktestingTimeRule
from datetime import datetime, timedelta
from backtester.timeRule.us_time_rule import USTimeRule
from backtester.orderPlacer.backtesting_order_placer import BacktestingOrderPlacer

import pickle
import requests
#import keras
#from keras.layers.core import Dense, Activation, Dropout
#from keras.layers.recurrent import LSTM
#from keras.models import Sequential
#from sklearn.preprocessing import StandardScaler
#from keras.models import load_model
import types
import tempfile


STOCKS = ['VKE', 'AIO', 'CBT', 'FCY', 'GGK',
          'IES', 'IYU', 'KMV', 'KRZ',
          'NYO', 'OGU', 'PLX', 'PMS',
          'UBF', 'UWD', 'VML', 'VSL',
          'XFD', 'XIT', 'YGC', 'YUZ']
DATE_TO_TRADE = '20180205'
BOOK_DATA_FILE = '../repo.auquan/bookData-' + DATE_TO_TRADE
ORDER_PLACER_FILE = '../repo.auquan/orderPlacer.txt'
ORDER_CONFIRMER_FILE = '../repo.auquan/orderConfirmer-' + DATE_TO_TRADE
INSTRUMENT_UPDATE_PROCESSED_FILE = "../repo.auquan/instrumentUpdateProcessed.txt"
FREQUENCY_TRADE_SECS = 5

IS_LIVE = False


class LSTMTradingFunctions():

    def __init__(self):
        # self.loadModelParams(filename="problem1_clustered_params.pkl")
        self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)
        self.stocks = self.unscrambler.index
        self.count = 0
        self.params = {}
        self.__featureKeys = []

    def getSymbolsToTrade(self):
        return [self.unscrambler.loc[i, 'Name'] for i in self.unscrambler.index]

    def loadModelParams(self, filename=""):
        """
        Load model paramters for each stock using pickle file
        """
        with open(filename, "rb") as f:
            self.stock_cluster, self.normalizer = pickle.load(f)
            # print(self.stock_cluster)
            # print(self.normalizer)

        self.model = dict()
        for i in range(6):
            self.model[i] = load_model("problem1_lstm_%d.h5" % i)
            # print(self.model[i].summary())

    def getInstrumentFeatureConfigDicts(self):

        basisDict = {'featureKey': 'basis',
                     'featureId': 'basis_calculator',
                     'params': {'instr1bid': 'stockTopBidPrice',
                                'instr1ask': 'stockTopAskPrice',
                                'instr2bid': 'futureTopBidPrice',
                                'instr2ask': 'futureTopAskPrice'}}

        real_features = ['basis', 'stockVWAP', 'futureVWAP', 'stockTopBidPrice', 'stockTopAskPrice', 'futureTopBidPrice',
                         'futureTopAskPrice', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidPrice',
                         'futureNextAskPrice', 'stockAverageBidPrice', 'stockAverageAskPrice', 'futureAverageBidPrice',
                         'futureAverageAskPrice']

        # generate_features = [basisDict]
        generate_features = []
        featureList = []
        for rf in real_features:
            ma_5 = {'featureKey': 'ma_5' + rf,
                    'featureId': 'moving_average',
                    'params': {'period': 5,
                               'featureName': rf}}
            expma = {'featureKey': 'expma' + rf,
                     'featureId': 'exponential_moving_average',
                     'params': {'period': 20,
                                'featureName': rf}}
            sdev_5 = {'featureKey': 'sdev_5' + rf,
                      'featureId': 'moving_sdev',
                      'params': {'period': 5,
                                 'featureName': rf}}

            generate_features += [ma_5, sdev_5, expma]
            featureList += [f_id + rf for f_id in ['ma_5', 'sdev_5', 'expma']]

        moving_min = {'featureKey': 'moving_min',
                      'featureId': 'moving_min',
                      'params': {'period': 5,
                                 'featureName': 'basis'}}
        moving_sum = {'featureKey': 'moving_sum',
                      'featureId': 'moving_sum',
                      'params': {'period': 5,
                                 'featureName': 'basis'}}
        delay = {'featureKey': 'delay',
                 'featureId': 'delay',
                 'params': {'period': 5,
                            'featureName': 'basis'}}
        diff = {'featureKey': 'diff',
                'featureId': 'difference',
                'params': {'period': 5,
                             'featureName': 'basis'}}
        rank = {'featureKey': 'rank',
                'featureId': 'rank',
                'params': {'period': 5,
                             'featureName': 'basis'}}
        scale = {'featureKey': 'scale',
                 'featureId': 'scale',
                 'params': {'period': 5,
                            'featureName': 'basis', 'scale': 3}}

        generate_features += [moving_min, moving_sum, delay, diff, rank, scale]
        featureList += ['moving_min', 'moving_sum', 'delay', 'diff', 'rank', 'scale']
        self.setFeatureKeys(featureList)
        return generate_features

    def getPrediction(self, time, updateNum, instrumentManager):

        lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

        predictedFairValue = []

        predictedFairValue = (lookbackInstrumentFeatures.getFeatureDf('ma_5basis').fillna(0).iloc[-1] + lookbackInstrumentFeatures.getFeatureDf('expmabasis').fillna(0).iloc[-1]) / 2

        return predictedFairValue

    def getFeatureKeys(self):
        return self.__featureKeys

    def setFeatureKeys(self, featureList):
        self.__featureKeys = featureList


class LivePairTradingParams(TradingSystemParameters):

    def __init__(self, tradingFunctions):
        self.__tradingFunctions = tradingFunctions
        self.__bookDataFile = BOOK_DATA_FILE
        self.__orderPlacerFile = ORDER_PLACER_FILE
        self.__orderConfirmerFile = ORDER_CONFIRMER_FILE
        self.__instrumentUpdateProcessedFile = INSTRUMENT_UPDATE_PROCESSED_FILE
        super(LivePairTradingParams, self).__init__()
        if IS_LIVE:
            self.__startTime = datetime.now()
            self.__endTime = self.__startTime + timedelta(seconds=900)
        else:
            self.__startTime = datetime.strptime(DATE_TO_TRADE, '%Y%m%d') + timedelta(hours=9, minutes=15)
            self.__endTime = datetime.strptime(DATE_TO_TRADE, '%Y%m%d') + timedelta(hours=15, minutes=30)
        self.__instrumentIds = self.__tradingFunctions.getSymbolsToTrade()

    def getStartingCapital(self):
        return 2000000

    def getDataParser(self):
        # Change last parameter to false, if you want to read from an already written file as opposed to tailing from a file.
        return AlphaGrepDataSource(self.__instrumentIds, self.__bookDataFile, self.__instrumentUpdateProcessedFile, True, IS_LIVE)

    def getTimeRuleForUpdates(self):
        # TODO: Better implementation of this and end time here matching actual trading end time
        # TODO: Might have to take care of trading days too in this later on
        return LiveTimeRule(self.__startTime, FREQUENCY_TRADE_SECS, self.__endTime)

    def getBenchmark(self):
        return None

    '''
    This is a way to use any custom features you might have made.
    Returns a dictionary where
    key: featureId to access this feature (Make sure this doesnt conflict with any of the pre defined feature Ids)
    value: Your custom Class which computes this feature. The class should be an instance of Feature
    Eg. if your custom class is MyCustomFeature, and you want to access this via featureId='my_custom_feature',
    you will import that class, and return this function as {'my_custom_feature': MyCustomFeature}
    '''

    def getCustomFeatures(self):
        return {'prediction_feature': PredictionFeature,
                'basis_calculator': BasisCalculator,
                'spread': SpreadCalculator,
                'total_fees': TotalFeesCalculator}

    def getInstrumentFeatureConfigDicts(self):
        stockFeatureConfigs = self.__tradingFunctions.getInstrumentFeatureConfigDicts()

        fairValuePrediction = {'featureKey': 'prediction',
                               'featureId': 'prediction_feature',
                               'params': {'lstm': self.__tradingFunctions}}
        scoreDict = {'featureKey': 'score',
                     'featureId': 'prob1_score',
                     'params': {'predictionKey': 'prediction',
                                'price': 'basis'}}
        sdevDictForExec = {'featureKey': 'sdev_5_for_exec',
                           'featureId': 'moving_sdev',
                           'params': {'period': 5,
                                      'featureName': 'basis'}}
        spreadConfigDict = {'featureKey': 'spread',
                            'featureId': 'spread',
                            'params': {'instr1bid': 'stockTopBidPrice',
                                       'instr1ask': 'stockTopAskPrice',
                                       'instr2bid': 'futureTopBidPrice',
                                       'instr2ask': 'futureTopAskPrice'}}
        feesConfigDict = {'featureKey': 'fees',
                          'featureId': 'total_fees',
                          'params': {'price': 'stockVWAP',
                                     'feesDict': {1: 0.0001, -1: 0.0001, 0: 0},
                                     'spread': 'spread'}}
        profitlossConfigDict = {'featureKey': 'pnl',
                                'featureId': 'pnl',
                                'params': {'price': self.getPriceFeatureKey(),
                                           'fees': 'fees'}}
        capitalConfigDict = {'featureKey': 'capital',
                             'featureId': 'capital',
                             'params': {'price': 'stockVWAP',
                                        'fees': 'fees',
                                        'capitalReqPercent': 0.15}}

        return {INSTRUMENT_TYPE_STOCK: stockFeatureConfigs +
                [fairValuePrediction, sdevDictForExec, scoreDict,
                 spreadConfigDict, feesConfigDict,
                 profitlossConfigDict, capitalConfigDict]}

    def getMarketFeatureConfigDicts(self):

        scoreDict = {'featureKey': 'score',
                     'featureId': 'prob1_score',
                     'params': {'price': 'basis',
                                'instrument_score_feature': 'score',
                                'benchmark_score_feature': 'benchmark_score'}}
        # TODO
        return []

    # def getPrediction(self, time, updateNum, instrumentManager):

    #     predictions = pd.Series(self.__tradingFunctions.getPrediction(time, updateNum, instrumentManager),
    #                             index = self.__instrumentIds)

    #     return predictions

    def getExecutionSystem(self):
        return BasisExecutionSystem(basisEnter_threshold=0.25, basisExit_threshold=0.01,
                                    basisLongLimit=25, basisShortLimit=25,
                                    basisCapitalUsageLimit=0.1, basisLotSize=1,
                                    basisLimitType='L', basis_thresholdParam='sdev_5_for_exec',
                                    price=self.getPriceFeatureKey())

    def getOrderPlacer(self):
        return LiveAlphaOrderPlacer(self.__orderPlacerFile, self.__orderConfirmerFile)
        # return BacktestingOrderPlacer()

    def getLookbackSize(self):
        return 120

    def getPriceFeatureKey(self):
        return 'basis'


class PredictionFeature(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        tf = featureParams['lstm']
        return pd.Series(tf.getPrediction(time, updateNum, instrumentManager),
                         index=instrumentManager.getAllInstrumentsByInstrumentId().keys())


class BasisCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        try:
            currentStockBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1bid']).iloc[-1]
            currentStockAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1ask']).iloc[-1]
            currentFutureBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2bid']).iloc[-1]
            currentFutureAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2ask']).iloc[-1]
        except KeyError:
            logError('Bid and Ask Price Feature Key does not exist')

        basis = currentStockAskPrice + currentStockBidPrice - currentFutureAskPrice - currentFutureBidPrice
        return basis / 2.0


class SpreadCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()
        try:
            currentStockBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1bid']).iloc[-1]
            currentStockAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr1ask']).iloc[-1]
            currentFutureBidPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2bid']).iloc[-1]
            currentFutureAskPrice = instrumentLookbackData.getFeatureDf(featureParams['instr2ask']).iloc[-1]
        except KeyError:
            logError('Bid and Ask Price Feature Key does not exist')

        currentSpread = currentStockAskPrice - currentStockBidPrice + currentFutureAskPrice - currentFutureBidPrice
        return currentSpread / 4.0


class TotalFeesCalculator(Feature):

    @classmethod
    def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
        instrumentLookbackData = instrumentManager.getLookbackInstrumentFeatures()

        positionData = instrumentLookbackData.getFeatureDf('position')
        feesDict = featureParams['feesDict']
        currentPosition = positionData.iloc[-1]
        previousPosition = 0 if updateNum <= 2 else positionData.iloc[-2]
        changeInPosition = pd.Series(0, index=instrumentManager.getAllInstrumentsByInstrumentId()) if updateNum <= 2 else (currentPosition - previousPosition)
        changeInPosition.fillna(0)
        f = [feesDict[np.sign(x)] for x in changeInPosition]
        fees = np.abs(changeInPosition) * [feesDict[np.sign(x)] for x in changeInPosition]
        if 'price' in featureParams:
            try:
                priceData = instrumentLookbackData.getFeatureDf(featureParams['price'])
                currentPrice = priceData.iloc[-1]
            except KeyError:
                logError('Price Feature Key does not exist')

            fees = fees * currentPrice
        # TODO: Implement
        total = 2 * fees \
            + (np.abs(changeInPosition) * instrumentLookbackData.getFeatureDf(featureParams['spread']).iloc[-1])
        return total


if __name__ == "__main__":
    tf = LSTMTradingFunctions()
    tsParams = LivePairTradingParams(tf)
    tradingSystem = TradingSystem(tsParams)
    if IS_LIVE:
        tradingSystem.startTradingLive(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)
    else:
        tradingSystem.startTrading(onlyAnalyze=False, shouldPlot=False, makeInstrumentCsvs=False)