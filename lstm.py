from backtester.features.feature import Feature
from backtester.trading_system import TradingSystem
from backtester.sample_scripts.fair_value_params import FairValueTradingParams
from backtester.version import updateCheck
from backtester.instruments_manager import InstrumentManager
import sys
import pandas as pd
import numpy as np
import pickle
import requests
import keras
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.preprocessing import StandardScaler
from keras.models import load_model
import types
import tempfile

def getDownloadLink(fileId):
	link = "https://drive.google.com/uc?id=" + fileId + "&export=download"
	return link

def downloadFile(fileId, name):
	link = getDownloadLink(fileId)
	r = requests.get(link)
	with open(name, 'wb') as f:
		f.write(r.content)

class Problem1Solver():

	'''
	Specifies which training data set to use. Right now support
	trainingData1, trainingData2, trainingData3.
	'''

	def __init__(self):
		self.stocks = ['AIO','CBT','FCY','GGK',
                'IES','IYU','KMV','KRZ',
                'NYO','OGU','PLX','PMS',
                'UBF','UWD','VML','VSL',
                'XFD','XIT','YGC','YUZ']
		# ['AGW','AIO','AUZ','BSN','BWU','CBT','CHV','CSR','CUN','CYD','DCO','DFZ','DVV','DYE','EGV','FCY','FFA','FFS','FKI','FRE','FUR','GFQ','GGK','GYJ','GYV','HHK','IES','IFL','ILW','IMC','IUE','IUQ','IYU','JSG','JYW','KAA','KFW','KKG','KMV','KRZ','LDU','LKB','LPQ','MAS','MQK','MUF','NDG','NSL','NYO','OED','OGU','OMP','PFK','PLX','PMS','PQS','PUO','QRK','SCN','SVG','TGI','TWK','UBF','UWC','UWD','VKE','VML','VND','VSL','VTN','WAG','WTJ','XAD','XCS','XFD','XIT','XPV','XYR','XZR','YGC','YHW','YUZ','ZEW','ZLX']
		downloadFile('1pQry2Ei8Wpx6PvmC5BfpjMqB8vpu6aYv', 'problem1_clustered_params.pkl')
		downloadFile('1R9TeVl-PeaNhm6ILhTwf_0hGZ-DsOuOM', 'problem1_lstm_0.h5')
		downloadFile('1fDZtpUS04uBzu7QOEpUHwDqbibe9Gg_u', 'problem1_lstm_1.h5')
		downloadFile('1LFonckILZHE2fCB7lXoMe__InlF0qwge', 'problem1_lstm_2.h5')
		downloadFile('1HooDxfRniwNkjaaLEIRT96hegAD0ysWO', 'problem1_lstm_3.h5')
		downloadFile('1bble96ypdpHN9BJDG7AX1qXD96Cs4Wjq', 'problem1_lstm_4.h5')
		downloadFile('12y5735uBX9w2llEyRO6Rk2IfXDuOnswk', 'problem1_lstm_5.h5')
		self.loadModelParams(filename="problem1_clustered_params.pkl")
		self.unscrambler = pd.read_csv('unscrambler.csv', index_col=0)


	def getTrainingDataSet(self):
		return "OSData2P1"

	def getSymbolsToTrade(self):
		print([self.unscrambler.loc[i, 'Name'] for i in self.stocks])
		return [self.unscrambler.loc[i, 'Name'] for i in self.stocks]



	def loadModelParams(self, filename=""):
		"""
		Load model paramters for each stock using pickle file
		"""
		with open(filename, "rb") as f:
			self.stock_cluster, self.normalizer = pickle.load(f)
			# print(self.stock_cluster)
			# print(self.normalizer)

		self.model = dict()
		for i in range(6):
			self.model[i] = load_model("problem1_lstm_%d.h5" % i)
			# print(self.model[i].summary())


	def getCustomFeatures(self):
		return {'my_custom_feature': MyCustomFeature}


	def getFeatureConfigDicts(self):
		real_features = ['basis', 'stockVWAP', 'futureVWAP', 'stockTopBidPrice', 'stockTopAskPrice', 'futureTopBidPrice',
						 'futureTopAskPrice', 'stockNextBidPrice', 'stockNextAskPrice', 'futureNextBidPrice', 
						 'futureNextAskPrice', 'stockAverageBidPrice', 'stockAverageAskPrice', 'futureAverageBidPrice',
						 'futureAverageAskPrice']

		generate_features = []
		self.feature_keys = []
		for rf in real_features:
			ma_5 = {'featureKey': 'ma_5' + rf,
					 'featureId': 'moving_average',
					 'params': {'period': 5,
								'featureName': rf}}
			expma = {'featureKey': 'expma' + rf,
				   'featureId': 'exponential_moving_average',
				   'params': {'period': 20,
								'featureName': rf}}
			sdev_5 = {'featureKey': 'sdev_5' + rf,
					  'featureId': 'moving_sdev',
					  'params': {'period': 5,
								 'featureName': rf}}

			generate_features += [ma_5, sdev_5, expma]
			self.feature_keys += [f_id + rf for f_id in ['ma_5', 'sdev_5', 'expma']]

		moving_min = {'featureKey': 'moving_min',
					'featureId': 'moving_min',
					'params': {'period': 5,
							   'featureName': 'basis'}}
		moving_sum = {'featureKey': 'moving_sum',
				  'featureId': 'moving_sum',
				  'params': {'period': 5,
							 'featureName': 'basis'}}
		delay = {'featureKey': 'delay',
				  'featureId': 'delay',
				  'params': {'period': 5,
							 'featureName': 'basis'}}
		diff = {'featureKey': 'diff',
				  'featureId': 'difference',
				  'params': {'period': 5,
							 'featureName': 'basis'}}
		rank = {'featureKey': 'rank',
				  'featureId': 'rank',
				  'params': {'period': 5,
							 'featureName': 'basis'}}
		scale = {'featureKey': 'scale',
				  'featureId': 'scale',
				  'params': {'period': 5,
							 'featureName': 'basis', 'scale': 3}}

		generate_features += [moving_min, moving_sum, delay, diff, rank, scale]
		self.feature_keys += ['moving_min', 'moving_sum', 'delay', 'diff', 'rank', 'scale']
		return generate_features

	def getFairValue(self, updateNum, time, instrumentManager):
		# holder for all the instrument features
		lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

		# dataframe for a historical instrument feature (ma_5 in this case). The index is the timestamps
		# atmost upto lookback data points. The columns of this dataframe are the stock symbols/instrumentIds.
		# fairValueData = lookbackInstrumentFeatures.getFeatureDf('FairValue')

		predictedFairValue = []
		x_star = []             # Data point at time t (whose FairValue will be predicted)
		for f in self.feature_keys:
			data = lookbackInstrumentFeatures.getFeatureDf(f).fillna(0)
			x_star.append(np.array(data.iloc[-1]))
		x_star = np.array(x_star)                                       # shape = d x s
		x_star = np.vstack((x_star, np.power(x_star[0,:], 2)))          # shape = (d+1) x s

		predictedFairValue = [self.model[self.stock_cluster[stock]].predict(np.expand_dims(self.normalizer[self.stock_cluster[stock]].transform(np.array([x_star[:,i]])), axis=1), batch_size=1)[0][0] for i, stock in enumerate(self.stocks)]
		print(predictedFairValue)
		# y_star = lookbackInstrumentFeatures.getFeatureDf('FairValue').iloc[-1]
		# print("True FairValue:", y_star)
		# print("UPDATENUM", updateNum)
		return predictedFairValue

'''
We have already provided a bunch of commonly used features. But if you wish to make your own, define your own class like this.
Write a class that inherits from Feature and implement the one method provided.
'''


class MyCustomFeature(Feature):
	''''
	Custom Feature to implement for instrument. This function would return the value of the feature you want to implement.
	This function would be called at every update cycle for every instrument. To use this feature you MUST do the following things:
	1. Define it in getCustomFeatures, where you specify the identifier with which you want to access this feature.
	2. To finally use it in a meaningful way, specify this feature in getFeatureConfigDicts with appropirate feature params.
	Example for this is provided below.
	Params:
	updateNum: current iteration of update. For first iteration, it will be 1.
	time: time in datetime format when this update for feature will be run
	featureParams: A dictionary of parameter and parameter values your features computation might depend on.
				   You define the structure for this. just have to make sure these parameters are provided when
				   you wanted to actually use this feature in getFeatureConfigDicts
	featureKey: Name of the key this will feature will be mapped against.
	instrumentManager: A holder for all the instruments
	Returns:
	A Pandas series with stocks/instrumentIds as the index and the corresponding data the value of your custom feature
	for that stock/instrumentId
	'''
	@classmethod
	def computeForInstrument(cls, updateNum, time, featureParams, featureKey, instrumentManager):
		# Custom parameter which can be used as input to computation of this feature
		param1Value = featureParams['param1']

		# A holder for the all the instrument features
		lookbackInstrumentFeatures = instrumentManager.getLookbackInstrumentFeatures()

		# dataframe for a historical instrument feature (basis in this case). The index is the timestamps
		# atmost upto lookback data points. The columns of this dataframe are the stocks/instrumentIds.
		lookbackInstrumentBasis = lookbackInstrumentFeatures.getFeatureDf('basis')

		# The last row of the previous dataframe gives the last calculated value for that feature (basis in this case)
		# This returns a series with stocks/instrumentIds as the index.
		currentBasisValue = lookbackInstrumentBasis.iloc[-1]

		if param1Value == 'value1':
			return currentBasisValue * 0.1
		else:
			return currentBasisValue * 0.5


# class GenerateModel():
#   """docstring for TrainModel"""
#     def __init__(self, problem, training_dataset):
#         self.problem = problem
#         self.training_dataset = training_dataset

#     def load_data(self):
#         for dataset in self.training_dataset:


#     def train(self):
		
#         pass

if __name__ == "__main__":
	if updateCheck():
		print('Your version of the auquan toolbox package is old. Please update by running the following command:')
		print('pip install -U auquan_toolbox')
	# else:
	problem1Solver = Problem1Solver()
	tsParams = FairValueTradingParams(problem1Solver)
	tradingSystem = TradingSystem(tsParams)
	# Set shouldPlot to True to quickly generate csv files with all the features
	# Set onlyAnalyze to False to run a full backtest
	# Set makeInstrumentCsvs to True to make instrument specific csvs in runLogs. This degrades the performance of the backtesting system
	tradingSystem.startTrading(onlyAnalyze=False, shouldPlot=True, makeInstrumentCsvs=True)
			