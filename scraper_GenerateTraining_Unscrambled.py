import pandas as pd
import numpy as np
import os
import datetime as dt
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta, TH

def calc_last_thursday(date):
    """Assuming the "date" is not equal to "last_thursday" """
    end_of_month = date + relativedelta(day=31)
    last_thursday = end_of_month + relativedelta(weekday=TH(-1))
    if last_thursday < date:
        end_of_next_month = date + relativedelta(months=1) + relativedelta(day=31)
        last_thursday = end_of_next_month + relativedelta(weekday=TH(-1))
    return last_thursday

for root, dirs, files in os.walk('/Users/chandinijain/Auquan/DataQQ3'):
	print(dirs)

root = '/Users/chandinijain/Auquan/DataQQ3'
dirs = 'QQDataAllDates'
files = next(os.walk('%s/%s'%(root,dirs)))[2]
useful = []
acc = None
for name in files[:]:
		print(name)
		# dateparse = lambda dates: [pd.datetime.strptime(d, '%d/%m/%y %H:%M:%S') for d in dates]

		data = pd.read_csv(os.path.join(root, dirs, name), index_col=0, parse_dates=True)#, parse_dates=[['DATE', 'TIME']], date_parser=dateparse)	
		data = data[data['CLOSE']!=0]

	######### PARSER when datetime does not exist #####	
		# data['datetime'] = pd.to_datetime(data['DATE'] + ' ' + data['TIME'], format='%d/%m/%y %H:%M:%S' )
		# data.set_index(data['datetime'], drop=True, inplace=True)
		# print(name, ' Start: ', dt.datetime.strftime(data.index[0], '%Y-%m-%d'), \
		# 	' Close: ', dt.datetime.strftime(data.index[-1], '%Y-%m-%d'), \
		# 	' Length: ', len(data))
	
	######### check if data exists from 2014-2018 
		# if data.index[-1] > dt.datetime.strptime('20180420', '%Y%m%d'):
		# 	if data.index[0] < dt.datetime.strptime('20140601', '%Y%m%d'):
		# 		useful = useful + [name]
		# print(useful) 
		# data.sort_index(inplace=True)
	
	######### check if big drop in data
		returns = data['CLOSE']/data['CLOSE'].shift(1) -1
		print(data[returns<-0.45])
		print(data[(data['CLOSE']/data['CLOSE'].shift(-1) -1)>0.95])
		highprice = data[(data['CLOSE']/data['CLOSE'].shift(-1) -1)>0.95]['CLOSE']
		lowprice = data[returns<-0.45]['CLOSE']
		print(highprice.values, lowprice.values, np.round(highprice.values/lowprice.values))
		divisor_vals = np.round(highprice.values/lowprice.values)
		divisor = pd.Series(np.nan, index = data.index)
		if len(divisor_vals)>0:
			divisor[returns<-0.45] = divisor_vals
		divisor.iloc[-1] = 1
		divisor.fillna(method='bfill',inplace=True)
		print(divisor)
		# print((1-data['RETURNS']))
		# print((1-data['RETURNS']).sort_index(ascending=False))
		# print((1-data['RETURNS']).sort_index(ascending=False).cumprod())
		# print(((1-data['RETURNS']).sort_index(ascending=False).cumprod()*(data['CLOSE'].iloc[-1])))
		# print(((1-data['RETURNS']).sort_index(ascending=False).cumprod()*(data['CLOSE'].iloc[-1])).sort_index(ascending=True))
		prices = ((1-data['RETURNS']).sort_index(ascending=False).cumprod()*(data['CLOSE'].iloc[-1])).sort_index(ascending=True).shift(-1)
		prices.iloc[-1] = data['CLOSE'].iloc[-1]
		# print(prices)
		delta = np.sign(prices / prices.shift(75) -1) + np.sign(prices / prices.shift(75) -1)
		data['Y'] = delta.abs()/2
		data['Y'].loc[data['Y']==0.5] = delta
		data['CLOSE'] = data['CLOSE'].map(lambda x: "{:.2f}".format(x))
		data['EQUITY']= data['EQUITY'].map(lambda x: "{:.3f}".format(x))
		data['RETURNS'] = data['RETURNS'].map(lambda x: "{:.3f}".format(x))
		data['ADJCLOSE'] = prices.map(lambda x: "{:.2f}".format(x))
		data['ADJVOLUME'] = (data['Volume']*divisor).map(lambda x: "{:.0f}".format(x))
		data['Volume'] = data['Volume'].map(lambda x: "{:.0f}".format(x))
		

	######### compare data with ACC to see missing points	
		# if acc is None:
		# 	acc = data['CLOSE']

		# fig, ax1 = plt.subplots()
		# ax1.plot(data['CLOSE'])
		# ax1.plot(prices, 'g')
		# ax1.set_xlabel('time (s)')
		# # Make the y-axis label, ticks and tick labels match the line color.
		# ax1.set_ylabel('%s'%name, color='b')
		# ax1.tick_params('y', colors='b')

		# ax2 = ax1.twinx()

		# ax2.plot(acc, 'r')
		# ax2.set_ylabel('ACC', color='r')
		# ax2.tick_params('y', colors='r')

		# fig.tight_layout()
		# plt.title('%s'%name)
		# plt.show()
		# finalTrainingData = data.loc['2014-1-1':'2017-6-10'] 
		data.to_csv('/Users/chandinijain/Auquan/DataQQ3/QQDataTraining/%s'%name, index=True)