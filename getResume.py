import csv
import requests


def getDownloadLink(filelink):
    try:
        downloadId = filelink.split("=", 1)[1]
        link = "https://drive.google.com/uc?id=" + downloadId + "&export=download"
    except:
        print("skipping:", filelink)
        link = None
    return link


def downloadFile(link, name):
    if link is not None:
        r = requests.get(link)
        with open(name, 'wb') as f:
            f.write(r.content)


with open('StudentResumeFlow.csv', 'rb') as f:
    reader = csv.reader(f)
    reader.next()
    reader.next()
    for row in reader:

        link = getDownloadLink(row[5])
        name = row[0] + '_' + row[1] + '_' + row[2] #+ '.pdf'
        name = name.replace(" ", "_")
        print "started " + name
        downloadFile(link, name)
        print "done " + name
        print